"""
Tipo String

Em Python, um dado é considerado um tipo String sempre que:

- Estiver entre aspas simples -> 'uma string', '234', 'a', 'True', '42.3'
- Estiver entre aspas duplas -> "uma string", "234", "a", "True", "42.3"
- Estiver entre aspas simples triplas -> '''42.3'''
"""
# - Estiver entre aspas duplas triplas -> """42.3"""

nome = 'Teste'
print(nome)
print(type(nome))

nome = "Gina's Bar"
print(nome)
print(type(nome))

nome = 'Angelina \nJolie'
print(nome)
print(type(nome))

nome = """Angelina
Jolie"""
print(nome)
print(type(nome))

# \ Serve como caractere de escape

print(f'Teste de Upper: {nome.upper()}')
print(f'Teste de Lower: {nome.lower()}')
print(f'Teste de Split: {nome.split()}')

"""
[ 0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14 ]
['G', 'e', 'e', 'k', ' ', 'U', 'n', 'i', 'v', 'e', 'r', 's', 'i', 't', 'y']

Função Split -> Transforma string em uma lista
"""

nome = 'Geek University'
print(nome[0:3])  # Slice de string
print(nome[5:13])  # Slice de string

# Split de acordo com espaço
print(nome.split()[0])
print(nome.split()[1])

# Pegando partes específicas da string
print(nome[14], nome[12], nome[11])

# Inverte a ordem da string
print(nome[::-1])

# Trocar letras
print(nome.replace('e', 'u'))

texto = 'socorram me subi no onibus em marrocos'
print(texto[::-1])  # Palíndromo
