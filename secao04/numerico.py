"""
Tipo Numérico

+ = Soma
- = Subtração
* = Multiplicação
/ = Divisão
// = Divisão com retorno em valor inteiro
% = Divide os valores e retorna o resto
** = Elevado
1_000_000_000 = Retorna o valor sem _, permitindo visualizar de forma humana e apresentar de forma de máquina

num = 1
num + 1
num += 1 -> num = num + 1
num -= 1 -> num = num - 1
num *= 1 -> num = num * 1
num /= 1 -> num = num / 1
num //= 1 -> num = num // 1
num **= 1 -> num = num ** 1

type(num) -> Exibe o tipo da variável (int/float/string/etc)

dir(num)
help(num.__add__)

CTRL + l = Limpa console

"""

num = 1_000_000

print(num)

print(float(num))
