"""
Escopo de variáveis

Dois casos de escopo:

1 - Variáveis Globais
    São reconhecidas, ou seja, seu escopo compreende, todo o programa.

2 - Variáveis Locais
    São reconhecidas apenas no bloco onde foram declaradas, seu escopo está
    limitado ao bloco onde foi declarada.


Para declarar variáveis em Python fazemos:

nome_da_variavel = valor_da_variavel

Python é uma linguagem de tipagem dinâmica. Isso significa que ao declararmos uma
variável, não é informado o tipo de dado dela.
Este tipo é inferido ao atribuírmos o valor à mesma.

Exemplo em C:
int numero = 42;

Exemplo em Java:
int numero = 42;

Exemplo em Python:
numero = 42

É possível fazer com que uma mesma variável assuma diferentes tipos de acordo com
o código (reatribuição)

"""

# Exemplo de reatribuição (int/string)

numero = 42
print(numero)
print(type(numero))

numero = 'Geek'
print(numero)
print(type(numero))

# Exemplo de escopo de variável

numero = 42

# Exemplo onde novo não é criado - Gera falha no código
if numero < 10:
    novo = numero + 10
    print(novo)

print(novo)

# Exemplo onde novo é criado
if numero > 10:
    novo = numero + 10
    print(novo)

print(novo)
