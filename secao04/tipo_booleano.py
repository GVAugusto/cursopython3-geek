"""
Tipo Booleano

Álgebra Booleana, criada por George Boole

2 Constantes, Verdadeiro ou Falso

True -> Verdadeiro
False -> Falso

OBS: Sempre com a inicial maiúscula

Errado:
true, false

Certo:
True, False
"""

ativo = True

print(ativo)

"""
Operações básicas:
"""

# Negação (not)
"""
Fazendo a negação, se o valor booleano for verdadeiro o resultado será falso,
se for falso o resultado será verdadeiro. Ou seja, sempre o contrário.
"""

print(not ativo)


# Ou (or)
"""
É uma operação binária, ou seja, depende de dois valores. Ou um ou outro devem ser
verdadeiros
"""

print(f'Resultado para True OR True: {True or True}')
print(f'Resultado para True OR False: {True or False}')
print(f'Resultado para False OR True: {False or True}')
print(f'Resultado para False OR False: {False or False}')

# E (and)
"""
É uma operação binária, ou seja, depende de dois valores. Ambos os valores devem
ser verdadeiros.
"""

print(f'Resultado para True AND True: {True and True}')
print(f'Resultado para True AND False: {True and False}')
print(f'Resultado para False AND True: {False and True}')
print(f'Resultado para False AND False: {False and False}')

# Operações booleanas comparativas

num1 = 10
num2 = 8
num3 = 5

print(f'Comparação {num1} < {num2}: {num1 < num2}')
print(f'Comparação {num2} > {num3}: {num2 > num3}')
print(f'Comparação {num1} = {num2}: {num1 == num2}')
print(f'Comparação {num1} >= {num2}: {num1 >= num2}')
print(f'Comparação {num3} <= {num2}: {num3 <= num2}')
print(f'Comparação {num3} <= {num2} ou {num1} >= {num2}: {num3 <= num2 or num1 >= num2}')
print(f'Comparação {num3} <= {num2} e {num2} >= {num1}: {num3 <= num2 and num2 >= num1}')
