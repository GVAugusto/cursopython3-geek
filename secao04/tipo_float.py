"""
Tipo Float / Tipo Real / Decimal

Casa decimais

OBS: O separador de caasa decimais na programação é o "ponto" (.) e não vírgula (,)
"""

# Errado para gerar float, gera tuple
valor = 1, 44
print(valor)
print(type(valor))

# Certo
valor = 1.44
print(valor)
print(type(valor))

# É possível fazer dupla atribuição
valor1, valor2 = 1, 44
print(valor1)
print(type(valor1))
print(valor2)
print(type(valor2))

# Podemos converter float para int
res = int(valor)
print(res)
print(type(res))

# Podemos trabalhar com números complexos
variavel = 5j
print(variavel)
print(type(variavel))
