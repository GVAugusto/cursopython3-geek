"""
Ranges

 - Precisamos conhecer o loop for para usar os ranges.
 - Precisamos conhecer o range para trabalhar melhor com loops for.

Ranges são utilizados para gerar sequências numéricas, não de forma aleatória, mas
sim de maneira especificada.

Formas gerais:

 - Forma 1: range(valor_de_parada)
    Valor de parada não incluso no loop
    Início padrão 0
    Passo 1 em 1

 - Forma 2: range(valor_inicio, valor_parada)
    Valor de parada não incluso no loop
    Inicio especificado pelo usuário
    Passo 1 em 1

 - Forma 3: range(valor_inicio, valor_parada, passo)
    Valor de parada não incluso no loop
    Inicio especificado pelo usuário
    Passo especificado pelo usuário

 - Forma 4: range(valor_inicio, valor_parada, passo) - Forma 3 invertida
    Valor de parada não incluso no loop - deve ser menor que valor inicial
    Início especificado pelo usuário
    Passo especificado pelo usuário deve ser negativo
    Faz decremento do valor inicial de acordo com passo

"""

# Exemplo Forma 1
for num in range(11):
    print(num)

# Exemplo Forma 2
for num in range(1, 12):
    print(num)

# Exemplo Forma 3
for num in range(4, 13, 2):
    print(num)

# Exemplo Forma 4
for num in range(10, 2, -1):
    print(num)

# Exemplo de uso de range em listas
lista = list(range(10))

print(lista)
