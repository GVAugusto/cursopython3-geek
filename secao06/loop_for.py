"""
Loop for

Loop -> Estrutura de repetição
For -> Uma dessas estruturas

C ou JAVA

for(int i = 0; i < limitador; i++){
    //execução loop
}

Python

for item in interavel:
    //execução do loop

Utilizamos loops para iterar sobre sequências ou sobre valores iteráveis

Exemplos de iteráveis:
 - String
   nome = 'Geek University'
 - Lista
   lista = [1, 3, 5, 7, 9]
 - Range
   numeros = range(1, 10)
"""

nome = 'Geek University'
lista = [1, 3, 5, 7, 9]
numeros = range(1, 10) # Temos que transformar em uma lista

# Exemplo de for 1 (Iterando em uma string)
for letra in nome:
    print(f'{letra}')

# Exemplo de for 2 (Iterando em uma lista)
for numero in lista:
    print(f'{numero}')

# Exemplo de for 3 (Iterando sobre um range)
"""
range(valor_inicial, valor_final)
OBS: O valor final não é incluso.
"""
for numero in range(1, 10):
    print(f'{numero}')

"""
Enumerate:

(0, seq[0]), (1, seq[1]), (2, seq[2]), ...
(0, 'G'), (1, 'e'), (2, 'e'), (3, 'k'), ...
"""

# Exemplo enumerate (formato nome[indice])
for indice, letra in enumerate(nome):
    print(nome[indice])

# Exemplo enumerate (formato letra, igual item acima)
for indice, letra in enumerate(nome):
    print(letra)

# Exemplo enumerate (mostrando indice e letra juntos para ter ideia dos valores)
for indice, letra in enumerate(nome):
    print(f'{indice} - {letra}')

# Exemplo sem necessidade do índice (_ serve para descarte de valor)
"""
OBS: Quando não precisamos de um valor podemos descartá-lo utilizando _
"""
for _, letra in enumerate(nome):
    print(f'{indice} - {letra}')

# Exemplo para demonstração do formato enumerate puro
for valor in enumerate(nome):
    print(valor)

# Exemplo para demonstração do indice apenas
for valor in enumerate(nome):
    print(valor[0])

qtd = int(input('Quantas vezes o loop deve rodar: '))

# Exemplo de for com range
for n in range(1, qtd):
    print(f'Imprimindo {n}')

# Exemplo de for com range + 1 para rodar até valor desejado
for n in range(1, qtd+1):
    print(f'Imprimindo {n}')

# Exemplo de for para somar
soma = 0

for n in range(1, qtd+1):
    num = int(input(f'Informe o {n}/{qtd} valor: '))
    soma += num

print(f'A soma é {soma}')

# Exemplo de for com string, sem pular linha (alteração de padrão na função print)
"""
Segurar o CTRL e clicar na função permite acessar documentação da função completa
Uma espécie de manual, mais fácil que HELP
"""
for letra in nome:
    print(letra, end='')

"""
Funções de concatenação de string:
+ -> Concatena duas strings (nome + nome2) => nomenome2
* -> Concatena a string N vezes (nome * 3) => nomenomenome

Tabela de emojis Unicode:
https://apps.timwhitlock.info/emoji/tables/unicode

"""

# Apresentando string com emoji N vezes concatenada
# Original: U+1F60D
# Modificado: U0001F60D

emoji = '\U0001F60D'

for num in range(1, 11):
    print(f'{emoji * num}')

# Apresentando string com emoji concatenada N vezes por N vezes
# Original: U+1F605
# Modificado: U0001F605

emoji = '\U0001F605'

for _ in range(3):
    for num in range(1, 11):
        print(f'{emoji * num}')
