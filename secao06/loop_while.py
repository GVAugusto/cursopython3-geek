"""
Loop While

Forma geral:

while expressão_booleana:
    //execução do loop

O bloco do while será repetido enquanto a expressão booleana for verdadeira.

Expressão Booleana é toda expressão onde o resultado é verdadeiro ou falso.

Exemplo:
 - num = 5
 - num < 5

OBS: Em um loop while é importante que cuidemos do critério de parada. Caso as
condições não sejam atendidas, pode gerar loop infinito.

Em C ou JAVA:
while(expressão){
  // execução
}

# do while
do {

}while(expressão);

Python não possui do while

"""

# Exemplo 1

numero = 1

while numero < 10:
    print(numero)
    numero += 1

# Exemplo 2

resposta = ''

while resposta.lower() != 'sim':
    resposta = input('Já acabou Jéssica? ')

