"""
Recebendo dados do usuário
"""

# Entrada de dados
# Versao composta (mais longa)
# print("Qual seu nome? ")
# nome = input()

# Versao simplififaca
nome = input("Qual seu nome? ")

# Exemplo de print Python 2.x
# print("Seja bem-vindo(a) %s" % nome)

# Exemplo de print Python 3.x
# print("Seja bem-vindo(a) {0}".format(nome))

# Exemplo de print mais recente (3.7+)
print(f"Seja bem-vindo(a) {nome}")

# Versao composta (mais longa)
# print("Qual sua idade? ")
# idade = input()

idade = int(input("Qual sua idade? "))


# Processamento

# Saída de dados
# Exemplo de print Python 2.x
# print("A %s tem %s anos" % (nome, idade))

# Exemplo de print Python 3.x
# print("A {0} tem {1} anos".format(nome, idade))

# Exemplo de print mais recente (3.7+)
print(f"O(A) {nome} tem {idade} anos")

# Versao com cast no resultado
# print(f"O(A) {nome} nasceu em {2021 - int(idade)}")

# Versao com cast na captura do dado
print(f"O(A) {nome} nasceu em {2021 - idade}")
"""
int(idade) => cast

Cast é a 'conversão' de um tipo de dado para outro

input() => Todo dado recebido via input é do tipo String

Em Python, string é tudo o que estiver entre
 - Aspas simples -> 'Angelina Jolie';
 - Aspas duplas -> "Angelina Jolie";
 - Aspas simples triplas -> '''Angelina Jolie''';
"""
# - Aspas duplas triplas """Angelina Jolie""";
