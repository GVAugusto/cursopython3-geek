"""
Documentando Funções com Docstrings

Os comentários utilizando aspas triplas são Docstrings

odemos ter acesso à documentação de uma função em Python utilizando a propriedade
especial __doc__
"""

print(help(print))
print(print.__doc__)

# Exemplos


def diz_oi():
    """Uma função simples que retorna a string 'Oi!'"""
    return 'Oi!'


print(diz_oi())
print(help(diz_oi))
print(diz_oi.__doc__)


def exponencial(numero, potencia=2):
    """
    Função que retorna por padrão o quadrado de 'numero' ou 'numero' a 'potencia'
    informada
    :param numero: Número que se deseja calcular exponencial
    :param potencia: Potencia que queremos gerar o exponencial, por padrão 2
    :return: Retorna o exponencial de 'numero' por 'potencia'
    """
    return numero * potencia


print(exponencial(2))
print(help(exponencial))
print(exponencial.__doc__)
