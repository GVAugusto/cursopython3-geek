"""
Funções com Parâmetro Padrão (Default Parameters)

- Funções onde a passagem de parâmetro seja opcional

Porque utilizar parâmetros com valor default?
- Nos permite ser mais flexíveis nas funções;
- Evita erros com parâmetros incorretos;
- Nos permite trabalhar com exemplos mais legíveis de código;

Quais tipos de dados podemos utilizar como valores default para parâmetros?
- Qualquer tipo de dados:
    - Números, strings, floats, booleanos, listas, tuplas, dicionários, funções, etc

"""

# Exemplo de função onde a passagem de parâmetro é opcional

print('Geek University')
print()

# Exemplo de função onde a passagem de parâmetro é obrigatória - Gera TypeError


def quadrado(numero):
    return numero ** 2


print(quadrado(3))
# print(quadrado()) --> Gera TypeError

# Exemplo de parâmetro opcional


def exponencial(numero=4, potencia=2):
    return numero ** potencia


print(exponencial(2, 3))  # 2 * 2 * 2 = 8
print(exponencial(3, 2))  # 3 * 3 * 3 = 9
print(exponencial(3))  # Por padrão eleva ao quadrado
print(exponencial(3, 5))  # Eleva a quinta
print(exponencial())

"""
OBS

Se o usuário não passar nenhum parâmetro, irá utilizar o que já está atribuído
em ambas variáveis da função (numero=4, potencia=2), sendo o resultado 16

Se o usuário passar apenas um parâmetro, este será atribuído ao parâmetro número
sendo calculado o quadrado do número (valor padrão potencia=2)

Se o usuário passar dois argumentos, o primeiro será atribuído ao parâmetro número
e o segundo ao parâmetro potencia, sendo então calculada a potencia desejada

-------
Em funções Python, os parâmetros com valores default (padrão) DEVEM
sempre estar ao final da declaração

ERRO!

def teste(num=2, potencia):
    return num ** potencia

Gera erro, pois o valor num=2 deve ser colocado como segundo parâmetro neste caso 
"""

# Outros exemplos


def soma(num1=5, num2=2):
    return num1 + num2


print(soma(4, 3))
print(soma(4))  # --> TypeError
print(soma())  # --> TypeError


# Exemplo mais complexo


def mostra_informacao(nome='Geek', instrutor=False):
    if nome == 'Geek' and instrutor:
        return 'Bem-vindo instrutor Geek!'
    elif nome == 'Geek':
        return 'Eu pensei que você era o instrutor'
    return f'Olá {nome}'


print(mostra_informacao())
print(mostra_informacao(instrutor=True))
print(mostra_informacao(True))
print(mostra_informacao('Ozzy'))
print(mostra_informacao(nome='Stephany'))


# Exemplos de uso de função em outra função

def soma(num1, num2):
    return num1 + num2


def subtracao(num1, num2):
    return num1 - num2


def mat(num1, num2, fun=soma):
    return fun(num1, num2)


print(mat(2, 3))
print(mat(2, 2, subtracao))


# Escopo - Evitar problemas e confusões

# Variáveis Globais
# Variáveis Locais

instrutor = 'Geek'  # Variável Global - não faz parte do escopo de nenhuma variável


def diz_oi():
    instrutor = 'Python'  # Variável Local - sobrepõe a variável Global
    return f'Oi {instrutor}'


print(diz_oi())

# Exemplo de restrição de variável local


def diz_oi():
    prof = 'Geek'  # Variável Local
    return f'Olá {prof}'


print(diz_oi())
# Sistema não reconhece variável prof que está declarada apenas na função
# print(prof)

"""
ATENÇÃO com variáveis globais (Se puder evitar, evite!)

total = 0


def incrementa():
    # Função incrementa não reconhece variável global, sendo necessário declarar
    total += 1
    return total


print(incrementa())
"""

total = 0


def incrementa():
    global total # Informa o uso da variável Global previamente declarada
    total += 1
    return total


print(incrementa())  # 1
print(incrementa())  # 2
print(incrementa())  # 3

# Podemos ter funções que são declaradas dentro de funções
# Possui uma forma especial de escopo de variável


def fora():
    contador = 0

    def dentro():
        nonlocal contador  # Variável não global, recebida da função anterior
        contador += 1
        return contador
    return dentro()


print(fora())
print(fora())
print(fora())

# print(dentro()) --> Função não é reconhecida fora da função "fora()"
