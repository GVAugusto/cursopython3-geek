"""
Funções com parâmetro (de entrada)

- Funções que recebem dados para serem processados dentro da mesma

Se pensarmos em um programa qualquer, geralmente temos:

entrada -> processamento -> saída

Se pensarmos em uma função, já sabemos que temos funções que:
- Não possuem entrada;
- Não possuem saída;
- Possuem entrada mas não possuem saída;
- Não possuem entrada mas possuem saída;
- Possuem entrada e saída;
"""

# Refatorando uma função


def quadrado_de_7():
    return 7 * 7


print(quadrado_de_7())


def quadrado(numero):
    # return numero * numero
    return numero ** 2


print(quadrado(7))
print(quadrado(2))
print(quadrado(5))

ret = quadrado(6)
print(ret)

# print(quadrado()) -> Gera erro TypeError sem parâmetro informado

# Refatorando a função


def cantar_parabens(aniversariante):
    print('Parabéns pra você')
    print('Nesta data querida')
    print('Muitas felicidades')
    print('Muitos anos de vida')
    print(f'Viva o/a {aniversariante}!')


cantar_parabens('Marcos')
cantar_parabens('Patrícia')

"""
Funções podem ter N parâmetros de entrada, ou seja, podemos receber como entrada
em uma função quantos parâmetros forem necessários. Eles são separados por vírguola.
"""

# Exemplo


def soma(a, b):
    return a + b


def multiplica(a, b):
    return a * b


def outra(num1, b, msg):
    return (num1 + b) * msg


print(soma(2, 5))
print(soma(10, 20))

print(multiplica(4, 5))
print(multiplica(2, 8))

print(outra(3, 2, 'Geek '))
print(outra(5, 4, 'Python '))

# OBS: Se informarmos um número errado de parâmetros ou argumentos, teremos TypeError
# print(soma(2, 3, 4)) -> TypeError
# print(soma(4)) -> TypeError

# Nomeando parâmetros


def nome_completo(nome, sobrenome):
    return f'Seu nome completo é {nome} {sobrenome}'


print(nome_completo('Angelina', 'Jolie'))

"""
A diferença entre Parâmetros e Argumentos

Parâmetros são variáveis declaradas na definição de uma função;
Argumentos são dados passados durante a execução de uma função;
"""

# A ordem dos parâmetros importa

nome = 'Felicity'
sobrenome = 'Jones'

print(nome_completo(nome, sobrenome))
print(nome_completo(sobrenome, nome))

"""
Argumentos nomeados (Keyword Arguments)

Caso utilizemos nomes dos parâmetros nos argumentos para informá-los, podemos
utilizar qualquer ordem
"""

print(nome_completo(nome='Angelina', sobrenome='Jolie'))
print(nome_completo(sobrenome='Jolie', nome='Angelina'))
print(nome_completo(sobrenome='Marques', nome='Marcia'))


# Erro comum na utilização do return


def soma_impares(numeros):
    total = 0
    for num in numeros:
        if num % 2 != 0:
            total += num
        # return total --> Nesta posição função executa menos do que deveria
    return total


lista = [1, 2, 3, 4, 5, 6, 7]
print(soma_impares(lista))

tupla = (1, 2, 3, 4, 5, 6, 7)
print(soma_impares(tupla))
