"""
Funções com retorno

Em Python quando uma função não retorna nenhum valor, o retorno é None.

Funções Python que retornam valores devem retornar estes valores com a palavra
reservada "return"

Não precisamos necessariamente criar uma variável para receber o retorno de uma
função, possamos passar a execução da função para outras funções

Sobre a palavra reservada return:

1. Ela finaliza a função, ou seja, ela sai da execução da função;
2. Podemos ter em uma função diferentes returns;
3. Podemos, em uma função, retornar qualquer tipo de dado, e até mesmo múltiplos
    valores
"""

numeros = [1, 2, 3]
ret_pop = numeros.pop()
print(f'Retorno de pop: {ret_pop}')
ret_pr = print(numeros)
print(f'Retorno de Print: {ret_pr}')

# Exemplo função


def quadrado_de_7():
    print(7 * 7)


ret = quadrado_de_7()
print(f'Retorno de Quadrado de 7: {ret}')

# Refatorando a função para gerar retorno


def quadrado_de_7():
    return 7 * 7


# Criamos uma variável para receber o retorno da função e então imprimir
ret = quadrado_de_7()
print(f'Retorno de Quadrado de 7: {ret}')

# É possível utilizar diretamente em outra função
print(f'Retorno de Quadrado de 7: {quadrado_de_7()}')

# Refatorando a primeira função


def diz_oi():
    return 'Oi '


alguem = 'Pedro'
print(diz_oi() + alguem + '!')

# Exemplo 1 - Sobre Return


def diz_oi():
    print('Estou sendo executado antes do retorno!')
    return 'Oi'
    print('Estou sendo executado após o retorno!')


print(diz_oi())

# Exemplo 2 - Sobre Return


def nova_funcao():
    # variavel = True
    # variavel = None
    variavel = False
    if variavel:
        return 4
    elif variavel is None:
        return 3.2
    return 'b'


print(nova_funcao())

# Exemplo 3 - Sobre Return


def outra_funcao():
    return 2, 3, 4, 5


num1, num2, num3, num4 = outra_funcao()

print(num1, num2, num3, num4)
print(outra_funcao())
print(type(outra_funcao()))


# Vamos criar uma função para jogar a moeda

from random import random


def joga_moeda():
    # Gera um número pseudo randomico entre 0 e 1
    valor = random()
    print(valor)
    if valor > 0.5:
        return 'Cara'
    return 'Coroa'


print(joga_moeda())

"""
Chamar minhas funções:

from funcoes_com_retorno import joga_moeda
"""

# Erros comuns na utilização do retorno, ou códigos desnecessários


def e_impar():
    # numero = 6
    numero = 5
    if numero % 2 != 0:
        return True
    # Else desnecessário
    return False


print(e_impar())
