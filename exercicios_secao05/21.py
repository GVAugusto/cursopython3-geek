"""
21. Escreva o menu de opções abaixo. Leia a opção do usuário e execute a operação
escolhida. Escreva uma mensagem de erro se a opção for inválida.
"""

operacao = int(input('Escolha uma das opções:\n\n'
                     '1 - Soma de dois números\n'
                     '2 - Diferença entre dois números (maior pelo menor)\n'
                     '3 - Produto entre dois números\n'
                     '4 - Divisão entre dois números (o denominador não pode ser 0)\n'
                     'Opção: '))

if 1 <= operacao <= 4:
    num1 = float(input('Insira o primeiro número: '))
    num2 = float(input('Insira o segundo número: '))

    resultado = 0

    if operacao == 2:
        if num1 >= num2:
            resultado = num1 - num2
        else:
            resultado = num2 - num1

    if operacao == 4:
        if num2 == 0:
            print('Favor informar um denominador diferente de 0.')
            exit()

    print({
        1: num1 + num2,
        2: resultado,
        3: num1 * num2,
        4: num1 / num2,
    }[operacao])

else:
    print('Favor selecionar uma opção válida')
