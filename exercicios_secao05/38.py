"""
38. Leia uma data de nascimento de uma pessoa fornecida através de três números
inteiros: Dia, Mês e Ano. Teste a validade desta data para saber se esta é uma
data válida. Teste se o dia fornecida é válida:

 - dia > 0
 - dia <= 28 para o mês de fevereiro (29 se o ano for bissexto)
 - dia <= 30 em abril, junho, setembro e novembro
 - dia <= 31 nos outros meses

 Teste a validade do Mês:

 - mês > 0
 - mês < 13

 Teste a validade do ano:

 - ano <= ano atual (use uma constante definida com o valor igual a 2020)

 Imprimir: "data válida" ou "data inválida" no final da execução do programa.
"""


def check_dias_mes(x):
    return {
        1: 31,
        2: 28,
        '2b': 29,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31,
    }[x]


dia = int(input('Insira um dia entre 1 e 31: '))
mes = int(input('Insira um mês entre 1 e 12: '))
ano = int(input('Insira um ano menor que o atual (2020): '))

if 1 <= mes <= 12:

    if 0 <= ano <= 2020:

        div400 = ano % 400
        div4 = ano % 4
        div100 = ano % 100

        if div400 == 0 or (div4 == 0 and div100 != 0):
            if mes == 2:
                dias_total = check_dias_mes('2b')
            else:
                dias_total = check_dias_mes(mes)

        else:
            dias_total = check_dias_mes(mes)

        if 1 <= dia <= dias_total:
            print(f'Data válida')

        else:
            print('Data inválida')

    else:
        print('Ano inválido')

else:
    print('Mês inválido')
