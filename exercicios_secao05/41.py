"""
41. Faça um algoritmo que calcule o IMC de uma pessoa e mostre sua classificação de
acordo com a tabela abaixo:

    IMC             Classificação
    < 18.5          Abaixo do peso
    18.6 - 24.9     Saudável
    25.0 - 29.9     Peso em excesso
    30.0 - 34.9     Obesidade Grau I
    35.0 - 39.9     Obesidade Grau II (severa)
    >= 40.0         Obesidade Grau III (mórbida)
"""

altura = float(input('Insira a altura da pessoa em m: '))
peso = float(input('Insira o peso da pessoa em Kg: '))

imc = peso / (altura ** 2)

if imc <= 18.5:
    print(f'Abaixo do peso. IMC: {imc:.2f}')
elif imc <= 24.9:
    print(f'Saudável. IMC: {imc:.2f}')
elif imc <= 29.9:
    print(f'Peso em excesso. IMC: {imc:.2f}')
elif imc <= 34.9:
    print(f'Obesidade Grau I. IMC: {imc:.2f}')
elif imc <= 39.9:
    print(f'Obesidade Grau II (severa). IMC: {imc:.2f}')
else:
    print(f'Obesidade Grau III (mórbida). IMC: {imc:.2f}')
