"""
18. Faça um programa que mostre ao usuário um menu com 4 opções de operações
matemáticas (as básicas, por exemplo). O usuário escolhe uma das opções e o seu
programa então pede dois valores numéricos e realiza a operação, mostrando o
resultado e saindo.
"""

operacao = int(input('Escolha uma das opções:\n\n'
                     '1 - Soma de dois números\n'
                     '2 - Subtração entre dois números\n'
                     '3 - Multiplicação entre dois números\n'
                     '4 - Divisão entre dois números\n'
                     'Opção: '))

if 1 <= operacao <= 4:
    num1 = float(input('Insira o primeiro número: '))
    num2 = float(input('Insira o segundo número: '))
    print({
        1: num1 + num2,
        2: num1 - num2,
        3: num1 * num2,
        4: num1 / num2,
    }[operacao])
else:
    print('Favor selecionar uma opção válida')
