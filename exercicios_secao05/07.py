"""
07. Faça um programa que receba dois números e mostre o maior. Se por acaso os dois
forem iguais, imprima a mensagem Números iguais.
"""

num1 = int(input('Insira um número: '))
num2 = int(input('Insira outro número: '))

if num1 < num2:
    print(f'O maior número é {num2}')
elif num2 < num1:
    print(f'O maior número é {num1}')
else:
    print(f'Ambos os números são iguais')
