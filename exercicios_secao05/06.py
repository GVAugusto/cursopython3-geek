"""
06. Escreva um programa que, dados dois números inteiros, mostre na tela o maior
deles, assim como a diferença existente entre ambos.
"""

num1 = int(input('Insira um número: '))
num2 = int(input('Insira outro número: '))

if num1 < num2:
    print(f'O maior número é {num2} sendo a diferença de {num2 - num1}')
elif num2 < num1:
    print(f'O maior número é {num1} sendo a diferença de {num1 - num2}')
else:
    print(f'Ambos os números são iguais')
