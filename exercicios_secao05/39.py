"""
39. Uma empresa decide dar um aumento aos seus funcionários de acordo com uma
tabela que considera o salário atual e o tempo de serviço de cada funcionário.
Os funcionários com menor salário terão um aumento proporcionalmente maior do que
os funcionários com um salário maior, e conforme o tempo de serviço na empresa, cada
funcionário irá receber um bônus adicional de salário. Faça um programa que leia:

 - o valor do salário atual do funcionário;
 - o tempo de serviço desse funcionário na empresa (número de anos de trabalho na
 empresa)

Use as tabelas abaixo para calcular o salário reajustado deste funcionário e
imprima o valor do salário final reajustado, ou uma mensagem caso o funcionário
não tenha direito a nenhum aumento.

    Salário Atual       Reajuste (%)        Tempo de Serviço        Bônus
    Até 500.00              25%              Abaixo de 1 ano        Sem bônus
    Até 1000.00             20%              De 1 a 3 anos          100.00
    Até 1500.00             15%              De 4 a 6 anos          200.00
    Até 2000.00             10%              De 7 a 10 anos         300.00
    Acima de 2000.00     Sem reajuste        Mais de 10 anos        500.00
"""

salario_atual = float(input('Favor inserir o valor do salário atual: '))
tempo_servico = float(input('Favor inserir tempo de trabalho em anos: '))

if salario_atual <= 500.0:
    salario_novo = salario_atual * 1.25
elif salario_atual <= 1000.0:
    salario_novo = salario_atual * 1.20
elif salario_atual <= 1500.0:
    salario_novo = salario_atual * 1.15
elif salario_atual <= 2000.0:
    salario_novo = salario_atual * 1.10
else:
    salario_novo = salario_atual


if tempo_servico <= 1.0:
    print(f'Novo salário: R${salario_novo:.2f}')
elif tempo_servico <= 3.0:
    salario_novo += 100.0
    print(f'Novo salário: R${salario_novo:.2f}')
elif tempo_servico <= 6.0:
    salario_novo += 200.0
    print(f'Novo salário: R${salario_novo:.2f}')
elif tempo_servico <= 10.0:
    salario_novo += 300.0
    print(f'Novo salário: R${salario_novo:.2f}')
else:
    salario_novo += 500.0
    print(f'Novo salário: R${salario_novo:.2f}')
