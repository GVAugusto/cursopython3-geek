"""
02. Leia um número fornecido pelo usuário. Se esse número for positivo, calcule a
raíz quadrada do número. Se o número for negativo, mostre uma mensagem dizendo que
o número é inválido.
"""

num = float(input('Insira um número: '))

if num >= 0:
    print(f'{num ** (1/2)}')
else:
    print('Número inválido, necessário informar um número positivo')
