"""
30. Faça um programa que receba três números e mostre-os em ordem crescente.
"""

numeros = [float(input('Insira um número: ')),
           float(input('Insira um número: ')),
           float(input('Insira um número: '))]

numeros.sort()
print(f'\n{numeros}')

"""

Opção básica

num1 = float(input('Insira um número: '))
num2 = float(input('Insira um número: '))
num3 = float(input('Insira um número: '))

numeros = [num1, num2, num3]

print(f'Ordem crescente: {sorted(numeros)}')

"""