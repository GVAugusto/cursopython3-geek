"""
17. Faça um programa que calcule e mostre a área de um trapézio. Sabe-se que:

 A = ( (basemaior + basemenor) * altura ) / 2

Lembre-se a base maior e a base menor devem ser números maiores que zero.
"""

base1 = float(input('Favor inserir o valor da base 1: '))
base2 = float(input('Favor inserir o valor da base 2: '))
altura = float(input('Favor inserir o valor da altura: '))

if base1 < 0.0 or base2 < 0.0:
    print('Valores das bases devem ser positivos, favor entrar corretamente')
else:
    area = ((base1 + base2) * altura) / 2
    print(f'Área do trapézio é: {area}')
