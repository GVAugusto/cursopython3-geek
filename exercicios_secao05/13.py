"""
13. Faça um algoritmo que calcule a média ponderada das notas de 3 provas. A primeira
e a segunda prova têm peso 1 e a terceira tem peso 2. Ao final mostrar a média do aluno
e indicar se o aluno foi aprovado ou reprovado. A nota para aprovação deve ser
igual ou superior a 60 pontos.
"""

nota1 = float(input('Favor informar a primeira nota: '))
nota2 = float(input('Favor informar a segunda nota: '))
nota3 = float(input('Favor informar a terceira nota: '))

if 0.0 <= nota1 <= 10.0 and 0.0 <= nota2 <= 10.0 and 0.0 <= nota3 <= 10.0:
    media = (((nota1 + nota2) + (nota3 * 2)) / 4) * 10
    if media >= 60:
        print(f'Aluno aprovado com {media} pontos')
    else:
        print(f'Aluno reprovado com {media} pontos')
else:
    print('Favor informar notas entre 0.0 e 10.0')
