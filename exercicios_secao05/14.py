"""
14. A nota final de um estudante é calculada a partir de três notas atribuídas entre
o intervalo de 0 até 10, respectivamente, a um trabalho de laboratório, a uma
avaliação semestral e a um exame final. A média das três notas mencionadas
anteriormente obedece aos pesos:

 - Trabalho de laboratório: 2
 - Avaliação Semestral: 3
 - Exame Final: 5

De acordo com o resultado, mostre na tela se o aluno está reprovado (média entre
0 e 2.9), de recuperação (entre 3 e 4.9) ou se foi aprovado. Faça todas as
verificações necessárias.
"""

nota1 = float(input('Favor informar a nota do Trabalho de Laboratório: '))
nota2 = float(input('Favor informar a nota da Avaliação Semestral: '))
nota3 = float(input('Favor informar a nota do Exame Final: '))

if 0.0 <= nota1 <= 10.0 and 0.0 <= nota2 <= 10.0 and 0.0 <= nota3 <= 10.0:
    media = ((nota1 * 2) + (nota2 * 3) + (nota3 * 5)) / 10
    if media < 3:
        print(f'Aluno reprovado - Média: {media}')
    elif 3 <= media < 5:
        print(f'Aluno em recuperação - Média: {media}')
    else:
        print(f'Aluno aprovado - Média {media}')
else:
    print('Favor informar notas entre 0.0 e 10.0')
