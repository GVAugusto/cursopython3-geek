"""
22. Leia a idade e o tempo de serviço de um trabalhador e escrva se ele pode ou não
se aposentar. As condições para aponsetadoria são:

 - Ter pelo menos 65 anos;
 - Ou ter trabalhado pelo menos 30 anos;
 - Ou ter pelo menos 60 anos e trabalhado pelo menos 25 anos.

"""

idade = int(input('Favor informar sua idade em anos: '))
tempo = float(input('Favor informar seu tempo de serviço: '))

if idade >= 65:
    print('É possível se aposentar por idade')
elif tempo >= 30:
    print('É possível se aposentar por tempo de contribuição')
elif idade >= 60 and tempo >= 25:
    print('É possível se aposentar pela combinação de idade e tempo de contribuição')
else:
    print('Não é possível se aposentar no momento')
