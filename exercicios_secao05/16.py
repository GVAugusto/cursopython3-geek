"""
16. Usando switch, escreva um programa que leia um inteiro entre 1 e 12 e imprima
o mês correspondente a este número. Isto é, janeiro se 1, fevereiro se 2 e
assim por diante.
"""

num = int(input('Insira um número de 1 a 12: '))

if 1 <= num <= 12:
    print({
        1: "Janeiro",
        2: "Fevereiro",
        3: "Março",
        4: "Abril",
        5: "Maio",
        6: "Junho",
        7: "Julho",
        8: "Agosto",
        9: "Setembro",
        10: "Outubro",
        11: "Novembro",
        12: "Dezembro",
    }[num])
