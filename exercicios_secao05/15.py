"""
15. Usando switch, escreva um programa que leia um inteiro entre 1 e 7 e iprima o
dia da semana correspondente a este número. Isto é, domingo se 1, segunda-feira se 2,
e assim por diante.
"""

num = int(input('Insira um número de 1 a 7: '))

if 1 <= num <= 7:
    print({
        1: "Domingo",
        2: "Segunda-feira",
        3: "Terça-feira",
        4: "Quarta-feira",
        5: "Quinta-feira",
        6: "Sexta-feira",
        7: "Sábado",
    }[num])
