"""
10. Faça um programa que receba a altura e o sexo de uma pessoa e calcule e mostre
seu peso ideal, utilizando as seguintes fórmulas (onde h corresponde à altura):

 - Homens -> (72.7 * h) - 58
 - Mulheres -> (62.1 * h) - 44.7
"""

sexo = input('Favor informar seu sexo (M/F): ')
altura = float(input('Favor informar sua altura (m): '))

if sexo == 'M' or sexo == 'm':
    peso = (72.7 * altura) - 58
    print(f'Peso ideal: {peso:.2f}')
elif sexo == 'F' or sexo == 'f':
    peso = (62.1 * altura) - 44.7
    print(f'Peso ideal: {peso:.2f}')
else:
    print('Opção informada inexistente, favor informar F para Feminino ou M para '
          'Masculino.')
