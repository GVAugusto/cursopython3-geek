"""
28. Faça um programa que leia três números inteiros positivos e efetue o cálculo de
uma das seguintes médias de acordo com um valor numérico digitado pelo usuário:

 A: Geométrica: (x * y * z) ^ 1/3
 B: Ponderada: ((x+2) * (y+3) * z) / 6
 C: Harmônica: 1 / ((1/x) + (1/y) + (1/z))
 D: Aritmética: (x + y + z) / 3
"""

operacao = int(input('Escolha uma das opções de cálculo de média:\n\n'
                     '1 - Geométrica\n'
                     '2 - Ponderada\n'
                     '3 - Harmônica\n'
                     '4 - Aritmética\n'
                     'Opção: '))

if 1 <= operacao <= 4:
    num1 = float(input('Insira o primeiro valor: '))
    num2 = float(input('Insira o segundo valor: '))
    num3 = float(input('Insira o terceiro valor: '))
    print({
        1: (num1 * num2 * num3) ** (1/3),
        2: ((num1 + 2) * (num2 + 3) * num3) / 6,
        3: 1 / ((1/num1) + (1/num2) + (1/num3)),
        4: (num1 + num2 + num3) / 3,
    }[operacao])
else:
    print('Favor selecionar uma opção válida')
