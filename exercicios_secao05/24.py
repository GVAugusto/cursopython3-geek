"""
24. Uma empresa vende o mesmo produto para quatro diferentes estados. Cada estado
possui uma taxa diferente de imposto sobre o produto:
 - MG: 7%
 - SP: 12%
 - RJ: 15%
 - MS: 8%

Faça um programa em que o usuário entre com o valor e o estado destino do produto e
o programa retorne o preço final do produto acrescido do imposto do estado em que
ele será vendido. Se o estado digitado não for válido, mostrar uma mensagem de erro.
"""

produto = float(input('Insira o valor do produto: '))
estado = input('Insira o estado de destino do produto (MG/SP/RJ/MS): ')

if estado.upper() == 'MG':
    print(f'Valor final do produto é: {produto * 1.07:.2f}')
elif estado.upper() == 'SP':
    print(f'Valor final do produto é: {produto * 1.12:.2f}')
elif estado.upper() == 'RJ':
    print(f'Valor final do produto é: {produto * 1.15:.2f}')
elif estado.upper() == 'MS':
    print(f'Valor final do produto é: {produto * 1.08:.2f}')
else:
    print('Estado inserido não listado, favor inserir entrada corretamente')
