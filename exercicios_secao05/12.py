"""
12. Ler um número inteiro. Se o número for negativo, escreva a mensagem "Número
inválido". Se o número for positivo, calcular o logaritmo deste número.
"""

import math

num = int(input('Informe um número inteiro: '))

if num > 0:
    print(f'Logaritmo de {num} é: {math.log(num)}')
else:
    print('Número inválido')
