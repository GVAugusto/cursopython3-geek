"""
40. O custo ao consumidor de um carro novo é a soma do custo da fábrica, da comissão
do distribuidor, e dos impostos. A comissão e os impostos são calculados sobre o
custo de fábrica, de acordo com a tabela abaixo. Leia o custo de fábrica e escreva o
custo ao consumidor.

    CUSTO DE FÁBRICA            % DO DISTRIBUIDOR           % DOS IMPOSTOS
    até R$12.000,00                     5                       isento
    entre R$12.000,00 e R$25.000,00     10                        15
    acima de R$25.000,00                15                        20
"""

custo_fabrica = float(input('Insira o custo de fábrica do veículo: '))

if custo_fabrica <= 12000:
    custo_consumidor = custo_fabrica * 1.05
elif custo_fabrica <= 25000:
    custo_consumidor = (custo_fabrica * 1.10) + (custo_fabrica * 0.15)
else:
    custo_consumidor = (custo_fabrica * 1.15) + (custo_fabrica * 0.20)

print(f'Custo ao consumidor: R${custo_consumidor:.2f}')
