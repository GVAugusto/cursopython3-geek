"""
31. Faça um programa que receba a altura e o peso de uma pessoa. De acordo com a
tabela a seguir, verifique e mostra qual a classificação dessa pessoa:

    Altura                              Peso
                        Até 60      Entre 60 e 90 (inclusive)   Acima de 90
    Menor que 1,20          A               D                       G
    De 1,20 até 1,70        B               E                       H
    Maior que 1,70          C               F                       I
"""

altura = float(input('Informa sua altura: '))
peso = float(input('Informe seu peso: '))


if altura < 1.20:
    if peso < 60:
        print('Classificação A')
    elif 60 <= peso <= 90:
        print('Classificação D')
    else:
        print('Classificação G')

elif 1.20 <= altura <= 1.70:
    if peso < 60:
        print('Classificação B')
    elif 60 <= peso <= 90:
        print('Classificação E')
    else:
        print('Classificação H')

else:
    if peso < 60:
        print('Classificação C')
    elif 60 <= peso <= 90:
        print('Classificação F')
    else:
        print('Classificação I')
