"""
25. Calcule as raízes da equação de 2 grau, lembrando que:
x = ( -b +- delta ^ 1/2) / 2a
Onde
delta = B2 - 4ac

Em ax2 + bx + c = 0 representa uma equação de 2 grau.

A variável a tem que ser diferente de zero. Caso seja igual, imprima a mensagem:
"Não é equação de segundo grau"

 - Se delta < 0, não existe real. Imprima a mensagem: "Não existe raíz"
 - Se delta = 0, existe uma raíz real. Imprima a raíz e a mensagem "Raíz única"
 - Se delta >= 0, imprima as duas raízes reais

"""

valor_a = float(input('Favor inserir valor de a: '))
valor_b = float(input('Favor inserir valor de b: '))
valor_c = float(input('Favor inserir valor de c: '))

if valor_a != 0:

    delta = (valor_b ** 2) - (4 * valor_a * valor_c)
    valor_x1 = ((-valor_b) + (delta ** 0.5)) / (2 * valor_a)
    valor_x2 = ((-valor_b) - (delta ** 0.5)) / (2 * valor_a)

    if delta < 0:
        print('Não existe raíz')
    elif delta == 0:
        print(f'Raíz única: {valor_x1}')
    else:
        print(f'Duas raízes: {valor_x1} e {valor_x2}')

else:
    print('Não é equação de segundo grau')
