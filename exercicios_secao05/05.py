"""
05. Faça um programa que receba um número inteiro e verifique se este número é
par ou ímpar.
"""

num = int(input('Insira um número: '))

check = num % 2

if (num % 2) == 1:
    print(f'Número {num} é ímpar.')
elif check == 0:
    print(f'Número {num} é par.')
