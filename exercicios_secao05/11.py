"""
11. Escreva um programa que leia um número inteiro maior que zero e devolva, na tela,
a soma de todos os seus algarismos. Por exemplo, ao número 251 corresponderá o valor
8 (2+5+1). Se o número lido não for maior do que zero, o programa terminará com a
mensagem "Número inválido"
"""

soma = 0

num = int(input('Favor informar um número inteiro: '))

if num >= 0:
    for i in str(num):
        soma += int(i)
    print(f'Segue soma dos números: {soma}')
else:
    print('Número inválido')
