"""
19. Faça um programa para verificar se um determinado número inteiro e divisível por
3 ou por 5, mas não simultaneamente pelos dois.
"""

num = int(input('Insira um número: '))

teste3 = num % 3
teste5 = num % 5

if teste3 == 0 or teste5 == 0:
    if teste3 == 0 and teste5 == 0:
        print('Valor divisível por 3 e 5')
    else:
        print('Valor divisivel por 3 ou 5, mas não por ambos')
