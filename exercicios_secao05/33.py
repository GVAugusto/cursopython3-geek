"""
33. Um produto vai sofrer aumento de acordo com a tabela abaixo. Leia o preço antigo
calcule e escreva o preço novo, e escreva uma mensagem em função do preço novo (de
acordo com a segunda tabela)

Tabela 1:
    Preço Antigo        Percentual de Aumento
    até R$50                5%
    entre R$50 e R$100      10%
    acima de R$100          15%

Tabela 2:
    Preço Novo              Mensagem
    até R$80                Barato
    entre R$80 e R$120      Normal
    entre R$120 e R$200     Caro
    acima de R$200          Muito caro
"""

preco_atual = float(input('Insira o preço atual do produto: '))

if preco_atual < 50.0:
    preco_novo = preco_atual * 1.05
    print(f'Novo preço: {preco_novo:.2f}')
elif 50.0 <= preco_atual <= 100.0:
    preco_novo = preco_atual * 1.10
    print(f'Novo preço: {preco_novo:.2f}')
else:
    preco_novo = preco_atual * 1.15
    print(f'Novo preço: {preco_novo:.2f}')

if preco_novo < 80.0:
    print('Barato')
elif 80.0 <= preco_novo <= 120.0:
    print('Normal')
elif 120.0 <= preco_novo <= 200.0:
    print('Caro')
else:
    print('Muito caro')
