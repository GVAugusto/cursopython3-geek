"""
26. Leia a distância em Km e a quantidade de litros de gasolina consumidos por um
carro em um percurso, calcule o consumo em Km/l e escreva uma mensagem de acordo
com a tabela abaixo:

 CONSUMO    KM/L    MENSAGEM
 menor que  8       Venda o carro!
 entre      8 e 14  Econômico!
 maior que  12      Super econômico!
"""

distancia = float(input('Favor inserir a distância em Km: '))
litros = float(input('Favor inserir a quantidade de litros de gasolina: '))

consumo = distancia / litros

if consumo < 8:
    print(f'Venda o carro!')
elif 8 <= consumo <= 12:
    print(f'Econômico!')
else:
    print(f'Super econômico!')
