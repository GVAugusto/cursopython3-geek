"""
36. Escreva um programa que, dado o valor da venda, imprima a comissão que deverá
ser paga ao vendedor. Para calcular a comissão, considere a tabela abaixo:

 Venda mensal                                            Comissão
 Maior ou igual a R$100.000,00                           R$700,00 + 16% das vendas
 Menor que R$100.000,00 e maior ou igual a R$80.000,00   R$650,00 + 14% das vendas
 Menor que R$80.000,00 e maior ou igual a R$60.000,00    R$600,00 + 14% das vendas
 Menor que R$60.000,00 e maior ou igual a R$40.000,00    R$550,00 + 14% das vendas
 Menor que R$40.000,00 e maior ou igual a R$20.000,00    R$500,00 + 14% das vendas
 Menor que R$20.000,00                                   R$400,00 + 14% das vendas
"""

valor_venda = float(input('Informar o valor da venda: '))

if valor_venda >= 100000.00:
    valor_total = 700.00 + (valor_venda * 0.16)
    print(f'Comissão: {valor_total:.2f}')

elif 80000 <= valor_venda < 100000.00:
    valor_total = 650.00 + (valor_venda * 0.14)
    print(f'Comissão: {valor_total:.2f}')

elif 60000 <= valor_venda < 80000.00:
    valor_total = 600.00 + (valor_venda * 0.14)
    print(f'Comissão: {valor_total:.2f}')

elif 40000 <= valor_venda < 60000.00:
    valor_total = 550.00 + (valor_venda * 0.14)
    print(f'Comissão: {valor_total:.2f}')

elif 20000 <= valor_venda < 40000.00:
    valor_total = 500.00 + (valor_venda * 0.14)
    print(f'Comissão: {valor_total:.2f}')

elif valor_venda < 20000.00:
    valor_total = 400.00 + (valor_venda * 0.14)
    print(f'Comissão: {valor_total:.2f}')
