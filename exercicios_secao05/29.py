"""
29. Faça uma prova de matemática para crianças que estão aprendendo a somar números
inteiros menores do que 100. Escolha números aleatórios entre 1 e 100, e mostre
na tela a pergunta: qual é a soma de A + B, onde A e B são os números aleatórios.
Peça a resposta. Faça cinco perguntas ao aluno, e mostre para ele as perguntas e
as respostas corretas, além de quantas vezes o aluno acertou.
"""

import random

i = 0
certa = 0
errada = 0
texto_fim = ""

while i < 5:
    num1 = random.randrange(0, 100)
    num2 = random.randrange(0, 100)

    resposta_certa = num1 + num2

    resposta = float(input(f'Qual a soma de {num1} + {num2}? '))

    if resposta == resposta_certa:
        certa += 1
        texto_fim += f"{num1} + {num2} = {resposta_certa} - Acertou\n"
    else:
        errada += 1
        texto_fim += f"{num1} + {num2} = {resposta_certa} - Errou - Resposta dada: " \
                     f"{resposta}\n"
    i += 1

print(f'\nVocê acertou {certa}, e errou {errada} respostas.')
print(f'\nRelatório final:\n\n{texto_fim}')
