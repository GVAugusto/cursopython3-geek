"""
03. Leia um número real. Se o número for positivo imrpima a raíz quadrada. Do contrário
imrpima o número ao quadrado.
"""

num = float(input('Insira um número: '))

if num >= 0:
    print(f'{num ** (1/2)}')
else:
    print(f'{num ** 2}')
