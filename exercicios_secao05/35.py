"""
35. Leia uma data e determine se ela é válida. Ou seja, verifique se o mês está
entre 1 e 12, e se o dia existe naquele mês. Note que Fevereiro tem 29 dias em
anos bissextos, e 28 dias em anos não bissextos.
"""


def check_dias_mes(x):
    return {
        1: 31,
        2: 28,
        '2b': 29,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31,
    }[x]


data = input('Insira uma data no formato DD/MM/AAAA: ')

dia = int(data.split('/')[0])
mes = int(data.split('/')[1])
ano = int(data.split('/')[2])

if 1 <= mes <= 12:

    if 0 <= ano <= 9999:

        div400 = ano % 400
        div4 = ano % 4
        div100 = ano % 100

        if div400 == 0 or (div4 == 0 and div100 != 0):
            if mes == 2:
                dias_total = check_dias_mes('2b')
            else:
                dias_total = check_dias_mes(mes)

        else:
            dias_total = check_dias_mes(mes)

        if 1 <= dia <= dias_total:
            print(f'Data válida')

        else:
            print('Data inválida')

    else:
        print('Ano inválido')

else:
    print('Mês inválido')
