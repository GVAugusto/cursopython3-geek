"""
20. Dados três valores, A, B, C, verificar se eles podem ser valores dos lados de um
triângulo e, se forem, se é um triângulo escaleno, equilátero ou isoscele, considerando
os seguintes conceitos:

 - O comprimento de cada lado de um triângulo é menor do que a soma dos outros dois
 lados
 - Chama-se equilátero o triângulo que tem três lados iguais
 - Denominam-se isósceles os triângulos que tem o comprimento de dois lados iguais
 - Recebe o nome de escaleno o triângulo que tem os três lados diferentes
"""

lado_a = float(input('Informar lado A do triângulo: '))
lado_b = float(input('Informar lado B do triângulo: '))
lado_c = float(input('Informar lado C do triângulo: '))

soma1 = lado_a + lado_b
soma2 = lado_a + lado_c
soma3 = lado_b + lado_c

if lado_a > soma3 or lado_b > soma2 or lado_c > soma1:
    print('Um dos lados é maior que a soma dos outros dois, favor corrigir entrada')
else:
    if lado_a == lado_b == lado_c:
        print('Triângulo equilátero')
    elif lado_a == lado_b or lado_a == lado_c or lado_b == lado_c:
        print('Triângulo isósceles')
    else:
        print('Triângulo escaleno')
