"""
23. Determine se um determinado ano lido é bissexto. Sendo que um ano é bissexto se
for divisivel por 400 ou por 4 e não for divisível por 100. Por exemplo:
 - 1988
 - 1992
 - 1996
"""

soma = 0

ano = int(input('Insira um ano com 4 dígitos: '))

for i in str(ano):
    soma += 1

if soma == 4:
    div400 = ano % 400
    div4 = ano % 4
    div100 = ano % 100

    if div400 == 0 or (div4 == 0 and div100 != 0):
        print('É um ano bissexto')
    else:
        print('Não é um ano bissexto')
else:
    print('Favor informar número com 4 digítos')
