"""
37. As tarifas de certo parque de estacionamento são as seguintes:
 - 1 e 2 hora -> R$1,00 cada
 - 3 e 4 hora -> R$1,40 cada
 - 5 horas ou mais -> R$2,00 cada

 Os números de horas a pagar é sempre inteiro e arredondado por excesso. Deste modo,
 quem estacionar durante 61 minutos pagará por duas horas, que é o mesmo que pagaria
 se tivesse permanecido por 120 minutos. Os momentos de chegada ao parque e partida
 deste são apresentados na forma de pares inteiros, representando horas e minutos.
 Por exemplo, o par 12 50 representará "dez para a uma da tarde". Pretende-se
 criar um programa que, lidos pelo teclado os momentos de chegada e partida,
 escreva na tela o preço cobrado pelo estacionamento. Admite-se que a chegada e a
 partida se dão com intervalo não superior a 24 horas. Portanto se uma dada hora
 de chegada for superior à da partida, isso não é uma situação de erro, antes
 significará que a partida ocorreu no dia seguinte ao da chegada.
"""

i = 1
valor_final = 0

tempo_entrada = input('Informe hora e minuto de entrada no formato HH MM: ')
tempo_saida = input('Informe hora e minuto de saída no formato HH MM: ')

hora_entrada = float(tempo_entrada.split(' ')[0])
minuto_entrada = float(tempo_entrada.split(' ')[1])
hora_saida = float(tempo_saida.split(' ')[0])
minuto_saida = float(tempo_saida.split(' ')[1])

if hora_saida < hora_entrada:
    hora_saida += 24.0

minuto_e_total = minuto_entrada + (hora_entrada * 60)
minuto_s_total = minuto_saida + (hora_saida * 60)
minuto_total = minuto_s_total - minuto_e_total

hora_total = minuto_total / 60
hora_total = hora_total.__ceil__()

print(f'{minuto_total} - {hora_total}')

while i <= hora_total:

    if 1 <= i <= 2:
        valor_final += 1

    elif 3 <= i <= 4:
        valor_final += 1.40

    else:
        valor_final += 2

    i += 1

print(f'Valor devido: R${valor_final:.2f}')
