"""
32. Escrever um programa que leia o código do produto escolhido do cardápio de uma
lanchonete e a quantidade. O programa deve calcular o valor a ser pago por aquele
lanche. Considere que cada execução somente será calculado um pedido. O cardápio da
lanchonete segue o padrão abaixo:

    Especificação       Código      Preço
    Cachorro Quente     100         1.20
    Bauru Simples       101         1.30
    Bauru com Ovo       102         1.50
    Hamburguer          103         1.20
    Cheeseburguer       104         1.70
    Suco                105         2.20
    Refrigerante        106         1.00
"""


"""
Alternativa para "def"
pre = {100: 1.20,
       101: 1.30,
       102: 1.50,
       103: 1.20,
       104: 1.70,
       105: 2.20,
       106: 1.20}

prod = {100: 'Cachorro quente',
        101: 'Bauru Simples',
        102: 'Bauru com Ovo',
        103: 'Hamburguer',
        104: 'Cheeseburguer',
        105: 'Suco',
        106: 'Refrigerante'}

prod[cod]
pre[cod]
        
"""


def produto(x):
    return {
        100: 'Cachorro Quente',
        101: 'Bauru Simples',
        102: 'Bauru com Ovo',
        103: 'Hamburguer',
        104: 'Cheeseburguer',
        105: 'Suco',
        106: 'Refrigerante',
    }[x]


def valor_produto(x):
    return {
        100: 1.20,
        101: 1.30,
        102: 1.50,
        103: 1.20,
        104: 1.70,
        105: 2.20,
        106: 1.00,
    }[x]


cod_produto = 1
qtd_total = 0
produto_total = 0
valor_total = 0
pedido = "Produto\t\t\t\tValor Un.\t\tQtd\t\tTotal\n"

while cod_produto != 0:
    cod_produto = int(input('\nSelecione o produto desejado: \n'
                            '100. Cachorro Quente - Valor R$1.20\n'
                            '101. Bauru Simples - Valor R$1.30\n'
                            '102. Bauru com Ovo - Valor R$1.50\n'
                            '103. Hamburguer - Valor R$1.20\n'
                            '104. Cheeseburger - Valor R$1.70\n'
                            '105. Suco - Valor R$2.20\n'
                            '106. Refrigerante - Valor R$1.00\n'
                            '0. Encerrar pedido\n'
                            '\nOpção desejada: '))

    if 100 <= cod_produto <= 106:
        nome_produto = produto(cod_produto)
        valor = valor_produto(cod_produto)

        qtd = int(input(f'\nFavor informar a quantidade de {nome_produto}: '))

        total = qtd * valor

        qtd_total += qtd
        produto_total += 1
        valor_total += total

        pedido += f'{nome_produto}\t\tR${valor:.2f}\t\t\t{qtd}\t\tR${total:.2f}\n'

    elif cod_produto == 0:

        pedido += f'\nResumo:\n' \
                  f'Tipos de lanches pedidos: {produto_total}\n' \
                  f'Quantidade de lanches pedidos: {qtd_total}\n' \
                  f'Valor total a ser pago: R${valor_total:.2f}'

        print(f'Pedido finalizado com sucesso, segue fechamento do pedido:\n'
              f'{pedido}')

    else:
        print('Código inválido, favor inserir código correto.')
