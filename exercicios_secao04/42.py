"""
42. Receba o salário-base de um funcionário. Calcule e imprima o salário a receber,
sabendo-se que esse funcionário tem uma gratificação de 5% sobre o salário-base.
Além disso, ele paga 7% de imposto sobre o salário-base.
"""

salario_base = float(input('Favor informar o valor do salário base em Reais: '))

gratificacao = salario_base * 0.05
imposto = salario_base * 0.07
valor_final = salario_base + gratificacao - imposto

print(f'Valor do salário base: {salario_base:.2f}\n'
      f'Valor da gratificação: {gratificacao:.1f}\n'
      f'Valor do imposto: {imposto:.2f}\n'
      f'Valor líquido: {valor_final:.2f}')
