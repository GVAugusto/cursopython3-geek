"""
44. Receba a altura do degrau de uma escada e a altura que o usuário deseja
alcançar subindo a escada. Calcule e mostre quantos degraus o usuário deverá subir
para atingir seu objetivo.
"""

import math

altura_espaco = float(input('Favor informar a altura a ser alcançada em cm: '))
altura_degrau = float(input('Favor informar a altura do degrau em cm: '))

degraus = altura_espaco / altura_degrau

print(f'Quantidade de degraus necessários (ceil): {math.ceil(degraus)}')

# Para arredondar mais exato usar "round(valor, <casas decimais>)"
print(f'Quantidade de degraus necessários (round): {round(degraus, 0)}')
