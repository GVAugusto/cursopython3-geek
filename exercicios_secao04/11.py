"""
11. Leia uma velocidade em m/s (metros por segundo) e apresente-a convertida em
km/h (quilometros por hora).

Fórmula -> K = M * 3.6
"""

vel_ms = float(input('Informe a velocidade em m/s para conversão: '))

vel_km = vel_ms * 3.6

print(f'A velocidade em m/s é: {vel_km}')
