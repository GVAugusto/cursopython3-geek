"""
10. Leia uma velocidade em km/h (quilômetros por hora) e apresente-a convertida em
m/s (metros por segundo).

Fórmula -> M = K / 3.6
"""

vel_kmh = float(input('Informe a velocidade em Km/h para conversão: '))

vel_ms = vel_kmh / 3.6

print(f'A velocidade em m/s é: {vel_ms}')
