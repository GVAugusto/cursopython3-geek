"""
28. Faça uma leitura de três valores e apresente como resultado a soma dos
quadrados dos valores lidos
"""
print('\nInforme 3 valores\n')

valor1 = float(input('Valor 1: '))
valor2 = float(input('Valor 2: '))
valor3 = float(input('Valor 3: '))

valor1 *= valor1
valor2 *= valor2
valor3 *= valor3

soma = valor1 + valor2 + valor3

print(f'\nA soma do quadrado dos valores inseridos é: {soma}')
