"""
34. Leia o valor do raio de um círculo e calcule e imprima a área do círculo
correspondente.

Fórmula = PI * R2 sendo PI=3.141592
"""

num = int(input('Informe o valor do raio do círculo: '))

area_circulo = 3.141592 * (num ** 2)

print(f'\nO círculo de raio {num} tem como área: {area_circulo}')
