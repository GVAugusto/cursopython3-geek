"""
16. Leia um valor de comprimento em polegadas e apresente-o convertido em
centimetros.

Fórmula -> C = P * 2.54
"""

comprimento_pol = float(input('Informe o comprimento em polegadas para conversão: '))

comprimento_cm = comprimento_pol * 2.54

print(f'O comprimento em centímetros é: {comprimento_cm}')
