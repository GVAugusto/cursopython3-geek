"""
32. Leia um número inteiro e imprima a soma do sucessor de seu triplo com o
antecessor de seu dobro
"""

num = int(input('Informe um número: '))

num1 = (num * 3) + 1
num2 = (num * 2) - 1

print(f'\nNúmero: {num}\n'
      f'Sucessor de seu triplo: {num1} \n'
      f'Antecessor de seu dobro: {num2} \n'
      f'Soma de ambos: {num1 + num2}')
