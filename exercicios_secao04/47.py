"""
47. Leia um número inteiro de 4 digitos (de 1000 a 9999) e imprima 1 digito por linha
"""

num = 0

while num < 1000 or num > 9999:
    num = int(input('Favor informar número entre 1000 e 9999: '))

convert = str(num)

for i in convert:
    print(f'{i}')
