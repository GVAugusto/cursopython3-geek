"""
36. Leia a altura e o raio de um cilindro circular e imprima o volume do cilindro.

Fórmula = V = PI * R2 * altura onde PI = 3.141592
"""

print('Informa os dados do cilidro conforme solicitado\n')

raio = float(input('Valor do raio: '))
altura = float(input('Valor da altura: '))

volume = 3.141592 * (raio ** 2) * altura

print(f'O volume do cilindro de altura {altura} e raio {raio} é: {volume}')
