"""
31. Leia um número inteiro e imprima o seu antecessor e o seu sucessor.
"""

num = int(input('Informe um número: '))

print(f'\nO número {num} tem como antecessor o número {num - 1} e '
      f'sucessor o número {num + 1}')
