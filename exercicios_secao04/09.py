"""
09. Leia uma temperatura em graus Celsius e apresente-a convertida em graus
Kelvin.

Fórmula -> K = C + 273.15
"""

temp_celsius = float(input('Informe a temperatura em graus Celsius para conversão: '))

temp_kelvin = temp_celsius + 273.15

print(f'Temperatura em Farenheit é: {temp_kelvin}')
