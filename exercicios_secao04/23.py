"""
23. Leia um valor de comprimento em metros e apresente-o convertido em jards.

Fórmula -> J = M / 0.91
"""

comp_mt = float(input('Informe o comprimento em metros para conversão: '))

comp_jd = comp_mt / 0.91

print(f'O comprimento em jardas é: {comp_jd}')
