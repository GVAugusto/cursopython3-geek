"""
22. Leia um valor de comprimento em jardas e apresente-o convertido em metros.

Fórmula -> M = 0.91 * J
"""

comp_jd = float(input('Informe o comprimento em jardas para conversão: '))

comp_mt = 0.91 * comp_jd

print(f'O comprimento em metros é: {comp_mt}')
