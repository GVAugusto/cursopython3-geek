"""
17. Leia um valor de comprimento em centímetros e apresente-o convertido em
polegadas.

Fórmula -> P = C / 2.54
"""

comprimento_cm = float(input('Informe o comprimento em centímetros para conversão: '))

comprimento_pol = comprimento_cm / 2.54

print(f'O comprimento em polegadas é: {comprimento_pol}')
