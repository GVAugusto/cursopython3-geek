"""
30. Leia um valor em real e a cotação do dólar. Em seguida, imprima o valor
correspondente em dólares.
"""

real = float(input('Informe o valor em reais (R$): '))
dolar = float(input('Informe a cotação atual do dólar (US$): '))

dolar_total = real / dolar

print(f'\nO valor em dólares de R${real} é: US${dolar_total}')
