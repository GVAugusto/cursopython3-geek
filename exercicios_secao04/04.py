"""
04. Leia um número real e imprima o resultado do quadrado desse número
"""

num = float(input('Insira um número real (decimal): '))

num *= num

print(f'O quadrado do valor informado é: {num}')
