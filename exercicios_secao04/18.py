"""
18. Leia um valor de volume em metros cúbicos m3 e apresente-o convertido em litros.

Fórmula -> L = 1000 * M
"""

volume_m = float(input('Informe o volume em metros cúbicos (m3) para conversão: '))

volume_l = 1000 * volume_m

print(f'O volume em litros é: {volume_l}')
