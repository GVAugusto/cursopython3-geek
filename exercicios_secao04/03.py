"""
03. Peça ao usuário para digitar três valores inteiros e imprima a soma deles
"""

num1 = int(input('Favor inserir 3 valores\n\nInsira o primeiro valor: '))
num2 = int(input('Insira o segundo valor: '))
num3 = int(input('Insira o terceiro valor: '))

soma = num1 + num2 + num3

print(f'A soma dos 3 valores informados é: {soma}')
