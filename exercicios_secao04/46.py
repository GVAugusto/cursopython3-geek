"""
46. Faça um programa que leia um número inteiro positivo de três digitos (de 100 a 999).
Gere outro número formado pelos digitos invertidos do número lido.

Exemplo:
Número Lido: 123
Número Gerado: 321
"""

num = 0

while num < 100 or num > 999:
    num = int(input('Favor inserir numero entre 100 e 999: '))

num = str(num)
reverse = num[::-1]

print(f'Número informado: {num}\nNúmero invertido: {reverse}')
