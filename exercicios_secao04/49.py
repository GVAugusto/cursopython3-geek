"""
49. Faça um programa que leia o horário (hora, minuto e segundo) de inicio e duração,
em segundos, de uma experiência biológica. O programa deve resultar com o novo
horário (hora, minuto e segundo) do termino da mesma.
"""

from datetime import datetime

# Solução sem coleta de data ínicio, gerando coleta do sistema

valor_entrada = int(input('Insira um valor em segundos: '))

now = datetime.now().strftime("%H:%M:%S")
hora_atual = int(now.split(":")[0])
minuto_atual = int(now.split(":")[1])
segundo_atual = int(now.split(":")[2])

hora_segundos = hora_atual * 3600
minuto_segundos = minuto_atual * 60

segundo_final = segundo_atual + hora_segundos + minuto_segundos + valor_entrada

valor_hora = int(segundo_final / 3600)
resto_hora = int(segundo_final - (valor_hora * 3600))
valor_minuto = int(resto_hora / 60)
resto_minuto = int(resto_hora - (valor_minuto * 60))

print(f'Horário Inicial: {hora_atual}:{minuto_atual}:{segundo_atual}\n'
      f'Horário Final: {valor_hora}:{valor_minuto}:{resto_minuto}\n')

# Solução com coleta de data início

hora_atual = int(input('Insira um valor em horas: '))
minuto_atual = int(input('Insira um valor em minutos: '))
segundo_atual = int(input('Insira um valor em segundos: '))

hora_segundos = hora_atual * 3600
minuto_segundos = minuto_atual * 60

segundo_final = segundo_atual + hora_segundos + minuto_segundos + valor_entrada

valor_hora = int(segundo_final / 3600)
resto_hora = int(segundo_final - (valor_hora * 3600))
valor_minuto = int(resto_hora / 60)
resto_minuto = int(resto_hora - (valor_minuto * 60))

print(f'Horário Inicial: {hora_atual}:{minuto_atual}:{segundo_atual}\n'
      f'Horário Final: {valor_hora}:{valor_minuto}:{resto_minuto}\n')
