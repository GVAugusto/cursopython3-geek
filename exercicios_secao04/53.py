"""
53. Faça um programa para ler as dimensões de um terreno (comprimento c e largura l),
bem como o preço do metro de tela p. Imprima o custo para cercar esse mesmo terreno
com tela.
"""

comp_c = float(input('Favor inserir o comprimento do terreno: '))
larg_l = float(input('Favor inserir a largura do terreno: '))
tela_p = float(input('Favor inserir valor do metro de tela em Reais: '))

comp_total = comp_c * 2
larg_total = larg_l * 2

tela_total = (comp_total + larg_total) * tela_p

print(f'Valor total para compra da tela para o terreno {comp_c:.2f} x {larg_l:.2f}: '
      f'R${tela_total:.2f}')
