"""
33. Leia o tamanho do lado de um quadrado e imprima como resultado a sua área
"""

num = int(input('Informe o valor de um lado de um quadrado: '))

print(f'\nO quadrado de lado {num} tem como área: {num ** 2}')
