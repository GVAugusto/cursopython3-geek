"""
20. Leia um valor de massa em quilogramas e apresente-o convertido em libras.

Fórmula -> L = K / 0.45
"""

massa_kg = float(input('Informe a massa em Kg para conversão: '))

massa_lb = massa_kg / 0.45

print(f'A massa em libras é: {massa_lb}')
