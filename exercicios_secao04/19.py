"""
19. Leia um valor de volume em litros e apresente-o convertido em metros cúbicos m3.

Fórmula -> M = L / 1000
"""

volume_l = float(input('Informe o volume em litros para conversão: '))

volume_m = volume_l / 1000

print(f'O volume em metros cúbicos (m3) é: {volume_m}')
