"""
08. Leia uma temperatura em graus Kelvin e apresente-a convertida em graus
Celsius.

Fórmula -> C = K - 273.15
"""

temp_kelvin = float(input('Informe a temperatura em graus Kelvin para conversão: '))

temp_celsius = temp_kelvin - 273.15

print(f'Temperatura em Farenheit é: {temp_celsius}')
