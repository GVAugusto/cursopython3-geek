"""
21. Leia um valor de massa em libras e apresente-o convertido em quilogramas.

Fórmula -> K = L * 0.45
"""

massa_lb = float(input('Informe a massa em libras para conversão: '))

massa_kg = massa_lb * 0.45

print(f'A massa em Kg é: {massa_kg}')
