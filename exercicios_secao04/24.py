"""
24. Leia um valor de área em metros quadrados (m2) e apresente-o convertido em acres.

Fórmula -> A = M * 0.000247
"""

area_mt = float(input('Informe a área em metros quadrados (m2) para conversão: '))

area_ac = area_mt * 0.000247

print(f'A área em acres é: {area_ac}')
