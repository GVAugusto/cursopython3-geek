"""
06. Leia uma temperatura em graus Celsius e apresente-a convertida em graus
Farenheit.

Fórmula -> F=C*(9.0/5.0)+32.0
"""

temp_celsius = float(input('Informe a temperatura em graus Celsius para conversão: '))

temp_faren = temp_celsius * (9.0 / 5.0) + 32.0

print(f'Temperatura em Farenheit é: {temp_faren}')
