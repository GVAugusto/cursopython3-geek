"""
48. Leia um valor inteiro em segundos, e imprima-o em horas, minutos e segundos
"""

# Solução convertendo segundos para padrão HH:MM:SS

valor_entrada = int(input('Insira um valor em segundos: '))

valor_hora = valor_entrada / 3600
resto_hora = valor_entrada - (int(valor_hora) * 3600)
valor_minuto = resto_hora / 60
resto_minuto = resto_hora - (int(valor_minuto) * 60)

print(f'Hora: {int(valor_hora)}, Minuto: {int(valor_minuto)}, Segundo: {int(resto_minuto)}')
print(f'{int(valor_hora)}:{int(valor_minuto)}:{int(resto_minuto)}')

# Solução convertendo segundos para horas, ou minutos

valor_hora = valor_entrada / 3600
valor_minuto = valor_entrada / 60

print(f'\n{valor_entrada} em segundos\n'
      f'{int(valor_minuto)} em minutos\n'
      f'{int(valor_hora)} em horas')
