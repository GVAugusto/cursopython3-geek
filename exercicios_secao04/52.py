"""
52. Três amigos jogaram na loteria. Caso eles ganhem, o prêmio deve ser repartido
proporcionalmente ao valor que cada um deu para a realização da aposta. Faça um
programa que leia quanto cada apostador investiu, o valor do prêmio, e imprima
quanto cada um ganharia de prêmio com base no valor investido.
"""

print('\nInsira os valores para os três apostadores:\n')

apostador1 = float(input('Valor do apostador 1: '))
apostador2 = float(input('Valor do apostador 2: '))
apostador3 = float(input('Valor do apostador 3: '))

premio = float(input('\nInsira o valor do prêmio: '))

total_aposta = apostador1 + apostador2 + apostador3

percent_premio1 = apostador1 / total_aposta
percent_premio2 = apostador2 / total_aposta
percent_premio3 = apostador3 / total_aposta

premio1 = percent_premio1 * premio
premio2 = percent_premio2 * premio
premio3 = percent_premio3 * premio

valor_liquido1 = premio1 - apostador1
valor_liquido2 = premio2 - apostador2
valor_liquido3 = premio3 - apostador3

print(f'Apostador 1:\n'
      f' Apostou: {apostador1:.2f}\n'
      f' Ganhou Bruto: {premio1:.2f}\n'
      f' Ganhou Líquido: {valor_liquido1:.2f}\n\n'
      f'Apostador 2:\n'
      f' Apostou: {apostador2:.2f}\n'
      f' Ganhou Bruto: {premio2:.2f}\n'
      f' Ganhou Líquido: {valor_liquido2:.2f}\n\n'
      f'Apostador 3:\n'
      f' Apostou: {apostador3:.2f}\n'
      f' Ganhou Bruto: {premio3:.2f}\n'
      f' Ganhou Líquido: {valor_liquido3:.2f}\n\n')
