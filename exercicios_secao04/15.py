"""
15. Leia um ângulo em radianos e apresente-o convertido em graus.

Fórmula -> G = R * 180 / PI (PI = 3.14)
"""

angulo_radianos = float(input('Informe o ângulo em radianos para conversão: '))

angulo_graus = angulo_radianos * 180 / 3.14

print(f'O ângulo em graus é: {angulo_graus}')
