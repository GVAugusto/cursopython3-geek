"""
12. Leia a distância em milhas e apresente-a convertida em quilômetros.

Fórmula -> K = 1.61 * M
"""

km = float(input('Informe a distância em Km para conversão: '))

ml = 1.61 * km

print(f'A distância em milhas é: {ml}')
