"""
39. A importância de R$780.000,00 será dividida entre três ganhadores de um concurso.
Sendo que da quantia total:

 - O primeiro ganhador receberá 46%;
 - O segundo ganhador receberá 32%;
 - O terceiro receberá o restante;

Calcule e imprima a quantia ganha por cada um dos ganhadores.
"""

valor_total = 780_000.00

premio1 = valor_total * 0.46
premio2 = valor_total * 0.32
premio3 = valor_total - premio1 - premio2

print(f'O prêmio para o primeiro ganhador é de: {premio1:.2f}\n'
      f'O prêmio para o segundo ganhador é de: {premio2:.2f}\n'
      f'O prêmio para o terceiro ganhador é de: {premio3:.2f}')
