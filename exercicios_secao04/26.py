"""
26. Leia um valor em área de metros quadrados (m2) e apresente-o convertido em hectares.

Fórmula -> H = M * 0.0001
"""

area_mt = float(input('Informe a área em metros quadrados (m2) para conversão: '))

area_hec = area_mt * 0.0001

print(f'A área em hectares é: {area_hec}')
