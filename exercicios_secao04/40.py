"""
40. Uma empresa contrata um encanador a R$30.00 por dia.

Faça um programa que solicite o número de dias trabalhados pelo encanador e imprima
a quantia líquida que deverá ser paga, sabendo-se que são descontados 8% para
imposto de renda.
"""

dias_trab = int(input('Favor informar a quantidade de dias trabalhados: '))

valor_bruto = dias_trab * 30.00

valor_liquido = valor_bruto * 0.92

print(f'O valor líquido ganho pelo encanador em {dias_trab} dias é de: '
      f'R${valor_liquido:.2f}')
