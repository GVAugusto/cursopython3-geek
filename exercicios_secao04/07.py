"""
07. Leia uma temperatura em graus Farenheit e apresente-a convertida em graus
Celsius.

Fórmula -> C = 5.0 * (F - 32.0) / 9.0
"""

temp_faren = float(input('Informe a temperatura em graus Farenheit para conversão: '))

temp_celsius = 5.0 * (temp_faren - 32.0) / 9.0

print(f'Temperatura em Farenheit é: {temp_celsius}')
