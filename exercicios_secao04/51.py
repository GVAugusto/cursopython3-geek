"""
51. Escreva um programa que leia as coordenadas x e y de pontos no R2 e calcule sua
distância da origem (0,0).
"""

import math

cateto_a = float(input('Informe o valor de x: '))
cateto_b = float(input('Informe o valor de y: '))

hipotenusa = math.sqrt((cateto_a ** 2) + (cateto_b ** 2))

print(f'\nA distância para (0,0) é: {hipotenusa}')
