"""
05. Leia um número real e imprima a quinta parte deste número
"""

num = float(input('Insira um número real (decimal): '))

num = num / 5

print(f'A quinta parte do número informado é: {num}')
