"""
43. Escreva um programa de ajuda para vendedores. A partir de um valor total lido
mostre:

 - o total a pagar com desconto de 10%;
 - o valor de cada parcela, no parcelamento de 3x sem juros;
 - a comissão do vendedor, no caso da venda ser a vista (5% sobre o valor com
 desconto)
 - a comissão do vendedor, no caso da venda ser parcelada (5% sobre o valor total)
"""

valor_venda = float(input('Favor informar o valor da venda em Reais: '))

valor_desconto = valor_venda * 0.90
valor_parcela = valor_venda / 3.0
comissao_desconto = valor_desconto * 0.05
comissao_parcela = valor_venda * 0.05

print(f'Valor da venda: R${valor_venda:.2f}\n'
      f'Valor à vista: R${valor_desconto:.2f}\n'
      f'Valor por parcela em 3x: R${valor_parcela:.2f}\n'
      f'Comissão Valor à Vista: R${comissao_desconto:.2f}\n'
      f'Comissão Valor Parcelado: R${comissao_parcela:.2f}')
