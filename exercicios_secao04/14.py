"""
14. Leia um ângulo em graus e apresente-o convertido em radianos.

Fórmula -> R = G * PI / 180 (PI = 3.14)
"""

angulo_graus = float(input('Informe o ângulo em graus para conversão: '))

angulo_radianos = angulo_graus * 3.14 / 180

print(f'O ângulo em radianos é: {angulo_radianos}')
