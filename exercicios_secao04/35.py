"""
35. Sejam a e b os catetos de um triângulo, onde a hipotenusa é obtida pela equação
hipotenusa é igual a raíz quadrada da soma dos catetos ao quadrado.

Faça um programa que receba os valores de a e b e calcule o valor da hipotenusa
através da equação dada. Imprima o resultado dessa operação.
"""

import math

cateto_a = float(input('Informe o valor do cateto a: '))
cateto_b = float(input('Informe o valor do cateto b: '))

hipotenusa = math.sqrt((cateto_a ** 2) + (cateto_b ** 2))

print(f'\nA hipotenusa vale: {hipotenusa}')
