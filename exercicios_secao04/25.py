"""
25. Leia um valor de área em acres e apresente-o convertido em metros quadrados (m2).

Fórmula -> M = A * 4048.58
"""

area_ac = float(input('Informe a área em acres para conversão: '))

area_mt = area_ac * 4048.58

print(f'A área em metros quadrados (m2) é: {area_mt}')
