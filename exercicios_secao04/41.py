"""
41. Faça um programa que leia o valor da hora de trabalho (em reais) e número de
horas trabalhadas no mês. Imprima o valor a ser pago ao funcionário, adicionando
10% sobre o valor calculado.
"""

horas_trab = float(input('Favor informar o número de horas trabalhadas: '))
valor_hora = float(input('Favor informar o valor da hora trabalhada em Reais: '))

valor_bruto = horas_trab * valor_hora
valor_liquido = valor_bruto * 1.10

print(f'Valor da hora: {valor_hora:.2f}\n'
      f'Horas trabalhadas: {horas_trab:.1f}\n'
      f'Valor bruto: {valor_bruto:.2f}\n'
      f'Valor líquido: {valor_liquido:.2f}')
