"""
13. Leia uma distância em quilometros e apresente-a convertida em milhas.

Fórmula -> M = K / 1.61
"""

ml = float(input('Informe a distância em Milhas para conversão: '))

km = ml / 1.61

print(f'A distância em milhas é: {km}')
