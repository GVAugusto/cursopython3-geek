"""
50. Implemente um programa que calcule o ano de nascimento de uma pessoa a partir
da idade e do ano atual.
"""

from datetime import datetime

# Solução sem coleta de data ínicio, gerando coleta do sistema

idade = int(input('Insira sua idade em anos: '))

ano_atual = int(datetime.now().strftime("%Y"))

ano_nascimento = ano_atual - idade

print(f'Você possui {idade} anos, sendo assim, nasceu em {ano_nascimento}')
