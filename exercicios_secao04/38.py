"""
38. Leia o salário de um funcionário. Calcule e imprima o valor do novo salário,
sabendo que ele recebeu um aumento de 25%
"""

valor_salario = float(input('Informe o valor do salário: '))

print(f'O valor do salário com aumento é de: {valor_salario * 1.25}')
