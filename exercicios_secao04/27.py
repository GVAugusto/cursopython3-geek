"""
27. Leia um valor em área de hectares e apresente-o convertido em metros quadrados (m2).

Fórmula -> M = H * 10000
"""

area_hec = float(input('Informe a área em hectares para conversão: '))

area_mt = area_hec * 10_000

print(f'A área em metros quadrados (m2) é: {area_mt}')
