"""
45. Faça um programa para converter uma letra maiúscula em letra minúscula. Use
a tabela ASCII para resolver o problema.
"""

letra_mai = input('Favor informar uma letra Maiúscula: ')

ordMai = ord(letra_mai)
ordMin = ordMai + 32
letra_min = chr(ordMin)

print(f'Resultado: {letra_min}')
