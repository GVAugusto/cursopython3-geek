"""
29. Leia quatro notas, calcule a média aritmética e imprima o resultado.
"""
print('\nInforme 4 notas\n')

nota1 = float(input('Nota 1: '))
nota2 = float(input('Nota 2: '))
nota3 = float(input('Nota 3: '))
nota4 = float(input('Nota 4: '))

media = (nota1 + nota2 + nota3 + nota4) / 4

print(f'\nA média aritmética das notas inseridas é: {media}')
