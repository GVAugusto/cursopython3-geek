"""
39. Faça um programa que calcule a área de um triângulo, cuja base e altura são
fornecidas pelo usuário. Esse programa não pode permitir a entrada de dados inválidos,
ou seja, medidas menores ou iguais a 0.
"""

while True:
    valores = input('Insira dois valores separados para base e altura de um triângulo: ')

    base = float(valores.split()[0])
    altura = float(valores.split()[1])

    if base > 0 and altura > 0:
        break
    else:
        print('Valores inválidos, preencha novamente')


area = altura * (base / 2)

print(f'A área do triângulo é: {area}')
