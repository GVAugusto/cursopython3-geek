"""
30. Faça programas para calcular as seguintes sequências:

 - 1 + 2 + 3 + 4 + 5 + ... + n
 - 1 - 2 + 3 - 4 + 5 + ... + (2n - 1)
 - 1 + 3 + 5 + 7 + ... + (2n - 1)
"""

num = int(input('Insira um número inteiro: '))

# SOMA SEQUENCIAL

soma = 0

for i in range(1, num+1):
    soma += i

print(f'Valor da soma Tipo 1: {soma}')


# SOMA ALTERNADA

soma = 0

for i in range(1, (num * 2)):
    check_par = i % 2

    if check_par == 0:
        soma -= i
    else:
        soma += i

print(f'Valor da soma Tipo 2: {soma}')

# SOMA ÍMPAR

soma = 0

for i in range(1, (num * 2)):
    check_par = i % 2

    if check_par == 1:
        soma += i

print(f'Valor da soma Tipo 3: {soma}')
