"""
20. Ler uma sequência de números inteiros e determinar se eles são pares ou não.
Deverá ser informado o número de dados lidos e número de valores pares. O processo
termina quando for digitado o número 1000.
"""

count = 1
par = 1

while True:
    num = int(input('Informe um número inteiro: '))

    if num == 1000:
        print(f'Número {num} é par')
        break

    count += 1

    check_par = num % 2

    if check_par == 0:
        par += 1
        print(f'Número {num} é par')
    else:
        print(f'Número {num} é impar')

print(f'{par} de {count} números informados eram pares')
