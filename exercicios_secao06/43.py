"""
43. Faça um programa que leia um número indeterminado de idades de indivíduos (pare
quando for infoada a idade 0), e calcule a idade média desse grupo.
"""

idade = 1
soma = 0
count = 0

while idade:

    idade = int(input('Informe a idade: '))

    soma += idade

    if idade != 0:
        count += 1

print(f'Média de idade do grupo: {soma / count}')