"""
22. Escreva um programa completo que permita a qualquer aluno introduzir, pelo teclado,
uma sequência arbitrária de notas (válidas no intervalo de 10 a 20) e que mostre na
tela, como resultado, a correspondente média aritmética. O número de notas com que o
aluno pretenda efetuar o cálculo não será fornecido ao programa, o qual terminará
quando for introduzido um valor que não seja válido como nota de aprovação.
"""

count = 0
soma = 0

nota = float(input('Favor inserir uma nota entre 10 e 20: '))

while 10 <= nota <= 20:
    count += 1
    soma += nota
    nota = float(input('Favor inserir uma nota entre 10 e 20: '))

print('Programa finalizado')

media = soma / count

print(f'A média é {media:.2f} ({soma}/{count})')
