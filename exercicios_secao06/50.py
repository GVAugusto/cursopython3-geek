"""
50. Chico tem 1.50 metro e cresce 2 centímetros por ano, enquanto Zé tem 1.10 metros
e cresce 3 centímetros por ano. Escreva um programa que calcule e imprima quantos
anos serão necessários para que Zé seja maior que Chico.
"""

altura_chico = 150
altura_ze = 110
count = 0

while True:

    count += 1

    altura_chico += 2
    altura_ze += 3

    if altura_ze > altura_chico:
        break

altura_ze /= 100
altura_chico /= 100

print(f'Zé agora tem {altura_ze:.2f} metros de altura\n'
      f'Chico agora tem {altura_chico:.2f} metros de altura\n'
      f'Foram necessários {count} anos')
