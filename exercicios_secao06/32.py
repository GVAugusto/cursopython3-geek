"""
32. Faça um programa que simula o lançamento de dois dados, d1 e d2, n vezes, e tem
como saída o número de cada dado e a relação entre eles (>,<,=) de cada lançamento.
"""


from random import random, randint

num = int(input('Favor informar o número de vezes que os dados serão lançados: '))

for i in range(0, num):
    d1 = randint(1, 6)
    d2 = randint(1, 6)

    if d1 == d2:
        print(f'Dado1: {d1} = Dado2: {d2}')
    elif d1 > d2:
        print(f'Dado1: {d1} > Dado2: {d2}')
    elif d1 < d2:
        print(f'Dado1: {d1} < Dado2: {d2}')
