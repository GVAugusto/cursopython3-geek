"""
46. Faça um programa que gera um número aleatório de 1 a 1000. O usuário deve tentar
acertar qual o número foi gerado, a cada tentativa o programa deverá informar se o
chute é menor ou maior que o número gerado. O programa acaba quando o usuário acerta
o número gerado. O programa deve informar em quantas tentativas o número foi
descoberto.
"""

from random import randint

num_aleatorio = randint(1, 1000)
count = 0
num = 0

while num_aleatorio != num:
    count += 1
    num = int(input('Tente adivinhar o número: '))

    if num > num_aleatorio:
        print(f'Número alto! => {num}')
    elif num < num_aleatorio:
        print(f'Número baixo! => {num}')

print(f'Parabéns, você acertou em {count} tentativas!!')
