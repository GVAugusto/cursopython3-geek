"""
48. Faça um programa que some os termos de valor par da sequência Fibonacci, cujos
valores não ultrapassem 4 milhões.
"""

num1 = 0
num2 = 1
fibonnaci = 0
final = '0'
soma = 0

while True:

    num1 = num2
    num2 = fibonnaci
    fibonnaci = num1 + num2

    if fibonnaci > 4000000:
        break

    final += ',' + str(fibonnaci)

    soma += fibonnaci

print(final)
print(f'\nSoma dos valores: {soma}')
