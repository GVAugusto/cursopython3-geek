"""
49. O funcionário chamado Carlos tem um colega chamado João que recebe um salário
que equivale a um terço do seu salário. Carlos gosta de fazer aplicações na caderneta
de poupança e vai aplicar seu salário integralmente nela, pois está rendendo 2% ao
mês. João aplicará seu salário integralmente no fundo de renda fixa, que está
rendendo 5% ao mês. Construa um programa que deverá calcular e mostra a quantidade
de meses necessários para que o valor pertencente a João iguale ou ultrapasse o valor
pertencente a Carlos. Teste com outros valores para taxas.
"""

investimento_carlos = 3000
investimento_joao = 1000
count = 0

while True:

    count += 1

    investimento_carlos *= 1.02
    investimento_joao *= 1.05

    if investimento_joao >= investimento_carlos:
        break

print(f'Investimento João: {investimento_joao:.2f}\n'
      f'Investimento Carlos: {investimento_carlos:.2f}\n'
      f'Meses: {count}')
