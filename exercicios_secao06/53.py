"""
53. Escreva um programa que leia um número inteiro positivo n e em seguida imprima
n linhas do chamado triângulo de Floyd. Para n = 6 temos:

1
2 3
4 5 6
7 8 9 10
11 12 13 14 15
16 17 18 19 20 21
"""

while True:

    num = int(input('Favor inserir um número inteiro positivo: '))

    if num >= 0:
        break

soma = 0
parcial = ''
final = ''

for i in range(1, num+1):

    for j in range(1, i+1):
        soma += 1
        parcial += f'{soma} '

    final += parcial + '\n'
    parcial = ''

print(final)


# OPÇÃO 2

z = 1
for i in range(num+1):
    for f in range(i):
        print(z, end=' ')
        z += 1
    print('')
