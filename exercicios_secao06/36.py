"""
36. Faça um programa que calcule a diferença entre a soma dos quadrados dos primeiros
100 números naturais e o quadrado da soma.

Exemplo:

A soma dos quadrados dos dez primeiros números naturais é:
1² + 2² + ... + 10² = 385

O quadrado da soma dos dez primeiros números naturais é:
(1 + 2 + ... + 10)² = 55² = 3025

A diferença entre a soma dos quadrados dos dez primeiros números naturais e o
quadrado da soma é 3025 - 385 = 2640

"""

somaq = 0
soma = 0

for i in range(1, 101):

    valors = i ** 2
    somaq += valors

    soma += i

qsoma = soma ** 2

dif = qsoma - somaq

print(f'A diferença entre as somatórias é: {dif}')
