"""
29. Escreva um programa para calcular o valor da série, para 5 termos:

S = 0 + 1/2! + 2/4! + 3/6! + ...
"""

valors = 0

for i in range(1, 6):

    div = 1
    new_range = i * 2

    for j in range(new_range, 0, -1):
        div *= j

    calc = (i/div)
    print(f'DIV: {div} - VALOR: {calc}')
    valors += (i/div)

print(f'Valor de S é: {valors:.10f}')
