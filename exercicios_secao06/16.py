"""
16. Faça um programa que leia um número inteiro positivo ímpar N e imprima todos os
números ípares de 0 até N em ordem decrescente.
"""

num = int(input('Favor informa um número ímpar: '))
check_par = num % 2

while check_par == 0:
    num = int(input('Número inválido. Favor informa um número ímpar: '))
    check_par = num % 2

for i in range(num, -1, -1):
    check_par = i % 2

    if check_par == 1:
        print(i)
