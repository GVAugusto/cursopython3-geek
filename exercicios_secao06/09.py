"""
09. Faça um programa que leia um número inteiro N e depois imprima os N primeiros
números naturais ímpares.
"""

count = 1
valor = 0

num = int(input('Digite um número inteiro: '))

while count <= num:
    check_impar = valor % 2

    if check_impar == 1:
        print(f'{valor} é impar')
        count += 1

    valor += 1
