"""
27. Em matemática, o número harmônico designado por H(n) define-se como sendo a soma
da série harmônica:

 - H(n) = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n

Faça um programa que leia um valor n inteiro e positivo e apresente o valor de H(n).
"""

harm = 0

num = int(input('Favor insira um número: '))

for i in range(1, (num+1)):
    harm += (1/i)

print(f'A série harmônica de {num} é: {harm:.2f}')
