"""
14. Faça um programa que leia um número inteiro positivo par N e imprima todos os
números pares de 0 até N em ordem decrescente.
"""

num = int(input('Favor informa um número par: '))
check_par = num % 2

while check_par == 1:
    num = int(input('Número inválido. Favor informa um número par: '))
    check_par = num % 2

for i in range(num, -1, -1):
    check_par = i % 2

    if check_par == 0:
        print(i)
