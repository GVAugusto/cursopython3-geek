"""
34. Faça um programa que calcule o menor número divisível por cada um dos números
de 1 a 20?

Exemplo:

2520 é o menor número que pode ser dividido por cada um dos números de 1 a 10, sem
sobrar resto
"""

count = 1

while True:

    valida = 0

    for i in range(1, 21):
        check_count = count % i

        if check_count == 0:
            valida += 1

    print(f'{count} - {valida}')

    if valida == 20:
        break
    else:
        count += 1

print(f'O primeiro número divisivel entre 1 e 20 é: {count}')
