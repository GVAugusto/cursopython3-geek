"""
41. Faça um programa que calcula a associação em paralelo de dois resistores R1 e R2
fornecidos pelo usuário via teclado. O programa fica pedindo estes valores e
calculando até que o usuário entre com um valor para resistência igual a 0.

R = (R1 * R2) / (R1 + R2)
"""

while True:
    valor = input('Insira o valor de dois resistores (R1 e R2): ')

    r1 = int(valor.split()[0])
    r2 = int(valor.split()[1])

    r = (r1 * r2) / (r1 + r2)

    if r == 0:
        print(f'Fim do programa - Resistência 0')
        break
    else:
        print(f'Valor da resistência: {r:.2f}')
