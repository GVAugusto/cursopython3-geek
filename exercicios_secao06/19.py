"""
19. Escreva um algoritmo que leia um número inteiro entre 100 e 999 e imprima na
saída cada um dos algarismos que compõem o número.
"""

num = int(input('Insira um número inteiro entre 100 e 999: '))

if 100 <= num <= 999:
    num = str(num)
    for i in num:
        print(i)

else:
    print('Favor inserir um valor válido')
