"""
51. Um funcionário recebe aumento anual. Em 1995 foi contratado por 2000 reais.
Em 1996 recebeu aumento de 1.5%. A partir de 1997, os aumentos sempre correspondem
ao dobro do ano anterior. Faça um programa que determine o salário atual do
funcionário.
"""

salario = 2000 * 1.015
aumento = 2000 * 0.015

for i in range(1997, 2022):
    aumento = (salario * 0.015 * 2)
    salario += aumento

    print(f'Ano: {i} - Aumento: {aumento:.2f} - Salário: {salario:.2f}')

print(f'Salário em 2021: {salario:.2f}')
