"""
61. Faça um programa que calcule o maior número palíndromo feito a partir do produto
de dois números de 3 dígitos.

Exemplo:

O maior palíndromo feito a partir do produto de dois números de dois dígitos é:

9009 = 91*99
"""

count = 0

for i in range(1, 1000):

    for j in range(999, 0, -1):

        check = i * j
        check = str(check)

        if len(check) == 6:
            str1 = check[0:3]
            str2 = check[6:2:-1]

            if str1 == str2:

                check = int(check)

                if count == 0:
                    maior = check
                elif check > maior:
                    maiori = i
                    maiorj = j
                    maior = check

                count += 1

print(f'{maiori} * {maiorj} = {maior}')
