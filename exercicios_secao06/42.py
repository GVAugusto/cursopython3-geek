"""
42. Faça um programa que leia um conjunto não determinado de valores, um de cada vez,
e escreva para cada um dos valores lidos, o quadrado, o cubo e a raíz quadrada.
Finalize a entrada de dados com um valor negativo ou zero.
"""

while True:

    valor = float(input('Insira um valor: '))

    if valor <= 0:
        break
    else:
        print(f'Quadrado: {valor ** 2}\n'
              f'Cubo: {valor ** 3}\n'
              f'Raíz Quadrada: {valor ** (1/2)}\n')
