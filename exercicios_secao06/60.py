"""
60. Faça um programa que leia vários números, calcule e mostre:

 a. A soma dos números digitados;
 b. A quantidade de números digitados;
 c. A média dos números digitados;
 d. O maior número digitado;
 e. O menor número digitado;
 f. A média dos números pares;

Finalize a entrada de dados caso o usuário informe o valor 0.
"""

soma, count, soma_par, count_par = 0, 0, 0, 0

while True:

    valor = float(input('Informe um valor: '))

    if valor == 0:
        break

    soma += valor
    count += 1

    if count == 1:
        maior = valor
        menor = valor
    elif valor > maior:
        maior = valor
    elif valor < menor:
        menor = valor

    check_par = valor % 2

    if check_par == 0:
        soma_par += valor
        count_par += 1

media = soma / count
mediapar = soma_par / count_par

print(f'\nSoma: {soma}\n'
      f'Números digitados: {count}\n'
      f'Média: {media}\n'
      f'Maior: {maior}\n'
      f'Menor: {menor}\n'
      f'Média par: {mediapar}')
