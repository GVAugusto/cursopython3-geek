"""
52. Escreva um programa que receba como entrada o valor do saque realizado pelo
cliente de um banco e retorne quantas notas de cada valor serão necessárias para
atender ao saque com a menor quantidade de notas possível. Serão utilizadas notas
de 100, 50, 20, 10, 5, 2 e 1 real.
"""

saque = int(input('Favor inserir o valor do saque: '))

notas = [100, 50, 20, 10, 5, 2, 1]
contarnotas = [0, 0, 0, 0, 0, 0, 0]

print('Serão utilizadas: ')

"""
zip é um iterador de tuplas em que o primeiro item em cada iterador passado é
emparelhado e, em seguida, o segundo item em cada iterador passado é emparelhado,
etc
"""

for a, b in zip(notas, contarnotas):
    if saque >= a:
        b = saque // a
        saque = saque - (b * a)
        print(b, 'de', a)
