"""
05. Faça um programa que peça ao usuário para digitar 10 valores e some-os.
"""

count = 1
soma = 0

while count <= 10:
    num = float(input(f'Digite o valor {count}/10: '))
    soma += num
    count += 1

print(f'Soma: {soma}')
