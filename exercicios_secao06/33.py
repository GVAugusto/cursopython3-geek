"""
33. Dados n e dois números inteiros positivos, i e j, diferentes de 0, imprimir em
ordem crescente os n primeiros naturais que são múltiplos de i ou de j e ou de ambos.

Exemplo:

Para n = 6, i = 2 e j = 3 a saída deverá ser 0,2,3,4,6,8
"""

interval = int(input('Insira número de iterações: '))
valori = int(input('Insira um valor para I: '))
valorj = int(input('Insira um valor para J: '))
count_ok = 0
count = 0
final = ''

while count_ok < interval:
    checki = count % valori
    checkj = count % valorj

    if checki == 0 or checkj == 0:
        count_ok += 1

        if count_ok != 1:
            final += ','

        final += str(count)

    count += 1

print(final)
