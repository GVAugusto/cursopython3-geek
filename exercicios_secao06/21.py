"""
21. Faça um programa que receba dois números. Calcule e mostre:

 - a soma dos números pares desse intervalo de números, incluindo os números digitados
 - a multiplicação dos números ímpares desse intervalo, incluindo os digitados
"""

soma = 0
multi = 1

num1 = float(input('Insira o primeiro número: '))
num2 = float(input('Insira o segundo número: '))

for i in range(int(num1), int(num2+1)):

    check_par = i % 2

    if check_par == 0:
        soma += i
    else:
        multi *= i

print(f'Valor da soma: {soma}\n'
      f'Valor da multiplicação: {multi}')
