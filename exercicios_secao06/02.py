"""
02. Escreva um programa que escreva na tela, de 1 até 100, de 1 em 1, 3 vezes.
A primeira vez deve usar a estrutura de repetição for, a segunda while e a
terceira do while.
"""

for i in range(1, 101):
    print(i)

i = 0

while i < 100:
    i += 1
    print(i)

i = 0

while True:
    i += 1
    print(i)
    if i == 100:
        break
