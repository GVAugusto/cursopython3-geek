"""
10. Faça um programa que calcule e mostre a soma dos 50 primeiros
números pares.
"""

count = 1
valor = 0
soma = 0

while count <= 50:
    check_par = valor % 2

    if check_par == 0:
        soma += valor
        print(f'{count}: Número: {valor} - Parcial: {soma}')
        count += 1

    valor += 1

print(f'Soma dos 50 primeiros números pares: {soma}')
