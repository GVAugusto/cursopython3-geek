"""
17. Faça um programa que leia um número inteiro positivo n e calcule a soma dos n
primeiros números naturais.
"""

soma = 0

while True:
    num = int(input('Insira um número positivo: '))
    if num >= 0:
        break


for i in range(0, num+1):
    soma += i

print(f'Soma: {soma}')
