"""
08. Escreva um programa que leia 10 números e escreva o menor valor lido e o maior
valor lido.
"""

count = 1
soma = 0

while count <= 10:
    num = float(input(f'Digite o valor {count}/10: '))

    if count == 1:
        maior = num
        menor = num

    if num > maior:
        maior = num
    elif num < menor:
        menor = num

    count += 1

print(f'Menor: {menor}\nMaior: {maior}')
