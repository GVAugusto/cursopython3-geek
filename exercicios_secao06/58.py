"""
58. Faça um programa que some os números primos existentes entre a e b, onde a e b
são números informados pelo usuário.
"""


while True:

    numa = int(input('Favor inserir número inteiro maior que 1: '))
    numb = int(input('Favor inserir número inteiro maior que 1 e o anterior: '))

    if 0 < numa < numb and numb > 0:
        break


soma = 0
primo = 0

for j in range(numa+1, numb):

    for i in range(1, j+1):
        check = j % i

        if check == 0:
            soma += 1

    if soma == 2:
        primo += j

    soma = 0

print(primo)
