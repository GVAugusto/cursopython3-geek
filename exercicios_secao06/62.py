"""
62. Se os números de 1 a 5 são escritos em palavras: um, dois, três, quatro, cinco,
então há 2 + 4 + 4 + 6 + 5 = 21 letras usadas no total. Faça um programa que conte
quantas letras seriam utilizadas se todos os números de 1 a 1000 (mil) fossem
escritos em palavras.

OBS.: Não conte espaços ou hifens.
"""

from num2words import num2words

total = 0

for i in range(1, 1001):
    numero = num2words(i, lang='pt-br')
    numero = numero.replace(' ', '')
    numero = numero.replace('-', '')
    total += len(numero)

print(f'Entre 1 e 1000 temos {total} letras')
