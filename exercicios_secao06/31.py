"""
31. Faça um programa que calcule e escreva o valor de S:

S = 1/1 + 3/2 + 5/3 + 7/4 + ... + 99/50
"""

soma = 1
valor1 = 1

for i in range(2, 51):

    valor1 += 2

    valor_final = valor1 / i
    print(f'{i} - {valor_final}')
    soma += valor_final

print(f'Valor Final: {soma}')
