"""
45. Faça um algoritmo que converta uma velocidade expressa em km/h para m/s e vice
versa. Você deve criar um menu com as duas opções de conversão e com uma opção para
finalizar o programa. O usuário poderá fazer quantas conversões desejar, sendo que
o programa só será finalizado quando a opção de finalizar for escolhida.
"""

opcao = 1

while opcao:
    opcao = int(input('Por favor selecione uma das opções:\n'
                      '1. Converter KM/H para M/S\n'
                      '2. Converter M/S para KM/H\n'
                      '0. Finalizar programa\n'
                      'Opção: '))

    if opcao == 1:
        velocidade = float(input('\nFavor informar a velocidade em KM/H: '))
        print(f'{velocidade / 3.6} m/s\n')
    elif opcao == 2:
        velocidade = float(input('\nFavor informar a velocidade em M/S: '))
        print(f'{velocidade * 3.6} km/h\n')
    elif opcao != 0:
        print('\nOpcao invalida, favor informar novamente')
