"""
44. Leia um número positivo do usuário, então, calcule e imprima a sequência
Fibonacci até o primeiro número superior ao número lido. Exemplo: se o usuário
informou o número 30, a sequência a ser impressa será 0 1 1 2 3 5 8 13 21 34.
"""

num = int(input('Insira um valor: '))

num1 = 0
num2 = 1
fibonnaci = 0
final = '0'

for i in range(num):
    num1 = num2
    num2 = fibonnaci
    fibonnaci = num1 + num2

    final += ',' + str(fibonnaci)

    if fibonnaci > num:
        break

print(final)
