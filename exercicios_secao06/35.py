"""
35. Faça um programa que some os números impares contidos em um intervalo definido
pelo usuário. O usuário define o valor inicial do intervalo e o valor final deste
intervalo e o programa deve somar todos os números ímpares contidos neste intervalo.

Caso o usuário digite um intervalo inválido (começando por um valor maior que o
valor final) deve ser escrito uma mensagem de erro na tela, "Intervalo de valores
inválido" e o programa termina.

Exemplo de tela de saída:

Digite o valor inicial e valor final: 5 10

Soma dos ímpares neste intervalo: 21

"""

soma = 0

num = input('Digite o valor inicial e valor final: ')

valori = int(num.split()[0])
valorf = int(num.split()[1])

if valori < valorf:

    for i in range(valori, valorf+1):
        check_valor = i % 2

        if check_valor != 0:
            soma += i

else:
    print('Intervalo de valores inválido')

print(f'Soma dos ímpares neste intervalo: {soma}')
