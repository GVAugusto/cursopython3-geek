"""
25. Faça um programa que some todos os números naturais abaixo de 1000 que são
múltiplos de 3 ou 5
"""

soma = 0

for i in range(1, 1001):

    check3 = i % 3
    check5 = i % 5

    if check3 == 0 or check5 == 0:
        soma += i

print(f'O valor é: {soma}')
