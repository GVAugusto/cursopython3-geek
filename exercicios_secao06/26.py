"""
26. Faça um algoritmo que encontre o primeiro múltiplo de 11, 13 ou 17 após um número
dado.
"""

check = 1

num = float(input('Insira um número: '))

num = int(num)

while True:

    check11 = num % 11
    check13 = num % 13
    check17 = num % 17

    if check11 == 0:
        print(f'{num} é divisivel por 11')
        break
    elif check13 == 0:
        print(f'{num} é divisivel por 13')
        break
    elif check17 == 0:
        print(f'{num} é divisivel por 17')
        break

    num += 1
