"""
28. Faça um programa que leia um valor N inteiro e positivo, calcule o valor e mostre
o valor E conforme a fórmula a seguir:

 - E = 1 + 1/1! + 1/2! + 1/3! + ... + 1/N!
"""

valore = 1

num = int(input('Insira um número inteiro: '))

for i in range(1, (num+1)):

    div = 1

    for j in range(i, 0, -1):
        div *= j

    calc = (1/div)
    print(f'DIV: {div} - VALOR: {calc:.2f}')
    valore += (1/div)

print(f'Valor de E é: {valore:.2f}')
