"""
24. Escreva um programa que leia um número inteiro e calcule a soma de todos os
divisores desse número, com exceção dele próprio. Ex: a soma dos divisores do número
66 é 1 + 2 + 3 + 6 + 11 + 22 + 33 = 78.
"""

count = 0
soma = 0

num = float(input('Insira um número positivo: '))

if num >= 0:
    for i in range(1, int(num)):
        check_div = int(num) % i

        if check_div == 0:
            count += 1
            soma += i

    print(f'{num} é divisivel por {count} números, sendo a soma deles:\n{soma}')

else:
    print('Número não é positivo')
