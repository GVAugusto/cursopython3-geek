"""
06. Faça um programa que leia 10 inteiros e imprima sua média.
"""

count = 1
soma = 0

while count <= 10:
    num = int(input(f'Digite o valor inteiro {count}/10: '))
    soma += num
    count += 1

media = soma / 10
print(f'Média: {media}')
