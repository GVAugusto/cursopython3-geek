"""
18. Escreva um algoritmo que leia certa quantidade de números e imprima o maior deles
e quantas vezes o maior número foi lido. A quantidade de números a serem lidos deve
ser fornecido pelo usuário.
"""

count = 0
count_maior = 0
maior = 0
vezes = int(input('Entre com a quantidade de números que será fornecida: '))

while count < vezes:
    num = float(input(f'Informe o número {count+1}/{vezes}: '))

    if count == 0:
        maior = num
        count_maior = 1
    else:
        if num > maior:
            maior = num
            count_maior = 1
        elif num == maior:
            count_maior += 1

    count += 1

print(f'Número {maior} foi o maior, repetido {count_maior} vez(es).')
