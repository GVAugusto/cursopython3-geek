"""
01. Faça um programa que determine e mostre os cincos primeiros múltiplos de 3,
considerando números maiores que 0.
"""

count = 0
numero = 1

while count < 5:
    resultado = numero % 3
    if resultado == 0:
        print(numero)
        count += 1
    numero += 1
