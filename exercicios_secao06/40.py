"""
40. Elabore um programa que faça leitura de vários números inteiros, até que se
digite um número negativo. O programa tem que retornar o maior e o menor número lidos.
"""

maior = 0
menor = 0
count = 0

while True:
    valor = int(input('Insira um número: '))

    if valor < 0:
        break

    elif count == 0:
        maior = valor
        menor = valor

    elif valor > maior:
        maior = valor

    elif valor < menor:
        menor = valor

    count += 1

print(f'Maior valor: {maior} \nMenor valor: {menor}')
