"""
37. Escreve um programa que verifique quais números entre 1000 e 9999 (inclusive)
possuem a propriedade seguinte: a soma dos dois dígitos de mais baixa ordem com os
dois dígitos de mais alta ordem elevada ao quadrado é igual ao próprio número.

Por exemplo, para o inteiro 3025, temos que:

30 + 25 = 55
55² = 3025
"""

final = ''

for i in range(1000, 10000):

    converte = str(i)

    num1 = int(converte[0:2])
    num2 = int(converte[2:4])

    soma = num1 + num2

    if i == (soma ** 2):
        final += str(i) + '\n'

print(final)
