"""
23. Faça um algoritmo que leia um número positivo e imprima seus divisores
"""

count = 0
final = ''

num = float(input('Insira um número positivo: '))

if num >= 0:
    for i in range(1, int(num+1)):
        check_div = int(num) % i

        if check_div == 0:
            count += 1
            final += f'Número {count}: ' + str(i) + '\n'

    print(f'{num} é divisivel por {count} números, sendo eles:\n{final}')

else:
    print('Número não é positivo')
