"""
59. Escreva um programa que leia o número de habitantes de uma determinada cidade,
o valor do kwh, e para cada habitante entre com os seguintes dados:

 - consumo do mês
 - código do consumidor: 1 - Residencial, 2 - Comercial, 3 - Industrial

No final imprima o maior, o menor e a média de consumo dos habitantes; e por fim o
total do consumo de cada categoria de consumidor
"""

habitantes = int(input('Informa o número de habitantes: '))
valor_kwh = float(input('Informe o valor do KWh: '))

consumo_total, consumo_residencial, consumo_industrial, consumo_comercial = 0, 0, 0, 0

for i in range(habitantes):

    while True:
        consumo_mes = float(input('\nInforme o consumo do mês: '))
        codigo_consumidor = int(input('Informe o código do consumidor:\n'
                                      '1. Residencial\n'
                                      '2. Comercial\n'
                                      '3. Industrial\n'
                                      'Opção: '))

        if 1 <= codigo_consumidor <= 3:
            break

    valor_consumo = valor_kwh * consumo_mes

    if i == 0:
        maior_consumo = valor_consumo
        maior_codigo = codigo_consumidor
        menor_consumo = valor_consumo
        menor_codigo = codigo_consumidor
    elif valor_consumo > maior_consumo:
        maior_consumo = valor_consumo
        maior_codigo = codigo_consumidor
    elif valor_consumo < menor_consumo:
        menor_consumo = valor_consumo
        menor_codigo = codigo_consumidor

    if codigo_consumidor == 1:
        consumo_residencial += valor_consumo

    elif codigo_consumidor == 2:
        consumo_comercial += valor_consumo

    elif codigo_consumidor == 3:
        consumo_industrial += valor_consumo

    consumo_total += valor_consumo

media_consumo = consumo_total / habitantes

print(f'\nConsumo total: {consumo_total}\n'
      f'Média por habitante: {media_consumo}\n\n'
      f'Consumo Residencial: {consumo_residencial}\n'
      f'Consumo Comercial: {consumo_comercial}\n'
      f'Consumo Industrial: {consumo_industrial}\n\n'
      f'Maior consumidor: {maior_consumo} - {maior_codigo}\n'
      f'Menor consumidor: {menor_consumo} - {menor_codigo}')
