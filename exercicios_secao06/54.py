"""
54. Faça um programa que receba um número inteiro maior do que 1 e verifique se o
número fornecido é primo ou não.
"""

while True:

    num = int(input('Favor inserir número inteiro maior que 1: '))

    if num > 1:
        break


soma = 0

for i in range(1, num+1):
    check = num % i

    if check == 0:
        soma += 1


if soma == 2:
    print('Número primo')
else:
    print('Número não é primo')
