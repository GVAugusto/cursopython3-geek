"""
55. Escreva um programa que leia um inteiro não negativo n e imprima a soma dos
n primeiros números primos.
"""

while True:

    num = int(input('Favor inserir número inteiro maior que 1: '))

    if num > 0:
        break


soma = 0
primo = 0

for j in range(1, num+1):

    for i in range(1, j+1):
        check = j % i

        if check == 0:
            soma += 1

    if soma == 2:
        primo += j

    soma = 0

print(primo)
