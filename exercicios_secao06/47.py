"""
47. Faça um programa que apresente um menu de opções para o cáculo das seguintes
operações entre dois números:

 - adição (opção 1)
 - subtração (opção 2)
 - multiplicação (opção 3)
 - divisão (opção 4)
 - saída (opção 5)

O programa deve possibilitar ao usuário a escolha da operação desejada, a exibição do
resultado e a volta ao menu de opções. O programa só termina quando for escolhida a
opção de saída (opção 5).
"""

opcao = 1

while opcao != 5:
    opcao = int(input('Favor informar a opção desejada:\n'
                      '1. Adição\n'
                      '2. Subtração\n'
                      '3. Multiplicação\n'
                      '4. Divisão\n'
                      '5. Saída\n'
                      'Opção desjada: '))

    if 1 <= opcao <= 4:
        num1 = float(input('Informa o primeiro número da operação: '))
        num2 = float(input('Informa o segundo número da operação: '))

        if opcao == 1:
            print(f'Soma: {num1} + {num2} = {num1 + num2}')
        elif opcao == 2:
            print(f'Subtração: {num1} - {num2} = {num1 - num2}')
        elif opcao == 3:
            print(f'Multiplicação: {num1} x {num2} = {num1 * num2}')
        elif opcao == 4:
            print(f'Subtração: {num1} / {num2} = {num1 / num2}')

    elif opcao != 5:
        print('Opção inválida, informa novamente')
