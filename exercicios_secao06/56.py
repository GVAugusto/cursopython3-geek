"""
56. Faça um programa que calcule a soma de todos os números primos abaixo de dois
milhões.
"""

"""
55. Escreva um programa que leia um inteiro não negativo n e imprima a soma dos
n primeiros números primos.
"""

soma = 0
primo = 0

for j in range(1, 2000000+1):

    for i in range(1, j+1):
        check = j % i

        if check == 0:
            soma += 1

    if soma == 2:
        primo += j

    soma = 0

print(primo)
