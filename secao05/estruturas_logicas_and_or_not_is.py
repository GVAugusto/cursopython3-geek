"""
Estruturas lógicas: and (e), or (ou), not (não), is (é)

Operadores unários:
    - not, is

Operadores binários:
    - and, or

Para o AND ambos valores precisam ser "True"
Para o OR apenas um valor precisa ser "True"
Para o NOT o valor do Booleano é invertido
Para o IS vale a comparação entre variáveis
"""

ativo = False
logado = True

if not ativo and logado:
    print('Favor ativar sua conta')
else:
    print('Usuário ativo no sistema')

print(f'Teste NOT TRUE: {not True}')
print(f'Teste NOT FALSE: {not False}')

if ativo is False:
    print('Ative sua conta')

if ativo:
    print('Bem vindo usuário')
elif logado:
    print('Favor realizar login')
else:
    print('Seja bem vindo')

print(f'Questionar se ATIVO é TRUE: {ativo is True}')

nome = 'Testando'
print(nome.isupper())
print(nome.islower())
print(nome.istitle())
