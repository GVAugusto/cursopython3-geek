"""
Estruturas condicionais
if, else, elif


Estrutura condicional IF em C

if(idade < 18){
    printf("Menor de idade");
}else if(idade == 18){
    printf("Tem 18 anos");
}else {
    printf("Maior de idade");
}

Estrutura condicional IF em Java

if(idade < 18){
    System.out.println("Menor de idade");
}else if(idade == 18){
    System.out.println("Tem 18 anos");
}else {
    System.out.println("Maior de idade");
}
"""

idade = 26

if idade < 18:
    print(f'Menor de idade: {idade} anos')
elif idade == 18:
    print(f'Tem {idade} anos')
elif idade == 26:
    print(f'Tem {idade} anos')
else:
    print(f'Maior de idade: {idade} anos')

if idade < 18:
    print(f'Menor de idade: {idade} anos')

if idade == 18:
    print(f'Tem {idade} anos')

if idade == 26:
    print(f'Tem {idade} anos')

if idade > 18:
    print(f'Maior de idade: {idade} anos')

"""
Estrutura em Python

if <regra>:
    regra identada (4 espaços)
elif <regra>:
    regra idendata (4 espaços)
else:
    regra idendata (4 espaços)
"""