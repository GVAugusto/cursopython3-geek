"""
33. Faça um programa que leia um vetor de 15 posições e o compacte, ou seja,
elimine as posições com valor zero. Para isso, todos os elementos à frente do
valor zero, devem ser movidos uma posição para trás no vetor.
"""

vetor = []

for i in range(15):
    vetor.append(int(input(f'Favor informar número {i+1} de 15: ')))

print(vetor)
qtdzero = vetor.count(0)

for i in range(qtdzero):
    vetor.pop(vetor.index(0))

for num, indice in enumerate(vetor):
    print(f'Indice: {num} - Valor: {indice}')
