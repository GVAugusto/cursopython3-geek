"""
37. Considere um vetor A com 11 elementos onde A1 < A2 < ... < A6 > A7 > A8 > ... A11
, ou seja, está ordenado em ordem crescente até o sexto elemento, e a partir desse
elemento está ordenado de forma decrescente. Dado o vetor da questão anterior,
proponha um algoritmo para ordenar os elementos.
"""

valores, vetora = [], []

for i in range(11):
    valores.append(int(input(f'Favor informe o valor {i+1}/11: ')))

valores.sort()

for i in range(5):
    vetora.append(valores[i])

for i in range(10, 4, -1):
    vetora.append(valores[i])

print(vetora)
