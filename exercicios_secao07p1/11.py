"""
11. Faça um programa que preencha um vetor com 10 números reais, calcule e mostre a
quantidade de números negativos e a soma dos números positivos deste vetor.
"""

from collections import Counter

num = []
i = 0
count = 0
soma = 0

for i in range(10):
    num.append(float(input(f'Favor informar número {i+1} de 10: ')))

for indice, valor in enumerate(num):
    if valor < 0:
        count += 1
    else:
        soma += valor

print(f'Possui {count} números negativos e {10-count} números positivos\n'
      f'Soma dos positivos: {soma}')
