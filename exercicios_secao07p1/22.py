"""
22. Faça um programa que leia dois vetores de 10 posições e calcule outro vetor
contendo, nas posições pares os valores do primeiro e nas posições ímpares os
valores do segundo.
"""

vetora, vetorb, vetorc = [], [], []
counta, countb = 0, 0

for i in range(2):
    for j in range(10):
        if i == 0:
            vetora.append(int(input(f'Informar valor inteiro para Vetor A {j + 1} de 10: ')))
        else:
            vetorb.append(int(input(f'Informar valor inteiro para Vetor B {j + 1} de 10: ')))

for i in range(len(vetora) + len(vetorb)):
    if i % 2 == 0:
        vetorc.append(vetora[counta])
        counta += 1
    else:
        vetorc.append(vetorb[countb])
        countb += 1

print(vetorc)
