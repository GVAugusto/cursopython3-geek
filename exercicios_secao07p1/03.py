"""
03. Ler um conjunto de números reais, armazenando-o em vetor e calcular o quadrado
das componentes deste vetor, armazenando o resultado em outro vetor.

Os conjuntos têm 10 elementos cada. Imprimir todos os conjuntos.
"""

numeros = []
quadrados = []
i = 0

while i < 10:
    numeros.append(float(input('Favor informar um número: ')))
    quadrados.append(numeros[i]**2)
    i += 1

for indice, valor in enumerate(numeros):
    print(f'Valor: {valor} - Quadrado: {quadrados[indice]}')
