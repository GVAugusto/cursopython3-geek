"""
15. Leia um vetor com 20 números inteiros. Escreva os elementos do vetor
eliminando elementos repetidos.
"""

num = []

for i in range(20):
    num.append(int(input(f'Favor inserir o número {i+1} de 20: ')))

num_dict = set(num)

print(num_dict)
