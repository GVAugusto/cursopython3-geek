"""
20. Escreva um programa que leia números inteiros no intervalo [0,50] e os armazene
em um vetor com 10 posições. Preencha um segundo vetor apenas com os números
ímpares do primeiro vetor. Imprima os dois vetores, 2 elementos por linha.
"""

valores = []
impares = []
count = 0

while True:
    num = int(input(f'Favor informar o número {count+1} de 10 (Entre 0 e 50): '))

    if 0 <= num <= 50:
        valores.append(num)
        count += 1
        if count == 10:
            break

for i in valores:
    if i % 2 == 1:
        impares.append(i)

print(valores)
print(impares)

print('Valores informados:')
for i in range(0, 9, 2):
    print(valores[i], valores[i+1])

if len(impares) % 2 == 0:
    range_impar = len(impares) - 1
else:
    range_impar = len(impares)

print('Valores ímpares:')
for i in range(0, range_impar, 2):
    if len(impares) % 2 == 1 and i == len(impares) - 1:
        print(impares[i])
    else:
        print(impares[i], impares[i+1])
