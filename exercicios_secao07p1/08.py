"""
08. Crie um programa que lê 6 valores inteiros e, em seguida, mostre na tela os
valores lidos em ordem inversa.
"""

num = []
i = 0

for i in range(6):
    num.append(int(input(f'Favor informar número {i+1} de 6: ')))

num.reverse()
print(num)
