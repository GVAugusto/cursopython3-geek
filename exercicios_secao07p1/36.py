"""
36. Leia um vetor com 10 números reais, ordene os elementos deste vetor, e no
final escreva os elementos do vetor ordenado.
"""

vetor = []

for i in range(10):
    vetor.append(float(input(f'Favor informar o número {i+1}/10: ')))

vetor.sort()

print(vetor)
