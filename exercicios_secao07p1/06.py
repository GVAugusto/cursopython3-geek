"""
06. Faça um programa que receba do usuário um vetor com 10 posições. Em seguida
deverá ser impresso o maior e o menor elemento do vetor.
"""

num = []

for i in range(10):
    num.append(float(input(f'Favor informar o número {i+1} de 10: ')))

print(f'Maior valor: {max(num)}')
print(f'Menor valor: {min(num)}')
