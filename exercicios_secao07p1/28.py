"""
28. Leia 10 números inteiros e armazene em um vetor V. Crie dois novos vetores
V1 e V2.
Copie os valores ímpares de V para V1 e os valores pares de V para V2. Note que cada
um dos vetores V1 e V2 têm no máximo 10 elementos, mas nem todos os elementos são
utilizados. No final escreva os elementos UTILIZADOS de V1 e V2.
"""

vetorv, vetorv1, vetorv2 = [], [], []

for i in range(10):
    vetorv.append(int(input(f'Favor informar o valor {i+1} de 10: ')))

for valor in vetorv:
    if valor % 2 != 0:
        vetorv1.append(valor)
    else:
        vetorv2.append(valor)

print(f'Vetor V1: {vetorv1}\n'
      f'Vetor V2: {vetorv2}')
