"""
32. Leia dois vetores inteiros X e Y, cada um com 5 elementos (assuma que o usuário
não informa elementos repetidos). Calcule e mostre os vetores resultantes em cada
caso abaixo:

 - Soma entre X e Y: soma de cada elemento de X com o elemento da mesma posição em Y
 - Produto entre X e Y: multiplicação de cada elemento de X com o elemento da mesma
    posição em Y
 - Diferença entre X e Y: todos os elementos de X que não existam em Y
 - Interseção entre X e Y: apenas os elementos que aparecem nos dois vetores
 - União entre X e Y: todos os elementos de X, e todos os elementos de Y que não
    estão em X
"""

vetorx = set()
vetory = set()
soma, produto = [], []

for i in range(2):
    j = 0
    while j < 5:
        if i == 0:
            valor = (float(input(f'Informar o número {j + 1} de 5 (Vetor A): ')))
            if valor not in vetorx:
                vetorx.add(valor)
                j += 1
        else:
            valor = (float(input(f'Informar o número {j + 1} de 5 (Vetor B): ')))
            if valor not in vetory:
                vetory.add(valor)
                j += 1

vetorx = list(vetorx)
vetory = list(vetory)

for indice in range(5):
    soma.append(vetorx[indice] + vetory[indice])
    produto.append(vetorx[indice] * vetory[indice])

print(f'Vetor X: {vetorx}\n'
      f'Vetor Y: {vetory}\n'
      f'Soma: {soma}\n'
      f'Produto: {produto}\n'
      f'Diferença: {set(vetorx).difference(vetory)}\n'
      f'Intersecção: {set(vetorx).intersection(vetory)}\n'
      f'União: {set(vetorx).union(vetory)}')
