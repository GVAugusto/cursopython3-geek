"""
17. Leia um vetor de 10 posições e atribua o valor 0 para todos os elementos que
possuírem valores negativos.
"""

valores = []

for i in range(10):
    valores.append(float(input(f'Favor informar o valor {i+1} de 10: ')))

for indice, valor in enumerate(valores):
    if valor < 0:
        valores[indice] = 0

print(valores)
