"""
25. Faça um programa que preencha um vetor de tamanho 100 com os 100 primeiros
naturais que não são múltiplos de 7 ou que terminam com 7.
"""

valores = []
count = 1
numeros = 0

while count <= 100:

    if numeros % 7 != 0 or str(numeros)[-1] != '7':
        valores.append(numeros)
        count += 1

    numeros += 1

print(valores)
