"""
13. Fazer um programa para ler 5 valores e, em seguida, mostrar a posição onde se
encontram o maior e o menor valor
"""

num = []

for i in range(5):
    num.append(float(input(f'Favor informe o valor {i+1} de 5: ')))

print(f'Maior valor: {max(num)} na posição {num.index(max(num))}\n'
      f'Menor valor: {min(num)} na posição {num.index(min(num))}')
