"""
07. Escreva um programa que leia 10 números inteiros e os armazene em um vetor.
Imprima o vetor, o maior elemento e a posição em que ele se encontra.
"""

num = []

for i in range(10):
    num.append(int(input(f'Favor informar o número {i+1} de 10: ')))

for indice, valor in enumerate(num):
    if valor == max(num):
        break

print(f'Valores: {num}\nMaior valor: {max(num)}\nIndice: {indice}')
