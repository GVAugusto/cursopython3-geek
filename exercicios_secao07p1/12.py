"""
12. Fazer um programa para ler 5 valores e, em seguida, mostrar todos os valores
lidos juntamente com o maior, o menor e a média dos valores.
"""

num = []

for i in range(5):
    num.append(float(input(f'Favor informe o valor {i+1} de 5: ')))

print(f'Valores: {num}\n'
      f'Maior: {max(num)}\n'
      f'Menor: {min(num)}\n'
      f'Média: {sum(num) / len(num)}')
