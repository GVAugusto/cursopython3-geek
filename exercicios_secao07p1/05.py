"""
05. Leia um vetor de 10 posições. Contar e escrever quantos valores pares ele possui.
"""

num = []
count = 0

for i in range(10):
    num.append(int(input('Favor informar um valor: ')))


for valor in num:
    if valor % 2 == 0:
        count += 1

print(f'Você digitou {count} valores pares')
