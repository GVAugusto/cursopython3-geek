"""
27. Leia 10 números inteiros e armazene em um vetor. Em seguida escreva os elementos
que são primos e suas respectivas posições no vetor.
"""

valores = []

for i in range(10):
    valores.append(int(input(f'Informe o valor {i+1} de 10: ')))

for valor in valores:
    if valor > 1:
        count = 0

        for i in range(1, valor+1):

            if valor % i == 0:
                count += 1

        if count == 2:
            print(f'Vetor {valores.index(valor)} possui número primo {valor}')
