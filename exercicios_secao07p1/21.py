"""
21. Faça um programa que receba do usuário dois vetores, A e B, com 10 números
inteiros cada. Crie um novo vetor denominado C calculando C = A - B. Mostre na tela
os dados do vetor C.
"""

vetora, vetorb, vetorc = [], [], []

for i in range(2):
    for j in range(10):
        if i == 0:
            vetora.append(int(input(f'Informar valor inteiro para Vetor A {j + 1} de 10: ')))
        else:
            vetorb.append(int(input(f'Informar valor inteiro para Vetor B {j + 1} de 10: ')))

for i in range(len(vetora)):
    vetorc.append(vetora[i] - vetorb[i])

print(vetorc)
