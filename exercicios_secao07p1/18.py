"""
18. Faça um programa que leia um vetor de 10 números. Leia um número X. Conte os
múltiplos de um inteiro X num vetor e mostre-os na tela.
"""

valores = []
multiplos = []
count = 0

for i in range(10):
    valores.append(float(input(f'Favor informar o número {i+1} de 10: ')))

num = int(input('Favor informar um valor inteiro: '))

for valor in valores:
    if valor % num == 0:
        multiplos.append(valor)
        count += 1

print(f'{count} múltiplos: {multiplos}')
