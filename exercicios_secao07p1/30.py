"""
30. Faça um programa que leia dois vetores de 10 elementos. Crie um vetor que seja a
intersecção entre os 2 vetores anteriores, ou seja, que contém apenas os números que
estão em ambos os vetores. Não deve conter números repetidos.
"""

vetora = set()
vetorb = set()
vetorc = []

for i in range(2):
    j = 0
    while j < 10:
        if i == 0:
            valor = (float(input(f'Informar o número {j + 1} de 10 (Vetor A): ')))
            if valor not in vetora:
                vetora.add(valor)
                j += 1
        else:
            valor = (float(input(f'Informar o número {j + 1} de 10 (Vetor B): ')))
            if valor not in vetorb:
                vetorb.add(valor)
                j += 1

vetorc = vetora.intersection(vetorb)

print(f'Vetor A: {vetora}\n'
      f'Vetor B: {vetorb}\n'
      f'Vetor C (Intersecção): {vetorc}')
