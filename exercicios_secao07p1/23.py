"""
23. Ler dois conjuntos de números reais, armazenando-os em vetores e calular o
produto escalar entre eles. Os conjuntos têm 5 elementos cada. Imprimir os dois
conjuntos e o produto escalar, sendo que o produto escalar é dado por:
x1 * y1 + x2 * y2 + ... + xn * yn
"""

vetora, vetorb = [], []
escalar = 0

for i in range(2):
    for j in range(5):
        if i == 0:
            vetora.append(float(input(f'Informar valor inteiro para Vetor A {j + 1} de 5: ')))
        else:
            vetorb.append(float(input(f'Informar valor inteiro para Vetor B {j + 1} de 5: ')))

for i in range(5):
    escalar += vetora[i] * vetorb[i]

print(f'Vetor A: {vetora}\n'
      f'Vetor B: {vetorb}\n'
      f'Produto Escalar: {escalar}')
