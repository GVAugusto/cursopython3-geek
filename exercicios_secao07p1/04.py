"""
04. Faça um programa que leia um vetor de 8 posições e, em seguida, leia também
dois valores de X e Y quaisquer correspondentes a duas posições no vetor.
Ao final seu programa deverá escrever a soma dos valores encontrados nas respectivas
posições X e Y.
"""

valores = []

for i in range(8):
    valores.append(float(input('Favor informar um número: ')))

while True:
    x = int(input('Favor informar um valor para X: '))
    y = int(input('Favor informar um valor para Y: '))

    if 1 <= x <= 8 and 1 <= y <= 8:
        break

print(f'Soma de {valores[x-1]} + {valores[y-1]} = {valores[x-1] + valores[y-1]}')
