"""
34. Faça um programa para ler 10 números DIFERENTES a serem armazenados em um
vetor. Os dados deverão ser armazenados no vetor na ordem que forem sendo lidos,
sendo que caso o usuário digite um número que já foi digitado anteriormente, o
programa deverá pedir para ele digitar um outro número. Note que cada valor
digitado pelo usuário deve ser pesquisado no vetor, verificando se ele existe
entre os números que já foram fornecidos. Exibir na tela o vetor final que foi
digitado.
"""

numeros = []
i = 0

while i < 10:
    valor = (int(input(f'Informar um número {i + 1} de 10: ')))

    if numeros.count(valor) == 0:
        numeros.append(valor)
        i += 1

print(numeros)
