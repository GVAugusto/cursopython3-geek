"""
16. Faça um programa que leia um vetor de 5 posições para números reais e, depois,
um código inteiro. Se o código for zero, finalize o programa; se for 1, mostre
o vetor na ordem direta; se for 2 mostre o vetor na ordem inversa. Caso o código
seja diferente de 1 e 2, escreva uma mensagem informando que o código é inválido.
"""

valores = []

for i in range(5):
    valores.append(float(input(f'Informe o número {i+1} de 5: ')))

while True:
    num = int(input('Selecione uma das opções:\n'
                    '1. Mostre os valores na ordem direta\n'
                    '2. Mostre os valores na ordem inversa\n'
                    '0. Sair\n'
                    'Opção: '))

    if num == 0:
        break
    elif num == 1:
        valores.sort()
        print(valores)
        break
    elif num == 2:
        valores.sort()
        valores.reverse()
        print(valores)
        break
