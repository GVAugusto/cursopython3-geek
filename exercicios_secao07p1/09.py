"""
09. Crie um programa que lê 6 valores inteiros pares e, em seguida, mostre na tela
os valores lidos em ordem inversa.
"""

num = []
i = 0

while i < 6:
    valor = int(input(f'Favor informar número {i+1} de 6: '))

    if valor % 2 == 0:
        num.append(valor)
        i += 1

num.reverse()
print(num)
