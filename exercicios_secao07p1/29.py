"""
29. Faça um programa que receba 6 números inteiros e mostre:
 - Os números pares digitados
 - A soma dos números pares digitados
 - Os números ímpares digitados
 - A quantidade de números ímpares digitados
"""

valores, pares, impares = [], [], []

for i in range(6):
    valores.append(int(input(f'Favor informar o valor {i+1} de 6: ')))

for valor in valores:
    if valor % 2 == 0:
        pares.append(valor)
    else:
        impares.append(valor)

print(f'Números pares digitados: {pares}\n'
      f'Soma dos números pares: {sum(pares)}\n'
      f'Números ímpares digitados: {impares}\n'
      f'Quantidade de números ímpares digitados: {len(impares)}')
