"""
24. Faça um programa que leia dez conjuntos de dois valores, o primeiro
representando o número do aluno e o segundo representando a sua altura em metros.
Encontre o aluno mais alto e o mais baixo. Mostre o número do aluno mais baixo e do
mais baixo, juntamente com suas alturas.
"""

aluno = {}

for i in range(10):
    j = 0
    while j < 2:
        if j == 0:
            numero = (int(input(f'Informar o número do aluno {i + 1} de 10: ')))
            if aluno.get(numero) is None:
                j += 1

        if j == 1:
            altura = (float(input(f'Informar a altura do aluno {i + 1} de 10: ')))
            aluno[numero] = altura
            j += 1

print(aluno)

for indice, valor in aluno.items():
    if valor == max(aluno.values()):
        indice_maior = indice
    elif valor == min(aluno.values()):
        indice_menor = indice

print(f'Aluno mais alto -> Número: {indice_maior} - Altura: {max(aluno.values())}\n'
      f'Aluno mais baixo -> Número: {indice_menor} - Altura: {min(aluno.values())}\n')
