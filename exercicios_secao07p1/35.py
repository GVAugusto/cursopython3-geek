"""
35. Faça um programa que leia dois números a e b (positivos menores que 10.000) e:

 - Crie um vetor onde cada posição é um algarismo do número. A primeira posição é
 o algarismo menos significativo

 - Crie um vetor que seja a soma de a e b, mas faça-o usando apenas os vetores
 construídos anteriormente

 Dica: soma as posições correspondentes. Se a soma ultrapassar 10, subtraia 10 do
 resultado e some 1 à próxima posição.
"""

valora = int(input('Favor inserir um valor para A: '))
valorb = int(input('Favor inserir um valor para B: '))

vetora = list(str(valora)[::-1])
vetorb = list(str(valorb)[::-1])
vetorc = []

print(vetora)
print(vetorb)

if len(vetora) > len(vetorb):
    maior = len(vetora)
    dif = len(vetora) - len(vetorb)
    for i in range(dif):
        vetorb.append(0)
else:
    maior = len(vetorb)
    dif = len(vetorb) - len(vetora)
    for i in range(dif):
        vetora.append(0)

difsoma = 0

for i in range(maior):
    vetorc.append(int(vetora[i]) + int(vetorb[i]) + difsoma)

    if vetorc[i] >= 10:
        difsoma = 1
        vetorc[i] -= 10
    else:
        difsoma = 0

if difsoma == 1:
    vetorc.append(difsoma)

print(vetorc)

