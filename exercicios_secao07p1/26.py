"""
26. Faça um programa que calcule o desvio padrão de um vetor v contendo n = 10
números onde m é a média do vetor.

Desvio padrão = √( 1 / (n-1) ) * Σ(i=1, n) (v[i] - m)^2
"""

import statistics

valores = []
soma = 0

for i in range(10):
    valores.append(float(input(f'Favor informar o valor {i+1} de 10: ')))

n = len(valores)
m = sum(valores) / n

for valor in valores:
    soma += (valor - m) ** 2

desvio = (soma / (n - 1)) ** 0.5

print(f'Desvio padrão a partir de fórmula manual: {desvio.__round__(2)}')
print(f'Desvio padrão com statistics: {statistics.stdev(valores).__round__(2)}')
