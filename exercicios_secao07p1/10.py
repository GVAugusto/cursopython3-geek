"""
10. Faça um programa para ler a nota da prova de 15 alunos e armazene num valor,
calcule e imprima a média geral.
"""

notas = []

for i in range(15):
    notas.append(float(input(f'Favor informar a nota do aluno {i+1}: ')))

print(f'Média da turma: {sum(notas) / len(notas)}')
