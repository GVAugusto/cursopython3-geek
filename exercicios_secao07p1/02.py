"""
02. Crie um programa que lê 6 valores inteiros e, em seguida, mostre na tela os
valores lidos.
"""

valores = []
i = 0

while i < 6:
    valores.append(int(input('Favor informar um valor inteiro: ')))

    i += 1

for indice, valor in enumerate(valores):
    print(f'valores[{indice}] = {valor}')
