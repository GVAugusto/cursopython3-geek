"""
14. Faça um programa que leia um vetor de 10 posições e verifique se existem valores
iguais e os escreva na tela.
"""

num = []

for i in range(10):
    num.append(float(input(f'Favor inserir o valor {i+1} de 10: ')))

for valor, indice in enumerate(num):
    if num.count(valor) > 1:
        print(f'Valor {valor} foi repetido {num.count(valor)} vezes')
