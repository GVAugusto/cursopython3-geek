"""
38. Peça ao usuário para digitar dez valores numéricos e ordene por ordem crescente
esses valores, guardando-os num vetor. Ordene o valor assim que ele for digitado.
Mostre ao final na tela os valores em ordem.
"""

valores = []

for i in range(10):
    valores.append(float(input(f'Favor inserir valor {i+1} de 10: ')))
    valores.sort()

print(valores)
