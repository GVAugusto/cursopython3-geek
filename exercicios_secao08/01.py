"""
01. Crie uma função que recebe como parâmetro um número inteiro e devolve o seu
dobro.
"""


def dobro(num):
    return num * 2


valor = int(input('Favor informar um número inteiro para calcular o dobro: '))

print(dobro(valor))
