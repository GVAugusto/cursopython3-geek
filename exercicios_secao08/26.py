"""
26. Faça um algoritmo que receba um número inteiro positivo n e calcule o somatório
de 1 até n.
"""


def somatorio(valor):
    soma = 0
    for i in range(valor+1):
        soma += i

    return soma


num = int(input('Favor informar valor para somatória: '))

print(somatorio(num))
