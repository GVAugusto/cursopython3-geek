"""
11. Elabore uma função que receba três notas de um aluno como parâmetros e uma
letra.
 - Se a letra for A, a função deverá calcular a média aritmética das notas;
 - Se a letra for P deverá calcular a média ponderada, com pesos 5, 3 e 2.
"""


def calculo_media(tipo_media='A', *args):
    print(args)
    if tipo_media == 'P' or tipo_media == 'p':
        media_final = ((args[0] * 5) + (args[1] * 3) + (args[2] * 2)) / 10
        return media_final
    elif tipo_media == 'A' or tipo_media == 'a':
        media_final = (args[0] + args[1] + args[2]) / 3
        return media_final
    return 'Opção de calculo inválida'


nota = []

for i in range(3):
    nota.append(float(input(f'Informe a nota {i+1} de 3: ')))

tipo = input('Informe o tipo de média desejada:\n'
             'A. Média Aritmética'
             'P. Média Ponderada'
             'Opção (Valor Padrão: A): ')

print(f'Resultado:\n'
      f'Nota 1: {nota[0]}\n'
      f'Nota 2: {nota[1]}\n'
      f'Nota 3: {nota[2]}\n'
      f'Média: {calculo_media(tipo, *nota)}')
