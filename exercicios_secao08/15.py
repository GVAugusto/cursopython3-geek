"""
15. Crie um programa que receba três valores (obrigatoriamente maiores que zero),
representando as medidas dos três lados de um triângulo. Elabore funções para:

  a. Determinar se esses lados formam um triângulo, sabendo que:
    - O comprimento de cada lado de um triângulo é menor do que a soma dos outros
    dois lados

  b. Determinar e mostrar o tipo de triângulo, caso as medidas formem um triângulo.
  Sendo que:
    - Chama-se equilátero o triângulo que tem três lados iguais
    - Denominam-se isósceles o triângulo que tem o comprimento de dois lados iguais
    - Recebe o nome de escaleno o triângulo que tem os três lados diferentes
"""


def check_triangulo_true(lado1, lado2, lado3):
    soma1 = lado2 + lado3
    soma2 = lado1 + lado3
    soma3 = lado1 + lado2

    if soma1 < lado1 or soma2 < lado2 or soma3 < lado3:
        return False
    return True


def check_triangulo(lado1, lado2, lado3):
    if lado1 == lado2 == lado3:
        return 'Equilátero'
    elif lado1 == lado2 or lado2 == lado3 or lado1 == lado3:
        return 'Isósceles'
    return 'Escaleno'


total = 0
triangulo = []

while total < 3:
    valor = float(input(f'Informar o valor do lado {total+1} do triângulo: '))

    if valor > 0:
        triangulo.append(valor)
        total += 1
    else:
        print('Valor deve ser maior que zero, favor informar novo valor!\n')

if check_triangulo_true(*triangulo):
    print(check_triangulo(*triangulo))
else:
    print('Lados informados não constituem um triângulo')
