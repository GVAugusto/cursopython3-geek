"""
29. Faça uma função que receba como parâmetro o valor de um ângulo em graus e
calcule o valor do seno hiperbólico desse ângulo usando sua respectiva série de
Taylor:

sin x = (soma n=0 ao infinito) (x^(2n+1) / (2n+1)!) = x + x^3/3! + x^5/5! - ...
para todo x, onde x é o valor do ângulo em radianos. Considerar pi = 3.141593 e n
variando de 0 até 5.
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


def graus_radianos(valor):
    return valor * 3.141593 / 180


def calculosenohiperbolico(valorangulo):
    soma = 0
    for n in range(6):
        seno = (valorangulo ** (2 * n + 1) / fatorial(2 * n + 1))
        soma += seno

    return soma


graus = float(input('Favor informar o ângulo para cálculo do seno: '))

radianos = graus_radianos(graus)

print(calculosenohiperbolico(radianos))
