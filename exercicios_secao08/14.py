"""
14. Faça uma função que receba a distância em Km e a quantidade de litros de
gasolina consumidos pr um carro em um percurso, calcule o consumo em Km/l e
escreva uma mensagem de acordo com a tabela abaixo:

CONSUMO        KM/L        MENSAGEM
MENOR QUE       8       VENDA O CARRO!
ENTRE          8 E 12   ECONOMICO!
MAIOR QUE       12      SUPER ECONOMICO
"""


def calculo_consumo(km, litro):
    consumo = km / litro
    if consumo < 8:
        return 'Venda o carro!'
    elif consumo > 12:
        return 'Super econômico'
    return 'Econômico'


kilometro = float(input('Favor informar os KM rodados: '))
litros = float(input('Favor informar quantos litros usou: '))

print(f'KM: {kilometro}\n'
      f'Litros: {litros}\n'
      f'Resultado: {calculo_consumo(kilometro, litros)}')
