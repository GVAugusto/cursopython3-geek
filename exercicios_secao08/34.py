"""
34. Faça uma função não-recursiva que receba um número inteiro positivo impar N
e retorne o fatorial duplo desse número. O fatorial duplo é definido como o
produto de todos os números naturais ímpares de 1 até algum número natual
ímpar N. Assim, o fatorial duplo de 5 é: 5!! = 1 * 3 * 5 = 15
"""


def fatorial_duplo(valor):
    resultado = 1
    for i in range(1, valor+1, 2):
        resultado *= i

    return resultado


def check_impar(valor):
    if valor % 2 == 1:
        return True
    return False


while True:
    num = int(input('Favor informar um valor ímpar: '))
    if check_impar(num):
        break

    print('Número par, favor informar valor ímpar')

print(fatorial_duplo(num))
