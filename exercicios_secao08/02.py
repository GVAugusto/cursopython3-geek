"""
02. Faça uma função que receba a data atual (dia, mês e ano em inteiro) e exiba-a
na tela no formato textual por extenso. Exemplo:
Data: 01/01/2000
Imprimir: 1 de janeiro de 2000
"""


def mes_extenso(mes):
    meses = {1: 'janeiro',
             2: 'fevereiro',
             3: 'março',
             4: 'abril',
             5: 'maio',
             6: 'junho',
             7: 'julho',
             8: 'agosto',
             9: 'setembro',
             10: 'outubro',
             11: 'novembro',
             12: 'dezembro'}
    return meses[mes]


def data_extenso(data):
    partes = data.split('/')
    texto = partes[0] + ' de ' + mes_extenso(int(partes[1])) + ' de ' + partes[2]
    return texto


resultado = 0

while resultado != 3:
    resultado = 0
    datauser = input('Favor informar data no formato DD/MM/AAAA: ')

    dia, mes, ano = datauser.split('/')

    dia = int(dia)
    mes = int(mes)
    ano = int(ano)

    if 1 <= dia <= 31:
        resultado += 1
    if 1 <= mes <= 12:
        resultado += 1
    if 999 < ano < 9999:
        resultado += 1
    if resultado != 3:
        print('Dia, Mês ou Ano inválidos, favor informar data com padrão correto.')

print(data_extenso(datauser))
