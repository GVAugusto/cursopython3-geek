"""
10. Faça uma função que receba dois números e retorne qual deles é maior.
"""


def check_maior(*args):
    return max(args)


val1 = float(input('Informe o primeiro valor a ser comparado: '))
val2 = float(input('Informe o segundo valor a ser comparado: '))

print(f'O maior valor entre {val1} e {val2} é {check_maior(val1, val2)}')
