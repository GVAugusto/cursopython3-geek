"""
42. Faça uma função que receba um vetor de reais e retorne e média dele.
"""


def media_vetor(vetor):
    media = sum(vetor) / len(vetor)
    return media


inteiros = []
count = 0

while count < 5:
    inteiros.append(int(input(f'Favor informar valor {count+1} de 5: ')))
    count += 1


print(f'Lista: {inteiros}\n'
      f'Maior valor da lista: {media_vetor(inteiros)}')
