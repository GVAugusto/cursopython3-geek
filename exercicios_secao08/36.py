"""
36. Faça uma função não-recursiva que receba um número inteiro positivo N e
retorne o superfatorial desse número. O superfatorial de um número N é definida
pelo produto dos N primeiros fatoriais de N. Assim, o superfatorial de 4 é:
sf(4) = 1! * 2! * 3! * 4! = 288
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


def superfatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= fatorial(i)

    return resultado


num = int(input('Favor informar o valor para calculo de superfatorial: '))

print(superfatorial(num))
