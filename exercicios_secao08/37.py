"""
37. Faça uma função não recursiva que receba um número inteiro positivo N e
retorne o hipefatorial desse número. O hiperfatorial de um número N, escrito H(n),
é definido por:
H(n) = produto(k=1 a n) k^k = 1^1 * 2^2 * 3^3 ... (n-1)^(n-1) * n^n
"""


def hiperfatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i ** i

    return resultado


num = int(input('Favor informar valor para calculo de hiperfatorial: '))

print(hiperfatorial(num))
