"""
23. Escreva uma função que gera um triângulo lateral de altura 2*n-1 e n largura.

Por exemplo, a saída para n=4 seria:
*
**
***
****
***
**
*
"""


def criatriangulo(valorn):
    texto = ''
    altura = (2 * valorn) - 1

    for i in range(1, valorn):
        texto += ('*' * i) + '\n'

    for j in range(valorn, 0, -1):
        texto += ('*' * j) + '\n'

    return texto


num = int(input('Favor informar um número: '))

print(criatriangulo(num))
