"""
06. Faça uma função que receba 3 números inteiros como parâmetro, representando
horas, minutos e segundos, e os converta em segundos.
"""


def converte_hora(horas, minutos, segundos):
    minutos += (horas * 60)
    segundos += (minutos * 60)
    return segundos


hora_in = int(input('Favor informar horas para conversão em segundos: '))
min_in = int(input('Favor informar minutos para conversão em segundos: '))
seg_in = int(input('Favor informar segundos para conversão em segundos: '))

print(f'O período de {hora_in}:{min_in}:{seg_in} corresponde a '
      f'{converte_hora(hora_in, min_in, seg_in)} segundos')
