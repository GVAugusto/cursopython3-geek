"""
07. Faça uma função que receba uma temperatura em graus Celsius e retorne-a
convertida em graus Fahrenheit. A fórmula de conversão é:
 F = C * (9.0 / 5.0) + 32.0, sendo

 F - A temperatura em Farenheits
 C - A temperatura em Celsius
"""


def converte_celsius(temp_celsius):
    temp_faren = temp_celsius * (9.0 / 5.0) + 32.0
    return round(temp_faren, 2)


temperatura = float(input('Informe a temperatura em graus Celsius para conversão'
                          'em graus Farenheit: '))

print(f'{temperatura}ºC correspondem a {converte_celsius(temperatura)}F')
