"""
05. Faça uma função e um programa de teste para o cálculo do volume de uma esfera.
Sendo que o raio é passado por parâmetro.
Fórmula:
V = 4/3 * pi * R^3
"""


def volume_esfera(raio):
    volume = (4 / 3) * 3.1415 * (raio ** 3)

    return round(volume, 2)


valor = float(input('Informa o valor do raio da esfera: '))

print(f'Volume da esfera de raio {valor} é {volume_esfera(valor)}')
