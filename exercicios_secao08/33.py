"""
33. Faça uma função que receba um número N e retorne a soma dos algarismos de N!.
Ex.: Se N=4, N!=24. Logo a soma de seus algarismos é 2+4=6.
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


def soma_algarismo(valor):
    soma = 0
    valor = fatorial(valor)
    for i in range(len(str(valor))):
        soma += int(str(valor)[i:i+1])

    return soma


num = int(input('Favor informar um número para cálculo: '))

print(soma_algarismo(num))
