"""
03. Faça uma função para verificar se um número é positivo ou negativo. Sendo que
o valor de retorno será 1 se positivo, -1 se negativo e 0 se for igual a 0.
"""


def check_numero(num):
    if num < 0:
        print('Número negativo')
        return -1
    elif num > 0:
        print('Número positivo')
        return 1
    print('Zero')
    return 0


valor = float(input('Favor informar um número: '))

print(check_numero(valor))
