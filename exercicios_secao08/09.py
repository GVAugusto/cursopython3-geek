"""
09. Faça uma função que receba a altura e o raio de um cilindro circular e retorne
o volume do cilindro. O volume de um cilindro circular é calculado por meio da
seguinte fórmula:

V = pi * raio^2 * altura, onde pi = 3.141592
"""


def volume_cilindro(raio, altura, pi=3.141592):
    volume = pi * (raio ** 2) * altura
    return round(volume, 2)


valor_altura = float(input('Informa o valor da altura do cilindro: '))
valor_raio = float(input('Informa o valor do raio do cilindro: '))

print(f'O cilindro de altura {valor_altura} e raio {valor_raio} tem '
      f'volume de {volume_cilindro(valor_raio, valor_altura)}')
