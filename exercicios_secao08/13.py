"""
13. Faça uma função que receba dois valores numéricos e um símbolo. Este símbolo
representará a operação que se deseja efetuar com os números:

 - + deverá realizar uma adição
 - - deverá realizar uma subtração
 - / deverá realizar uma divisão
 - * deverá realizar uma multiplicação
"""


def calc_operacao(val1, val2, op='+'):
    if op == '+':
        return val1 + val2
    elif op == '-':
        return val1 - val2
    elif op == '/':
        return val1 / val2
    elif op == '*':
        return val1 * val2
    return 'Operação não disponível'


valor1 = float(input('Informe o primeiro valor: '))
valor2 = float(input('Informe o segundo valor: '))
operacao = input('Informe a operação desejada:\n'
                 '+ para Soma\n'
                 '- para Subtração\n'
                 '/ para Divisão\n'
                 '* para Multiplicação\n'
                 'Opção: ')

print(f'O cálculo para {valor1} {operacao} {valor2} ='
      f' {calc_operacao(valor1, valor2, operacao)}')
