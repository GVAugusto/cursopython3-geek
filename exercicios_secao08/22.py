"""
22. Crie uma função que receba como parâmetro um valor inteiro e gere como saída
n linhas com pontos de exclamação, conforme o exemplo abaixo (para n = 5):
!
!!
!!!
!!!!
!!!!!
"""


def criaexclamacao(valor):
    texto = ''
    for i in range(1, valor+1):
        texto += (i * '!') + '\n'

    return texto


num = int(input('Favor informar o número de linhas de exclamação: '))

print(criaexclamacao(num))
