"""
18. Faça uma função que receba por parâmetro dois valores X e Z. Calcule e retorne o
resultado de X^Z para o programa principal. Atenção, não utilize nenhuma função
pronta de exponenciação.
"""


def exponencial(valorx, valorz):
    resultado = 1
    for _ in range(valorz):
        resultado *= valorx

    return resultado


numx = int(input('Favor informar valor para X: '))
numz = int(input('Favor informar valor para Z: '))

print(f'{numx}^{numz} = {exponencial(numx, numz)}')
