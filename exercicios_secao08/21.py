"""
21. Escreva uma função para determinar a quantidade de números primos abaixo N.
"""


def checkprimo(valor_check):
    cont = 0
    for i in range(1, valor_check+1):
        if valor_check % i == 0:
            cont += 1

    if cont == 2:
        return True
    return False


def quantidadeprimos(valor):
    primos = []
    for i in range(valor+1):
        if checkprimo(i):
            primos.append(i)

    return primos


num = int(input('Informe o número para análise dos primos: '))

print(f'Quantidade de números primos: {len(quantidadeprimos(num))}.\n'
      f'Números primos: {quantidadeprimos(num)}')
