"""
19. Faça uma função que retorne o maior fator primo de um número.
"""


def checkprimo(valor_check):
    cont = 0
    for i in range(1, valor_check+1):
        if valor_check % i == 0:
            cont += 1

    if cont == 2:
        return True
    return False


def fatorprimo(valor):
    fatores = []

    while valor > 1:
        if checkprimo(valor):
            fatores.append(valor)
            return fatores

        fator_maximo = int(valor ** 0.5)

        for i in range(fator_maximo, 1, -1):
            teste = valor % i
            if teste == 0:
                if checkprimo(i):
                    fatores.append(i)
                    valor /= i
                    valor = int(valor)

    return fatores


num = int(input('Favor informe um valor para cálculo do fator primo: '))

print(f'Os fatores primos do número {num} são: {fatorprimo(num)}.\n'
      f'O maior fator primo do número {num} é: {max(fatorprimo(num))}')
