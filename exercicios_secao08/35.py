"""
35. Faça uma função não-recursiva que receba um número inteiro positivo n e
retorne o fatorial quádruplo desse número. O fatorial quádruplo de um número
n é dado por: (2n)! / n!
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


def fatorial_quadruplo(valor):
    resultado = fatorial(2 * valor) / fatorial(valor)

    return resultado


num = int(input('Favor informar valor para calculo de fatorial quadruplo: '))

print(fatorial_quadruplo(num))
