"""
31. Faça uma função para calcular o número neperiano usando uma série.
A função deve ter como parâmetro o número de termos que serão somados (note que,
quanto maior o número, mais próxima a resposta estará do valor 'l')

l = somatório(n=0 a infinito) = 1/0! + 1/1! + 1/2! + 1/3! + 1/4! + ...
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


def neperiano(valor):
    soma = 0
    for i in range(valor+1):
        soma += 1 / fatorial(i)

    return soma


num = int(input('Favor informar número: '))

print(neperiano(num))
