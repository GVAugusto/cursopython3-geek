"""
39. Faça uma função 'Troque', que recebe duas variáveis reais A e B e troca o valor
delas (por exemplo: A recebe o valor de B e B recebe o valor de A)
"""


def troque(valora, valorb):
    aux = valora
    valora = valorb
    valorb = aux
    return valora, valorb


numa = float(input('Favor informar valor A: '))
numb = float(input('Favor informar valor B: '))

novoa, novob = troque(numa, numb)
print(f'Valores originais:\n'
      f'A: {numa} - B: {numb}\n'
      f'Valores trocados:\n'
      f'A: {novoa} - B: {novob}')
