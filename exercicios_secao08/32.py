"""
32. Faça uma função chamada 'simplifica' que recebe como parâmetro o numerador e o
denominador de uma fração. Esta função deve simplificar a fração recebida
dividindo o numerador e o denominador pelo maior fator possível. Por exemplo:

A fração 36/60 simplifica para 3/5 dividindo o numerador e o denominador por 12.

A função deve modificar as variáveis passadas como parâmetro.
"""


def check_menor(valor1, valor2):
    if valor1 > valor2:
        return valor2
    return valor1


def simplifica(numerador, denominador):
    menor = check_menor(numerador, denominador)
    for i in range(menor, 0, -1):
        if numerador % i == 0 and denominador % i == 0:
            numerador /= i
            denominador /= i
            return numerador, denominador, i


numer = int(input('Favor informar o numerador da fração: '))
denom = int(input('Favor informar o denominador da fração: '))

novo_num, novo_den, razao = simplifica(numer, denom)

print(f'Fração Inicial: {numer}/{denom}\n'
      f'Razão Simplificação: {razao}\n'
      f'Fração Simplificada: {novo_num}/{novo_den}')
