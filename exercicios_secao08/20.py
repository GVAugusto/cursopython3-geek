"""
20. Faça um algoritmo que receba um número inteiro positivo n e calcule o seu
fatorial, n!.
"""


def fatorial(valor):
    resultado = 1
    for i in range(1, valor+1):
        resultado *= i

    return resultado


num = int(input('Favor informar o valor para calculo de fatorial: '))

print(fatorial(num))
