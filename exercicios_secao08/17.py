"""
17. Faça uma função que receba dois números inteiros positivos por parâmetro e
retorne a soma dos N números inteiros existentes entre eles.
"""


def somainteiro(valor1, valor2):
    soma = 0
    for i in range(valor1+1, valor2):
        soma += i

    return soma


def somainteiroinclusive(valor1, valor2):
    soma = 0
    for i in range(valor1, valor2+1):
        soma += i

    return soma


num1 = int(input('Favor informar o primeiro valor: '))
num2 = int(input('Favor informar o segundo valor: '))

print(f'A soma dos números entre {num1} e {num2} é: {somainteiro(num1, num2)}')
print(f'A soma dos números entre {num1} e {num2} incluindo os valores citados é: '
      f'{somainteiroinclusive(num1, num2)}')