"""
24. Escreva uma função que gera um triângulo de altura e lados n e base 2*n-1.
Por exemplo, a saída para n=6 seria:

     *
    ***
   *****
  *******
 *********
***********
"""


def imprimetriangulo(linhas):
    aux = 1
    texto = ''

    for i in reversed(range(1, linhas + 1)):
        a = ' ' * i
        b = '*' * aux
        aux += 2
        texto += a + b + '\n'

    return texto


num = int(input('Favor informar número de linhas: '))

print(imprimetriangulo(num))
