"""
38. Faça uma função não-recursiva que receba um número inteiro positivo N e
retorne o fatorial exponencial desse número. Um fatorial exponencial é um inteiro
positivo N elevado à potência de N-1, que por sua vez é elevado à potencia de
N-2, e assim em diante. Ou seja:
n^(n-1^(n-2^(...)))
"""


def fatorial_exponencial(valor):
    resultado = valor
    for i in range(1, valor):
        resultado = resultado ** (valor - i)

    return resultado


num = int(input('Favor informar número para calculo de Fatorial Exponencial: '))

print(fatorial_exponencial(num))
