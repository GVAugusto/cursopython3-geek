"""
08. Sejam a e b catetos de um triângulo, onde a hipotenusa é obtida pela equação:
hipotenusa = (a^2 + b^2) ^1/2. Faça uma função que receba os valores de a e b e
calcule o valor da hipotenusa através da equação.
"""


def valor_hipotenusa(catetoa, catetob):
    hipotenusa = ((catetoa ** 2) + (catetob ** 2)) ** 0.5
    return round(hipotenusa, 2)


valora = float(input('Informa o valor do cateto A: '))
valorb = float(input('Informa o valor do cateto B: '))

print(f'Cateto a: {valora}\n'
      f'Cateto b: {valorb}\n'
      f'Hipotenusa: {valor_hipotenusa(valora, valorb)}')
