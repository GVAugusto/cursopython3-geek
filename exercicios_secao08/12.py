"""
12. Escreva uma função que receba um número inteiro maior do que zero e retorne
a soma de todos os seus algarismos. Por exemplo, ao número 251 corresponderá o
valor 8 (2 + 5 + 1). Se o número lido não for maior do que zero, o programa
terminará com a mensagem "Número inválido".
"""


def check_numero(valor):
    if valor > 0:
        return 1
    return 0


def soma_algarismos(valor):
    soma = 0
    for num in list(str(valor)):
        soma += int(num)
    return soma


numero = int(input('Favor informar um número maior que zero: '))

if check_numero(numero):
    print(f'A soma dos algarismos é: {soma_algarismos(str(numero))}')
else:
    print('Número inválido')
