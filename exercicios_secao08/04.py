"""
04. Faça uma função para verificar se um número é um quadrado perfeito. Um
quadrado perfeito é um número inteiro, não negativo que pode ser expresso como o
quadrado de outro número inteiro. Ex: 1, 4, 9.
"""


def quadrado_perfeito(valor):
    if valor != int(valor) or valor < 0:
        return 0

    teste = valor ** (1/2)

    if teste == int(teste):
        return 1
    return 0


numero = int(input('Favor informar um número: '))

if quadrado_perfeito(numero):
    print('Valor é um quadrado perfeito')
else:
    print('Valor não é um quadrado perfeito')
