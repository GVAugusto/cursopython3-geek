"""
16. Faça uma função chamada DesenhaLinha. Ele deve desenhar uma linha na tela usando
vários simbolos de igual (Ex.: ======). A função recebe por parâmetro quantos sinais
de igual serão mostrados.
"""


def desenhalinha(qtd_simbolo):
    linha = ''
    for _ in range(qtd_simbolo):
        linha += '='

    return linha


qtde = int(input('Informe a quantidade de "=" que serão mostrados na linha: '))

print(desenhalinha(qtde))
