"""
40. Faça uma função que receba um vetor de inteiros e retorne quantos valores
pares ele possui.
"""


def check_par(valores):
    total = 0
    for i in valores:
        if i % 2 == 0:
            total += 1

    return total


inteiros = []
count = 0

while count < 5:
    inteiros.append(int(input(f'Favor informar valor {count+1} de 5: ')))
    count += 1


print(f'Lista: {inteiros}\n'
      f'Quantidade de pares: {check_par(inteiros)}')
