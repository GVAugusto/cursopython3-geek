"""
41. Faça uma função que receba um vetor de inteiros e retorne o maior valor.
"""


def maior(valores):
    return max(valores)


inteiros = []
count = 0

while count < 5:
    inteiros.append(int(input(f'Favor informar valor {count+1} de 5: ')))
    count += 1


print(f'Lista: {inteiros}\n'
      f'Maior valor da lista: {maior(inteiros)}')
