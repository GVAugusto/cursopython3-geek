"""
25. Faça uma função que receba um inteiro N como parâmetro, calcule e retorne o
resultado da seguinte série:
S = 2/4 + 5/5 + 10/6 + ... + (N^2 + 1)/(N+3)
"""


def calculo_serie(valor):
    serie = 0
    for i in range(1, valor+1):
        numerador = (i ** 2) + 1
        denominador = i + 3
        serie += numerador / denominador

    return serie


num = int(input('Favor informar um valor para calculo de série: '))

print(calculo_serie(num))
