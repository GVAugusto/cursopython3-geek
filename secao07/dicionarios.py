"""
Dicionários

OBS: Em algumas linguagens de programação, os dicionários Python são conhecidos por
mapas.

Dicinonários são coleções do tipo chave/valor.
- Chave  0  1  2
- Valor [1, 2, 3]

Dicionários são representados por chaves {}

Observações importantes:
 - Chave e Valor são separados por ":". exemplo: "chave:valor"
 - Tanto Chave quanto Valor podem ser de qualquer tipo de dados
 - Podemos misturar tipos de dados
"""

print(type({}))

# Criação de dicionários

# Forma 1 (Mais comum)

paises = {'br': 'Brasil', 'eua': 'Estados Unidos', 'py': 'Paraguai'}

print(paises)
print(type(paises))


# Forma 2 (Menos comum)

paises = dict(br='Brasil', eua='Estados Unidos', py='Paraguai')

print(paises)
print(type(paises))


# Acessando elementos

# Forma 1 - Acessando via Chave, da mesma forma que lista/tupla

print(paises['br'])
print(paises['py'])

# OBS: Caso tentemos fazer acesso a uma chave inexistente, teremos KeyError
# print(paises['ru'])

# Forma 2 - Acessando via Get - Recomendado

print(paises.get('br'))
print(paises.get('ru'))  # País inexistente - Trata erro com Get - Retorna "None"

# Utilizando condicional com get retornando None

russia = paises.get('ru')

if russia:
    print('Encontrei o país')
else:
    print('Não encontrei o país')

# Utilizando condicional com get retornando valor

pais = paises.get('py')

if pais:
    print('Encontrei o país')
else:
    print('Não encontrei o país')

# Utilizando get e inserindo resposta padrão em caso de não encontrado no dict

pais = paises.get('py', 'País não encontrado')
print(pais)

pais = paises.get('ru', 'País não encontrado')
print(pais)

# Podemos verificar se uma determinada chave se encontra em um dicionário
# Retorna True ou False na consulta do dicionário
print('br' in paises)
print('ru' in paises)
print('eua' in paises)

if 'ru' in paises:
    russia = paises['ru']


# Podemos utilizar qualquer tipo de dado (int, float, string, boolean)
# Inclusive lista, tupla, dicionário, como chaves de dicionários

# Criação de dicionário contendo tupla(float,float):string
# Tuplas são bastante interessantes de serem utilizadas como chave de dicionários
# Pois tuplas são imutáveis
localidades = {
    (35.6895, 39.6917): 'Escritório em Tóquio',
    (40.7128, 74.0060): 'Escritório em Nova York',
    (37.7749, 122.4194): 'Escritório em São Paulo',
}

print(localidades)
print(type(localidades))


# Adicionar elementos em um dicionário

receita = {'jan': 100, 'fev': 200, 'mar': 300}

print(receita)
print(type(receita))

# Forma 1 (Mais comum)
receita['abr'] = 350
print(receita)

# Forma 2
novo_dado = {'mai': 500}
receita.update(novo_dado)
# Alternativa: receita.update({'mai': 500})

print(receita)


# Atualizando dados em um dicionário

# Forma 1 (Mais comum)
receita['abr'] = 400
print(receita)

# Forma 2
receita.update({'mai': 550})
print(receita)

# CONCLUSÃO 1: A forma de adicionar ou atualizar dados em um dicionário é a mesma
# CONCLUSÃO 2: Em dicionários NÃO podemos ter chaves repetidas

# Removendo dados de um dicionário

# Forma 1 (Mais Comum)
receita.pop('mar')
print(receita)

# OBS: Necessário sempre informar chave ao "pop"
# Caso a chave não exista, um KeyError é gerado

ret = receita.pop('abr')
print(ret)
print(receita)

# OBS: Quando utilizamos o POP, o valor associado à chave é retornado

# Forma 2
del receita['fev']
print(receita)

# OBS: Se a chave não existir, irá gerar KeyError
# OBS: Utilizando o DEL não retorna o valor da chave removida


"""
Imagine que você tem um comércio eletrônico, onde temos um carrinho de compras
na qual adicionamos produtos.

Carrinho de compras:
 Produto 1:
  - nome
  - quantidade
  - preço
 Produto 2:
  - nome
  - quantidade
  - preço
  
"""

# Opção 1 - Poderíamos criar uma lista para gerar o carrinho

carrinho = []

produto1 = ['Playstation 4', 1, 2300.00]
produto2 = ['God of War 4', 1, 150.00]

carrinho.append(produto1)
carrinho.append(produto2)

print(carrinho)

"""
Problemas no uso de lista:
 - Necessário saber o índice de cada informação no produto
"""

# Opção 2 - Poderíamos criar uma tupla para gerar o carrinho

produto1 = ('Playstation 4', 1, 2300.00)
produto2 = ('God of War 4', 1, 150.00)

carrinho = (produto1, produto2)

print(carrinho)

"""
Problemas no uso de tupla:
 - Necessário saber o índice de cada informação no produto
"""

# Opção 3 - Poderíamos criar um dicionário para gerar o carrinho

carrinho = []

produto1 = {'nome': 'Playstation 4', 'quantidade': 1, 'preco': 2300.00}
produto2 = {'nome': 'God of War 4', 'quantidade': 1, 'preco': 150.00}

carrinho.append(produto1)
carrinho.append(produto2)

print(carrinho)

"""
Desta forma, facilmente adicionamos ou removemos produtos do carrinho
Em cada produto podemos ter a certeza sobre cada informação
"""

# Métodos de dicionários

d = dict(a=1, b=2, c=3)

print(d)
print(type(d))

# Método Copy - Copiar dicionário

# Forma 1 - Deep Copy
novo = d.copy()
print(novo)
print(d)

novo['d'] = 4
print(d)
print(novo)

# Forma 2 - Shallow Copy
novo = d
print(novo)
print(d)

novo['d'] = 4
print(d)
print(novo)


# Método FromKeys - Criação de dicionários, método não usual
# Recebe dois parâmetros, um para chave e um para valor, onde a chave pode ser iterável

# Forma 1 - Inserção simples
outro = {}.fromkeys('a', 'b')
print(outro)
print(type(outro))

# Forma 2 - Inserção iterável lista,valor
usuario = {}.fromkeys(['nome', 'pontos', 'email', 'profile'], 'desconhecido')
print(usuario)
print(type(usuario))

# Forma 3 - Inserção iterável - várias letras,valor
veja = {}.fromkeys('teste', 'valor')
print(veja)

# Forma 4 - Utilizando range na iteração - range, valor
veja = {}.fromkeys(range(1, 11), 'valor')
print(veja)


# Método Clear - Limpar dicionário (zerar)

d.clear()
print(d)
