"""
Módulo Collections - Deque

Podemos dizer que o deque é uma lista de alta perfomance.
"""

# Import
from collections import deque

# Criando deques
deq = deque('geek')

print(deq)


# Adicionando elementos no Deque
deq.append('y')
print(deq)

deq.appendleft('k')  # Insere no começo da lista
print(deq)


# Removendo elementos
print(deq.pop())  # Remove e retorna o último item da lista
print(deq)

print(deq.popleft())  # Remove e retorna o primeiro item da lista
print(deq)
