"""
Módulo Collections - Named Tuple

# Recapitulando Tupla
tupla = (1, 2, 3)
print(tupla[1])

Named Tuple -> São tuplas diferenciadas, onde, especificamos um nome para a mesma
e também parâmetros.
"""

# Fazendo import
from collections import namedtuple

# Precisamos definir o nome e os parâmetros

# Forma 1 - Declaração Named Tuple
cachorro1 = namedtuple('cachorro', 'idade raça nome')

# Forma 2 - Declaração Named Tuple
cachorro2 = namedtuple('cachorro', 'idade, raça, nome')

# Forma 3 - Declaração Named Tuple
cachorro3 = namedtuple('cachorro', ['idade', 'raça', 'nome'])

# Utilização
ray1 = cachorro1(idade=2, raça='Chow-Chow', nome='Ray')
ray2 = cachorro2(idade=2, raça='Poodle', nome='Ray')
ray3 = cachorro3(idade=2, raça='Vira-Lata', nome='Ray')

print(ray1)
print(ray2)
print(ray3)

# Acessando os dados

# Forma 1
print(ray1[0])
print(ray1[1])
print(ray1[2])

# Forma 2
print(ray1.idade)
print(ray1.raça)
print(ray1.nome)


# Capturando dados das tuplas
print(ray1.index('Chow-Chow'))
print(ray1.count('Chow-Chow'))