"""
Módulo Collections - Counter (Contador)

Collections -> High-performance Container Datetypes

Counter -> Recebe um iterável como parâmetro e cria um objeto do tipo:
    Collections Counter que é parecido com um Dicionário, contendo como chave
    o elemento da lista passado como parâmetro e como valor a quantidade de
    ocorrências deste elemento
"""

# Realizando o Import

from collections import Counter

lista = [1, 1, 1, 2, 2, 3, 3, 3, 3, 1, 1, 2, 2, 4, 4, 4, 5, 5, 5, 5, 3, 45, 45, 66, 66, 43, 34]

# Utilizando o Counter

# Exemplo 1
res = Counter(lista)

print(type(res))
print(res)

"""
Cria uma lista com as chaves sendo os valores encontrados, e os valores sendo a 
quantidade de ocorrências.

Counter({1: 5, 3: 5, 2: 4, 5: 4, 4: 3, 45: 2, 66: 2, 43: 1, 34: 1})
"""

# Exemplo 2
print(Counter('Geek University'))

# Exemplo 3
texto = """Super Mario All-Stars é uma compilação de jogos eletrônicos de 
plataforma desenvolvido pela Nintendo Entertainment Analysis & Development e 
publicado pela Nintendo em 1993 para o Super Nintendo Entertainment System 
(SNES). O jogo contém recriações de quatro jogos da série Mario lançadas 
para o Nintendo Entertainment System (NES) ou o Family Computer Disk System: 
Super Mario Bros., Super Mario Bros.: The Lost Levels, Super Mario Bros. 2 e 
Super Mario Bros. 3. Os remakes adaptam as premissas originais dos jogos e 
design de níveis para o SNES com gráficos e música melhorados. Assim como nos 
jogos originais, o jogador controla o encanador italiano Mario e seu irmão Luigi 
por meio de mundos temáticos, coletando power-ups, evitando obstáculos e 
encontrando áreas secretas. As mudanças incluem a adição de rolagem parallax 
e física modificada, enquanto alguns bugs foram corrigidos."""

palavras = texto.split()
# print(palavras)

res = Counter(palavras)
print(res)

# Método most_common(n) - Localiza as N ocorrências mais comum da variável
print(res.most_common(5))
print(res.most_common(10))

"""
Para visualizar mais informações a respeito das Collections:
docs.python.org/3/library/collections.html
"""