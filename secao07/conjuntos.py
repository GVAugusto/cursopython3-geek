"""
Conjuntos

- Conjuntos em qualquer linguagem de programação, estamos fazendo referência a
Teoria dos Conjuntos da Matemática.

Em Python os conjuntos são chamados de Sets.

Dito isto, da mesma forma que na matemática:
 - Sets (conjuntos) não possuem valores duplicados;
 - Sets (conjuntos) não possuem valores ordenados;
 - Elementos não são acessados via índice, ou seja, conjuntos não são indexados;

Conjuntos são bons para se utilizar quando precisamos armazenar elementos mas não
nos importamos com a ordenação deles. Quando não precisamos nos preocupar com
chaves, valores e itens duplicados.

Os conjuntos (sets) são referenciados em Python com chaves {}.

Diferença entre Conjuntos (Sets) e Mapas (Dicionários) em Python:
 - Um dicionário tem chave/valor;
 - Um conjunto tem apenas valor;
"""

# Definindo um conjunto

# Forma 1 (Mais comum)

s = set({1, 2, 3, 4, 5, 5, 6, 7, 2, 3})  # Repare que temos valores repetidos

print(s)
print(type(s))

# OBS: Ao criar um conjunto, em caso de valores repetidos, estes são descartados

# Forma 2
s = {1, 2, 3, 4, 5, 5, 6, 7, 2, 3}  # Repare que temos valores repetidos

print(s)
print(type(s))


# Inserindo string em set:
s = set('Geek University')
print(s)


# Gerando conjunto (set) a partir de lista
lista = [1, 2, 3, 4, 5, 5, 6, 7, 2, 3]
print(lista)

s = set(lista)
print(s)


# Gerando conjunto (set) a partir de tupla
tupla = (1, 2, 3, 4, 5, 5, 6, 7, 2, 3)
print(tupla)

s = set(tupla)
print(s)


# Verificando se elemento existe no conjunto (set)
if 3 in s:
    print('Tem o 3')
else:
    print('Não tem o 3')


# Importante lembrar que além de não termos valores duplicados, não temos ordem

# Listas aceitam valores duplicados
lista = [99, 2, 34, 23, 2, 12, 1, 44, 5, 34]
print(f'Lista: {lista} com {len(lista)} elementos')

# Tuplas aceitam valores duplicados
tupla = (99, 2, 34, 23, 2, 12, 1, 44, 5, 34)
print(f'Tupla: {tupla} com {len(tupla)} elementos')

# Dicionários não aceitam chaves duplicadas
dicionario = {}.fromkeys(lista, 'dict')
print(f'Dicionário: {dicionario} com com {len(dicionario)} elementos')

# Conjuntos não aceitam valores duplicados, ordenando de forma aleatória os itens
conjunto = {99, 2, 34, 23, 2, 12, 1, 44, 5, 34}
print(f'Conjunto: {conjunto} com {len(conjunto)} elementos')


# Assim como todo outro conjunto Python podemos colocar tipos de dados misturados em Sets

s = {1, 'b', False, 34.22, 44}
print(s)
print(type(s))

for valor in s:
    print(valor)


"""
Usos interessantes de Sets

Imagine que fizemos um formulário de cadastro de visitantes em uma feira ou museu
e os visitantes informam manualmente a cidade de onde vieram.

Nós adicionamos cada cidade em uma lista Python, já que na lista podemos adicionar
novos elementos e ter repetição.
"""

cidades = ['Belo Horizonte', 'São Paulo', 'Campo Grande', 'Cuiabá', 'São Paulo', 'Cuiabá']

print(cidades)
print(len(cidades))

# Precisamos saber quantas cidades distintas, únicas, temos

print(len(set(cidades)))


# Adicionando elementos em um conjunto (Set)

s = {1, 2, 3}
s.add(4)
print(s)

# Gerar um novo add com o mesmo valor, ignora a inserção, sem gerar erro
s.add(4)
print(s)


# Remover elementos em um conjunto (Set)

# Forma 1 - Comando remove() - Nenhum valor é retornado
s = {1, 2, 3}
print(s)
s.remove(3)  # Necessário informar o valor, NÃO É O INDEX
print(s)

# OBS: Caso valor não exista no conjunto, irá gerar KeyError
# s.remove(4)

# Forma 2 - Comando discard() - Nenhum valor é retornado
s = {1, 2, 3}
print(s)
s.discard(3)  # Necessário informar o valor, NÃO É O INDEX
print(s)

# OBS: Não gera erro caso valor não encontrado, diferentemente do remove()


# Copiando um conjunto para outro
s = {1, 2, 3}
print(s)

# Forma 1 - Deep Copy
novo = s.copy()
print(novo)

novo.add(4)

print(novo)
print(s)

# Forma 2 - Shallow Copy
novo = s
print(novo)

novo.add(4)

print(novo)
print(s)


# Método Clear - Remove todos os itens do conjunto

print(s)
s.clear()
print(s)


"""
Métodos Matemáticos de Conjuntos

Imagine que temos dois conjuntos: Um contendo estudantes do curso Python e um
contendo estudantes do curso de Java.
"""

estudantes_python = {'Marcos', 'Patricia', 'Ellen', 'Pedro', 'Julia', 'Guilherme'}
estudantes_java = {'Fernando', 'Gustavo', 'Julia', 'Ana', 'Patricia'}

"""
Veja que alguns alunos que estudam Python também estudam Java.

Necessário gerar um conjunto com nomes de estudantes únicos
"""

# Forma 1 - Método Union()

unicos1 = estudantes_python.union(estudantes_java)
print(unicos1)

# Forma 2 - Utilizando caractere pipe "|"

unicos2 = estudantes_python|estudantes_java
print(unicos2)


# Criar um conjunto de estudantes que estão em ambos os cursos

# Forma 1 - Método Intersection()

ambos1 = estudantes_python.intersection(estudantes_java)
print(ambos1)

# Forma 2 - Utilizando &
ambos2 = estudantes_python&estudantes_java
print(ambos2)


# Criar um conjunto de estudantes que estão em apenas 1 curso

# Método Difference()
so_python = estudantes_python.difference(estudantes_java)
print(so_python)

so_java = estudantes_java.difference(estudantes_python)
print(so_java)


# Soma, Valor Máximo, Valor Mínimo, Tamanho

s = {1, 2, 3, 4, 5, 6}

print(sum(s))
print(max(s))
print(min(s))
print(len(s))
