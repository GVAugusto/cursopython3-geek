"""
Tuplas (Tuple)

Tuplas são bastante parecidas com listas.

Existem duas diferenças básicas:

 1 - As tuplas são representadas por parenteses ()
 2 - As tuplas são imutáveis, ou seja, ao criar uma tupla ela não muda. Toda
 operação em uma tupla gera uma nova tupla.

"""

print(type(()))

# CUIDADO 1 : As tuplas são representadas por parenteses

tupla1 = (1, 2, 3, 4, 5, 6)
print(tupla1)
print(type(tupla1))

tupla2 = 1, 2, 3, 4, 5, 6
print(tupla2)
print(type(tupla2))


# CUIDADO 2: Tuplas com 1 elemento

tupla3 = (4)  # Isso não é uma tupla
print(tupla3)
print(type(tupla3))

tupla4 = (4,)  # Isso é uma tupla
print(tupla4)
print(type(tupla4))

tupla5 = 5,  # Isso é uma tupla
print(tupla5)
print(type(tupla5))

# CONCLUSÃO: Podemos concluir que tuplas são definidias pela vírgula e não pelo parenteses

# Podemos gerar uma tupla dinamicamente com range
tupla = tuple(range(11))
print(tupla)

# Desempacotamento de Tupla

tupla = ('Geek University', 'Programação em Python: Essencial')
escola, curso = tupla

print(escola)
print(curso)

# Gera ValueError se número de elementos for diferente para desempacotar

# Métodos para: adição e remoção de elementos nas tuplas não existem!

# Funções de Soma, Valor Máximo, Valor Mínimo e Tamanho, iguais em lista

print(sum(tupla1))
print(max(tupla1))
print(min(tupla1))
print(len(tupla1))


# Concatenação de Tuplas

tupla1 = (1, 2, 3)
print(tupla1)

tupla2 = (4, 5, 6)
print(tupla2)

print(tupla1 + tupla2)  # Tuplas são imutáveis
print(tupla1)
print(tupla2)

tupla3 = tupla1 + tupla2
print(tupla1)
print(tupla2)
print(tupla3)

tupla1 += tupla2  # Tuplas são imutáveis, mas podem ser sobrescritas
print(tupla1)


# Verificar se determinado elemento está na tupla
tupla = (1, 2, 3)
print(3 in tupla)
print(33 in tupla)


# Iterando sobre uma tupla
for n in tupla:
    print(n)

for indice, valor in enumerate(tupla):
    print(indice, valor)


# Contando elementos dentro de uma tupla
tupla = ('a', 'b', 'c', 'd', 'e', 'a', 'b')

print(tupla.count('a'))
print(tupla.count('b'))
print(tupla.count('c'))


# Criar uma tupla a partir de uma string
escola = tuple('Geek University')
print(escola)

print(escola.count('e'))

"""
Utilização de tuplas

Devemos usar tuplas SEMPRE que não precisarmos modificar os dados contidos em uma
coleção
"""

# Exemplo 1
meses = ('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
         'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro')

# O acesso a elementos de uma Tupla é semelhante ao de uma lista
print(meses[5])


# Iterar com while
i = 0
while i < len(meses):
    print(meses[i])
    i += 1


# Verificando em qual index um elemento está na tupla
print(meses.index('Junho'))
print(meses.index('Junho', 5))  # Busca valor a partir de um dado index

# Caso elemento não existe irá gerar ValueError
# print(meses.index('Junhoo'))


# Slicing
# tupla[inicio:fim:passo]

print(meses[0::])
print(meses[5::])
print(meses[5:9])


"""
Porque utilizar tuplas?

 - Tuplas são mais rápidas do que listas
 - Tuplas deixam seu código mais seguro (imutabilidade)
 
 Trabalhar com elementos imutáveis traz segurança para o código
"""


# Copiando uma tupla para outra

tupla = (1, 2, 3)
print(tupla)

nova = tupla  # Na tupla não há Shallow Copy

print(nova)
print(tupla)

outra = (4, 5, 6)
nova += outra

print(nova)
print(tupla)
