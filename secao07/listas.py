"""
Listas

Listas em Python funcionam como vetores/matrizes (arrays) em outras linguagens,
com a diferença de serem DINÂMICOS e também de podermos colocar QUALQUER tipo
de dado.

Linguagens C/Java: Arrays
    - Possuem tamanho e tipo de dado fixo: nessas linguagens se criar um array
    do tipo int e com tamanho 5, este array será sempre do tipo inteiro e poderá
    ter sempre no máximo 5 valores.

Em Python
    - Dinâmico: Não possui tamanho fixo, ou seja, podemos criar a lista e
    simplesmente ir adicionando elementos, de acordo com capacidade de memória
    do sistema em que o aplicativo está em execução.

    - Tipo de dado: Não possuem tipo de dado fixo, ou seja, podemos trabalhar com
    qualquer tipo de dado.

As listas em Python são representadas em colchetes: []

Listas são mutáveis, elas podem mudar constantemente.
"""

print(type("a"))
print(type(1))
print(type(True))
print(type([]))

lista1 = [1, 99, 4, 27, 15, 22, 3, 1, 44, 42, 27]
lista2 = ['G', 'e', 'e', 'k', ' ', 'U', 'n', 'i', 'v', 'e', 'r', 's', 'i', 't', 'y']
lista3 = []
lista4 = list(range(11))
lista5 = list('Geek University')

print(f'Lista1: {lista1}\n'
      f'Lista2: {lista2}\n'
      f'Lista3: {lista3}\n'
      f'Lista4: {lista4}\n'
      f'Lista5: {lista5}\n')

# Podemos facilmente checar se determinado valor está contido na lista

num = 8

if num in lista4:
    print(f'Encontrei o número {num}\n')
else:
    print(f'Não encontrei o número {num}\n')

letra = 'e'

if letra in lista5:
    print(f'Encontrei a letra {letra}\n')
else:
    print(f'Não encontrei a letra {letra}\n')

"""
Consultar possíveis comandos para lista
print(dir(lista5))

Para verificar em detalhes um comando, segurar CTRL e clica sobre o comando
no PyCharm.
"""
# Podemos facilmente ordenar uma lista

lista1.sort()
print(lista1)

lista5.sort()
print(lista5)

# Podemos facilmente contar o número de ocorrências de um valor em uma lista

print(lista1.count(1))

print(lista5.count('e'))

# Adicionar elementos em listas

"""
Para adicionar elementos ou valores em listas utilizamos a função "append"

OBS.: Com append conseguimos adicionar apenas 1 elemento por vez.
lista1.append(12, 34, 56) # Erro
"""

print(lista1)
lista1.append(42)
print(lista1)

lista1.append([8, 3, 1])  # Insere a lista como um elemento (sublista)
print(lista1)

if [8, 3, 1] in lista1:
    print('Encontrei a lista')
else:
    print('Não encontrei a lista')

"""
Para adicionar vários elementos ou valores em listas utilizamos a função "extend"

OBS.: Não aceita inserção de um único dado (feito com append)
"""

lista1.extend([123, 44, 67])  # Insere cada elemento na lista, como parte da lista existente
print(lista1)


"""
Para adicionar elemento na lista em posição desejada, utilizar função "insert"

OBS.: Isso não substitui o valor no local original, apenas desloca o restante
da lista.
"""

lista1.insert(2, 'Novo valor')
print(lista1)


"""
Podemos juntar duas listas de 3 formas:

lista6 = lista1 + lista2
lista1 += lista2
lista1.extend(lista2)
"""
lista6 = lista1 + lista2
print(lista6)


"""
Podemos reverter a ordem da lista de 2 formas:
lista1.reverse()
lista2.reverse()

ou

print(lista1[::-1])
print(lista2[::-1])
"""

lista1.reverse()
lista2.reverse()

print(lista1)
print(lista2)


# Copiar uma lista

lista6 = lista2.copy()
print(lista6)


# Verificar tamanho da lista - len(<lista>)

print(len(lista2))


# Remover último elemento da lista
# OBS.: O comando pop remove e retorna o último elemento
print(lista5)
lista5.pop()
print(lista5)


# Podemos remover um elemento utilizando pop e um índice
# OBS.: Os elementos a direita do índice são deslocados para a esquerda
# OBS.: Se não houver o elemento no índice informado, teremos IndexError
print(lista5)
lista5.pop(2)
print(lista5)


# Podemos remover todos os elementos (zerar a lista)
print(lista5)
lista5.clear()
print(lista5)


# Podemos repetir elementos em uma lista
nova = [1, 2, 3]
print(nova)
nova *= 3
print(nova)


# Podemos converter string em lista

# Exemplo 1
curso = 'Programação em Python: Essencial'
print(curso)
curso = curso.split()
print(curso)

# OBS.: Por padrão o split separa os elementos da lista pelo espaço entre elas

# Exemplo 2
curso = 'Programação,em,Python:,Essencial'
print(curso)
curso = curso.split(',')
print(curso)


# Convertendo uma lista em uma string

lista6 = ['Programação', 'em', 'Python:', 'Essencial']
print(lista6)

# Insere espaço em branco entre cada elemento da lista
curso = ' '.join(lista6)
print(curso)

curso = '$'.join(lista6)
print(curso)

# É possível criar listas com todos os tipos de dados, mesmo que misturados
lista7 = [1, 2.34, True, 'Geek', 'd', [1, 2, 3], 45354657654]
print(lista7)
print(type(lista7))

# Iterando sobre listas
# Exemplo 1


for elemento in lista1:
    print(elemento)

# Exemplo 2
soma = 0

for elemento in lista4:
    soma += elemento

print(soma)

# Exemplo 3

carrinho = []
produto = ''

while produto != 'sair':
    print('Adicione um produto na lista ou digite "sair" para sair: ')
    produto = input()

    if produto != 'sair':
        carrinho.append(produto)

for produto in carrinho:
    print(produto)

# Utilizando variáveis em listas

numeros = [1, 2, 3, 4, 5]
print(numeros)

num1 = 1
num2 = 2
num3 = 3
num4 = 4
num5 = 5

numeros = [num1, num2, num3, num4, num5]
print(numeros)


# Fazemos acessos aos elementos de forma indexada
#           0         1         2        3
cores = ['verde', 'amarelo', 'azul', 'branco']

print(cores[0])  # verde
print(cores[1])  # amarelo
print(cores[2])  # azul
print(cores[3])  # branco


# Fazer acesso aos elementos de forma indexada inversa

print(cores[-1])  # branco
print(cores[-2])  # azul
print(cores[-3])  # amarelo
print(cores[-4])  # verde


for cor in cores:
    print(cor)

indice = 0
while indice < len(cores):
    print(cores[indice])
    indice += 1


# Gerar indice em um for
# Enumerate gera indices para a lista

for indice, cor in enumerate(cores):
    print(indice, cor)


# Listas aceitam valores repetidos

lista8 = []

lista8.append(42)
lista8.append(42)
lista8.append(33)
lista8.append(33)
lista8.append(42)

print(lista8)


# Outros métodos não tão importantes mas também úteis

# Encontrar o índice de um elemento na lista

lista9 = [5, 6, 7, 5, 8, 9, 10]
print(lista9)

# Em qual indice da lista está o valor 6?
print(lista9.index(6))

# Em qual indice da lista está o valor 9?
print(lista9.index(9))

# OBS.: Caso não tenha o número na lista, será apresentado erro (ValueError)
# print(lista9.index(19)) # Gera ValueError

# Em qual indice da lista está o valor 5 - Valor Duplicado?
print(lista9.index(5))  # Retorna apenas o primeiro índice encontrado na lista

# Podemos fazer busca dentro de um range, por qual índice começa a busca
print(lista9.index(5, 1))  # busca o valor 5 a partir do índice 1
print(lista9.index(5, 2))  # busca o valor 5 a partir do índice 2

# Podemos fazer busca dentro de um range, início/fim
print(lista9.index(5, 3, 6))  # busca o valor 5 entre os índices 3 e 6


# Revisão de slicing
# lista[inicio:fim:passo]
# range[inicio:fim:passo]

# Trabalhando com slice de lista com o parâmetro 'início'

lista = [1, 2, 3, 4]

print(lista[1:])  # Inicia a apresentação da lista do índice 1 em diante
print(lista[::])  # Mostra todos os elementos
print(lista[3:])  # Inicia a apresentação da lista do índice 3 em diante


# Trabalhando com slice de lista com o parâmetro 'fim'

print(lista[:3])  # Apresenta a partir do primeiro índice (0) até 3 - 1 (2)
print(lista[:4])  # Apresenta a partir do primeiro índice (0) até 4 - 1 (3)
print(lista[1:3])  # Apresenta a partir do índice 1 até 3 - 1 (2)

print(lista[:-1])  # Apresenta o índice de 0 até o fim menos o último índice
print(lista[:-2])  # Apresenta o índice de 0 até o fim menos os 2 últimos índice
print(lista[1:-1])  # Apresenta a partir do índice 1 até o fim menos o último índice

# Trabalhando com slice em lista com o parâmetro 'passo'

print(lista[1::2])  # Apresenta a partir do índice 1, de 2 em 2 índices
print(lista[::2])  # Apresenta a partir do índice 0, de 2 em 2 índices
print(lista[::3])  # Apresenta a partir do índice 0, de 3 em 3 índices
print(lista[1::3])  # Apresenta a partir do índice 1, de 3 em 3 índices

print(lista[2::-1])  # Apresenta a lista de forma invertida a partir do índice 2
print(lista[1::-1])  # Apresenta a lista de forma invertida a partir do índice 1


# Invertendo valores em uma lista

nomes = ['Geek', 'University']
nomes[0], nomes[1] = nomes[1], nomes[0]
print(nomes)

nomes = ['Geek', 'University']
nomes.reverse()
print(nomes)


# Soma, Valor Máximo, Valor Mínimo, Tamanho

# Se os valores forem todos inteiros ou reais

list = [1, 2, 3, 4, 5, 6]

print(sum(list))
print(max(list))
print(min(list))
print(len(list))


# Transformar uma lista em tupla
# Diferença inicial, lista entre [] tuplas entre ()
print(list)
print(type(list))

tupla = tuple(list)
print(tupla)
print(type(tupla))


# Desempacotamento de listas

lista10 = [1, 2, 3]
num1, num2, num3 = lista10

print(num1)
print(num2)
print(num3)

"""
Caso a quantidade de valores na lista seja maior que o de variáveis, ou vice versa,
irá gerar erro

Exemplo - 4 itens na lista para apenas 3 variáveis:
lista10 = [1, 2, 3, 4]
num1, num2, num3 = lista10

print(num1)
print(num2)
print(num3)

Erro - Too many values to unpack (expected 3)

Exemplo - 3 itens na lista para 4 variáveis:
lista10 = [1, 2, 3]
num1, num2, num3, num4 = lista10

print(num1)
print(num2)
print(num3)
print(num4)

Erro - Not enough values to unpack (expected 4, got 3)
"""

# Copiando uma lista para outra (Shallow Copy e Deep Copy)

# Forma 1 - Deep Copy

lista = [1, 2, 3]
print(lista)

nova = lista.copy()
print(nova)

nova.append(4)
print(lista)
print(nova)

"""
A cópia de uma lista para outra, gera uma nova lista independente, ou seja,
a modificação de uma lista não afeta a outra. Em Python isso é chamado de 
Deep Copy (cópia profunda).
"""

# Forma 2 - Shadow Copy

lista = [1, 2, 3]
print(lista)

nova = lista  # cópia
print(nova)

nova.append(4)
print(lista)
print(nova)

"""
A cópia de uma lista para outra através de atribuição, cria uma nova lista
dependente da outra, onde toda alteração reflete em ambas listas.
Isso em Python é chamado de Shallow Copy.
"""