"""
18. Faça um programa que permita ao usuário entrar com uma matriz de 3x3 números
inteiros. Em seguida gere um array unidimensional pela soma dos números de cada
coluna da matriz e mostrar na tela esse array. Por exemplo, a matriz:

 5 -8 10
 1  2 15
25 10  7

Vai gerar um vetor, onde cada posição é a soma das colunas da matriz. A primeira
posição será: 5 + 1 + 25, e assim por diante:

31 4 32
"""

import random

matriz, linha, msoma = [], [], []

for i in range(3):
    for j in range(3):
        linha.append(random.randint(0, 99))
    matriz.append(list(linha))
    linha.clear()

for linha in matriz:
    print(linha)

for i in range(len(matriz)):
    soma = 0
    for j in range(len(matriz[i])):
        soma += matriz[j][i]
    msoma.append(soma)

print(f'Soma das colunas:\n'
      f'{msoma}')
