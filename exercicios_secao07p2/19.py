"""
19. Faça um programa que leia uma matriz de 5 linhas e 4 colunas contendo as
seguintes informações sobre alunos de uma disciplina, sendo todas as informações
do tipo inteiro:

 - Primeira coluna: número de matrícula (use um inteiro)
 - Segunda coluna: média das provas
 - Terceira coluna: média dos trabalhos
 - Quarta coluna: nota final

Elabore um programa que:

a. Leia as três primeiras informações de cada aluno
b. Calcule a nota final como sendo a soma das média das provas e da média dos
    trabalhos
c. Imprima a matrícula do aluno que obteve a maior nota final (assuma que só
    existe uma maior nota)
d. Imprima a média aritmética das notas finais
"""

import random

alunos = []
aluno = {}
aprovados, i = 0, 0

while i < 5:
    achou = False
    matricula = int(input(f'Favor informar a matrícula do aluno {i+1} de 5: '))
    mediap = random.randint(0, 10)
    mediat = random.randint(0, 10)
    notaf = mediap + mediat

    for linha in alunos:
        if linha['matricula'] == matricula:
            achou = True

    if achou is False:
        aluno = {'matricula': matricula, 'mediap': mediap, 'mediat': mediat,
                 'notaf': notaf}
        alunos.append(aluno)
        i += 1
    else:
        print('Matricula já existe, favor informar outra válida')

for i, linha in enumerate(alunos):
    print(f'Aluno {i+1}: {linha}')

notas = [alunos[i]['notaf'] for i in range(5)]
maior = alunos[notas.index(max(notas))]['matricula']
media_arit = round(sum(notas) / 5, 3)

print(f'Maior nota foi {max(notas)} do aluno {maior}')
print(f'Média aritmética das notas: {media_arit}')
