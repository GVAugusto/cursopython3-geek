"""
17. Leia uma matriz 10x3 com as notas de 10 alunos em 3 provas. Em seguida, escreva
o número de alunos cuja pior nota foi na prova 1, o números de alunos cuja pior nota
foi na prova 2, e o número de alunos cuja pior nota foi na prova 3.

Em caso de empate das piores notas de um aluno, o critério de desempate é arbitrário,
mas o aluno deve ser contabilizado apenas uma vez.
"""

import random

alunos, provas = [], []
prova1, prova2, prova3 = 0, 0, 0
listp1, listp2, listp3 = [], [], []

for i in range(10):
    for j in range(3):
        provas.append(random.randint(0, 10))
    alunos.append(list(provas))
    provas.clear()

for i, linha in enumerate(alunos):
    print(f'Aluno {i+1}: {linha}')

    if linha.index(min(linha)) == 0:
        prova1 += 1
        listp1.append(i+1)

    elif linha.index(min(linha)) == 1:
        prova2 += 1
        listp2.append(i+1)

    elif linha.index(min(linha)) == 2:
        prova3 += 1
        listp3.append(i+1)


print(f'\nPiores notas por aluno:\n'
      f'Prova 1: {prova1} - Alunos: {listp1}\n'
      f'Prova 2: {prova2} - Alunos: {listp2}\n'
      f'Prova 3: {prova3} - Alunos: {listp3}\n')

"""
Resposta para estudo

# 17 -
print('----------')
print('Seção 7 - Parte 2 - Questão 17')
from random import *
from collections import Counter
matriz1 = []
lista = []
valor = 0
pior_nota = Counter([])
for l in range(10):
    for c in range(3):
        lista.append(randint(0, 10))
    matriz1.append(list(lista))
    lista.clear()
for l in range(len(matriz1)):
    print(matriz1[l])
    print(matriz1[l].index(min(matriz1[l])))
    lista.append(matriz1[l].index(min(matriz1[l])))
pior_nota = Counter(lista)
print(f'{pior_nota[0]} alunos tiveram a pior nota na prova 1.')
print(f'{pior_nota[1]} alunos tiveram a pior nota na prova 2.')
print(f'{pior_nota[2]} alunos tiveram a pior nota na prova 3.')
"""