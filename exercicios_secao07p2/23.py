"""
23. Faça um programa que leia uma matriz A de tamanho 3x3 e calcule B = A^2
"""

import random

matriz1, lista1, matriz2, lista2, matriz3, lista3 = [], [], [], [], [], []
valmin, valmax = 0.0, 10.0


for i in range(3):
    for j in range(3):
        lista1.append(round(valmin + (valmax - valmin) * random.random(), 2))

    matriz1.append(list(lista1))
    lista1.clear()

matriz2 = matriz1.copy()

print('Matriz 1:')

for linha in matriz1:
    print(linha)

print('\nMatriz 2:')

for linha in matriz2:
    print(linha)

for i in range(len(matriz1)):
    for j in range(len(matriz1[i])):
        valor = 0
        for k in range(len(matriz1[i])):
            "print(f'Multi {k}: {matriz1[i][k]} * {matriz2[k][j]}') # Visualização do calculo"
            valor += (matriz1[i][k] * matriz2[k][j])
        "print(f'Valor {i}{j}: {valor}') # Visualização do valor retornado"
        lista3.append(round(valor, 2))
        "print(f'Lista: {lista3}') # Visualização da lista a ser inclusa na matriz"
    matriz3.append(list(lista3))
    lista3.clear()

print('\nMatriz resultante da soma:')
for linha in matriz3:
    print(linha)

"""
Para estudo:

# #########################################################
# 23
# Faça um programa que leia uma matriz A de tamanho 3 x 3 e
# calcule B = A² #
# #########################################################
 
 
mA = [[4, 3, 0], [0, 1, 7], [3, 6, 3]]
mB = mA.copy()
mC = list()
lmC = list()
potencia = 2
 
 
# Criando uma matriz com elementos iguais a zero para utilizar os índices
for p in range(len(mA)):
    for q in range(len(mA[0])):
        lmC.append(0)
    mC.append(list(lmC))
    lmC.clear()
 
 
# O laço for irá repetir o código que multiplica uma matriz por outra
# até a quantidade de vezes indicada na variável 'potencia'.
for o in range(1, potencia):
    
    # O laço for realiza a multiplicação das matrizes
    for l in range(len(mA)):
        for m in range(len(mA[0])):
            for n in range(len(mC)):
                mC[l][m] += mA[l][n] * mB[n][m]
    
    # A matriz mB recebe os valores de mC para utilizar no caso
    # de um potência acima de 2 ( O que NÃO foi pedido no exercício). Após isso, a matriz mC é limpa.
    mB = mC.copy()
    mC.clear()
    
    # O laço seguinte recria a matriz C (mC) com valores zerados
    # mas do tamanho da matriz A para novo ciclo
    for p in range(len(mA)):
        for q in range(len(mA[0])):
            lmC.append(0)
        mC.append(list(lmC))
        lmC.clear()
 
# Imprimindo a matriz por linha:
for linhas in mB[::]:
    print(linhas)
"""