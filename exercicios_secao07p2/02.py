"""
02. Declare uma matriz 5x5. Preencha com 1 a diagonal principal e com 0 os demais
elementos. Escreva ao final a matriz obtida.
"""

matriz = []
lista = []

for i in range(5):
    for j in range(5):
        if i == j:
            lista.append(1)
        else:
            lista.append(0)
    matriz.append(list(lista))
    lista.clear()

for linhas in matriz:
    print(linhas)
