"""
14. Faça um programa para gerar automaticamente números entre 0 e 99 de uma cartela
de bingo. Sabendo que cada cartela deverá conter 5 linhas de 5 números, gere estes
dados de modo a não ter números repetidos dentro das cartelas. O programa deve
exibir na tela a cartela gerada.
"""

import random

matriz, lista = [], []


for i in range(5):
    j = 0

    while j < 5:
        existe = False
        valor = random.randint(1, 99)

        for linha in matriz:
            for n in linha:
                if valor == n:
                    existe = True

        if existe is not True:
            lista.append(valor)
            j += 1

    matriz.append(list(lista))
    lista.clear()

print('Matriz:')

for linhas in matriz:
    print(linhas)


"""
Solução para estudo:

print('Este programa gera aleatoriamente 25 elementos não-repetidos entre 0 e 99 e os exibe numa matriz 5x5.')
 
from random import randint
 
 
def n_rand():
    # Função que gera um conjunto com 25 números aleatórios
    s_aux = set()
 
    while len(s_aux) < 25:
        n = randint(0, 99)
        s_aux.add(n)
 
    return s_aux
 
 
s = n_rand()
print(f'Números: {s}')
 
M = [[s.pop() for _ in range(5)] for _ in range(5)]
 
print('\nCartela:')
for linha in M:
    print(linha)
"""