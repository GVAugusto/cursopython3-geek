"""
03. Faça um programa que preencha uma matriz 4x4 com o produto do valor da linha
e da coluna de cada elemento. Em seguida, imprima na tela a matriz.
"""

matriz = []
lista = []

for i in range(4):
    for j in range(4):
        lista.append(i * j)
    matriz.append(list(lista))
    lista.clear()

for linhas in matriz:
    print(linhas)
