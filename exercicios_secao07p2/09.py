"""
09. Leia uma matriz de 3x3 elementos. Calcule a soma dos elementos que estão abaixo
da diagional principal.
"""

import random

matriz, lista = [], []
soma = 0

for i in range(3):
    for j in range(3):
        lista.append(random.randint(0, 100))
    matriz.append(list(lista))
    lista.clear()

print('Matriz:')

for linhas in matriz:
    print(linhas)

for i in range(3):
    for j in range(3):
        if i != 2 and i == j:
            soma += matriz[i+1][j]

print(f'\nSoma: {soma}')
