"""
15. Leia uma matriz 5x10 que se refere a respostas de 10 questões de múltipla
escolha, referentes a 5 alunos. Leia também um vetor de 10 posições contendo
o gabarito de respostas que podem ser a, b, c ou d.
Seu programa deverá comparar as respostas de cada candidato com o gabarito e emitir
um vetor denominado resultado, contendo a pontuação correspondente a cada aluno.
"""

import random

aluno, respostas, gabarito = [], [], []

for i in range(5):
    for j in range(10):
        respostas.append(chr(random.randint(ord('a'), ord('d'))))
    aluno.append(list(respostas))
    respostas.clear()

for i in range(10):
    gabarito.append(chr(random.randint(ord('a'), ord('d'))))

print('Matriz:')

for linhas in aluno:
    print(linhas)

print(f'\nGabarito: \n{gabarito}\n')

for i in range(5):
    nota = 0
    for j in range(10):
        if aluno[i][j] == gabarito[j]:
            nota += 1

    print(f'Nota do aluno {i+1}: {nota}')

"""
Para estudo

'Ex15'
from collections import Counter
 
print('Este programa guarda 10 respostas de 5 alunos, entre elas a, b, c ou d, o gabarito, e retorna a quantidade '
      'de acertos de cada aluno.')
 
# Montando a matriz de respostas...
print('Respostas')
respostas = [[input(f'Aluno {i}: ') for _ in range(10)] for i in range(5)]
 
# Definindo o gabarito...
gabarito = ('a', 'a', 'c', 'd', 'd', 'd', 'b', 'a', 'b', 'c')
 
# Aqui colocamos 1 se a resposta estiver correta e 0 se estiver errada. Estamos conferindo a resposta j do aluno i
# com a resposta j do gabarito...
M_score = [[1 if respostas[i][j] == gabarito[j] else 0 for j in range(10)] for i in range(5)]
 
# Utilizamos o counter para contar quantas respostas certas há em cada linha de M_score...
score = [Counter(M_score[k])[1] for k in range(5)]
 
# Imprimindo o resultado...
print('')
for i in range(5):
    if score[i] > 1:
        print(f'O aluno {i} acertou {score[i]} questões.')
    elif score[i] == 1:
        print(f'O aluno {i} acertou 1 questão.')
    else:
        print(f'O aluno {i} não acertou nenhuma questão.')
"""