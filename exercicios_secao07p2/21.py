"""
21. Faça um programa que leia duas matrizes 2x2 com valores reais. Ofereça ao
usuário um menu de opções:

a. somar as duas matrizes
b. subtrair a primeira matriz da segunda
c. adicionar uma constante às duas matrizes
d. imprimir as matrizes

Nas duas primeiras opções uma terceira matriz 3x3 deve ser criada.
Na terceira opção o valor da constante deve ser lido e o resultado da adição da
constante deve ser armazenado na própria matriz.
"""

import random

matriz1, lista1, matriz2, lista2, matriz3, lista3 = [], [], [], [], [], []
valmin, valmax = 0.0, 10.0

for i in range(2):
    for j in range(2):
        lista1.append(round(valmin + (valmax - valmin) * random.random(), 2))
        lista2.append(round(valmin + (valmax - valmin) * random.random(), 2))

    matriz1.append(list(lista1))
    matriz2.append(list(lista2))

    lista1.clear()
    lista2.clear()

print('Matriz 1:')

for linha in matriz1:
    print(linha)

print('\nMatriz 2:')

for linha in matriz2:
    print(linha)

while True:
    opcao = str(input(f'\n\nFavor selecionar uma das opções disponíveis:\n'
                      f'a. Somar as duas matrizes\n'
                      f'b. Subtrair a primeira matriz da segunda\n'
                      f'c. Adicionar uma constante às duas matrizes\n'
                      f'd. Imprimir as matrizes\n'
                      f'Opção: '))

    if opcao == 'a' or opcao == 'b' or opcao == 'c' or opcao == 'd':
        break

if opcao == 'a':
    for i in range(len(matriz1)):
        for j in range(len(matriz1[i])):
            lista3.append(round(matriz1[i][j] + matriz2[i][j], 2))
        matriz3.append(list(lista3))
        lista3.clear()

    print('\nMatriz resultante da soma:')
    for linha in matriz3:
        print(linha)

elif opcao == 'b':
    for i in range(len(matriz1)):
        for j in range(len(matriz1[i])):
            lista3.append(round(matriz1[i][j] - matriz2[i][j], 2))
        matriz3.append(list(lista3))
        lista3.clear()

    print('\nMatriz resultante da soma:')
    for linha in matriz3:
        print(linha)

elif opcao == 'c':
    valor = float(input('Favor informar o valor da constante: '))
    for i in range(len(matriz1)):
        for j in range(len(matriz1[i])):
            matriz1[i][j] += valor
            matriz2[i][j] += valor

    print('\nMatriz 1 resultante da soma:')
    for linha in matriz1:
        print(linha)

    print('\nMatriz 2 resultante da soma:')
    for linha in matriz2:
        print(linha)

if opcao == 'd':
    print('Matriz 1:')

    for linha in matriz1:
        print(linha)

    print('\nMatriz 2:')

    for linha in matriz2:
        print(linha)
