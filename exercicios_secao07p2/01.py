"""
01. Leia uma matriz 4x4, conte e escreva quantos valores maiores que 10 ela possui.
"""

matriz = []
lista = []

for i in range(4):
    for j in range(4):
        lista.append(float(input(f'Favor inserir o valor da matriz {i+1} x {j+1}: ')))
    matriz.append(list(lista))
    lista.clear()

for linhas in matriz:
    print(linhas)
