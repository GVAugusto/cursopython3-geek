"""
16. Faça um programa para corrigir uma prova com 10 questões de múltipla escolha
(a, b, c, d ou e), em uma turma com 3 alunos. Cada questão vale 1 ponto. Leia o
gabarito, e para cada aluno leia sua matrícula (número inteiro) e suas respostas.
Calcule e escreva: Para cada aluno, escreva sua matrícula, sua resposta, sua nota.
O percentual de aprovação, assumindo média 7.0.
"""

import random

alunos, respostas, gabarito = [], [], []
aluno = {}
aprovados, i = 0, 0

while i < 3:
    achou = False
    matricula = int(input(f'Favor informar a matrícula do aluno {i+1} de 3: '))

    for j in range(10):
        respostas.append(chr(random.randint(ord('a'), ord('d'))))

    for linha in alunos:
        if linha['matricula'] == matricula:
            achou = True

    if achou is False:
        aluno = {'matricula': matricula, 'respostas': list(respostas), 'nota': 0}
        alunos.append(aluno)
        i += 1
    else:
        print('Matricula já existe, favor informar outra válida')

    respostas.clear()


for i in range(10):
    gabarito.append(chr(random.randint(ord('a'), ord('d'))))

print(gabarito)

for linha in alunos:
    nota = 0

    for i, valor in enumerate(list(linha['respostas'])):
        if gabarito[i] == valor:
            nota += 1

    linha.update({'nota': nota})
    if nota >= 7:
        aprovados += 1
    print(linha)

print(f'Percentual de Aprovação: {(aprovados / 3) * 100}%')
