"""
07. Gerar e imprimir uma matriz de tamanho 10x10, onde seus elementos são da forma:

A[i][j] = 2i + 7j - 2 se i < j
A[i][j] = 3i^2 - 1 se i = j
A[i][j] = 4i^3 - 5j^2 + 1 se i > j
"""

matriz = []
lista = []

for i in range(10):
    for j in range(10):
        if i < j:
            lista.append((2 * i) + (7 * j) - 2)
        elif i == j:
            lista.append((3 * (i ** 2)) - 1)
        else:
            lista.append((4 * (i ** 3)) - (5 * (j ** 2)) + 1)
    matriz.append(list(lista))
    lista.clear()

for linhas in matriz:
    print(linhas)
