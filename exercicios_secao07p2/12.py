"""
12. Leia uma matriz de 3x3 elementos. Calcule e imprima a sua transposta.
"""

import random

matriz, lista = [], []
soma = 0
transposta, ltransposta = [], []

for i in range(3):
    for j in range(3):
        lista.append(random.randint(0, 100))
    matriz.append(list(lista))
    lista.clear()

print('Matriz:')

for linhas in matriz:
    print(linhas)

for i in range(3):
    for j in range(3):
        ltransposta.append(matriz[j][i])
    transposta.append(list(ltransposta))
    ltransposta.clear()

print('\nMatriz Transposta:')
for linhas in transposta:
    print(linhas)
