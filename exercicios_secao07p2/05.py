"""
05. Leia uma matriz 5x5. Leia também um valor X. O programa deverá fazer uma busca
desse valor na matriz e, ao final, escrever a localização (linha e coluna) ou uma
mensagem: "não encontrado"
"""

import random

matriz, lista = [], []
achou = 0

for i in range(4):
    for j in range(4):
        lista.append(random.randint(0, 100))
    matriz.append(list(lista))
    lista.clear()

for linhas in matriz:
    print(linhas)

x = int(input('Favor informar o valor a ser procurado na matriz: '))

for linha in matriz:
    for n in linha:
        if n == x:
            px = matriz.index(linha)
            py = linha.index(n)
            print(f'Posição X/Y: {px}/{py}')
            achou += 1

if achou == 0:
    print(f'{x} não encontrado na matriz')
