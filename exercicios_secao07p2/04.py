"""
04. Leia uma matriz 4x4, imprima a matriz e retorne a localização (linha e coluna)
do maior valor.
"""

import random

matriz, lista = [], []

for i in range(4):
    for j in range(4):
        lista.append(random.randint(0, 100))
    matriz.append(list(lista))
    lista.clear()

maior = max(max(linha) for linha in matriz)

for linha in matriz:
    for n in linha:
        if n == maior:
            x = matriz.index(linha)
            y = linha.index(n)

for linhas in matriz:
    print(linhas)

print(f'Maior: {maior}')
print(f'Posição X/Y: {x}/{y}')


"""
Solução para estudo

print('Este programa recebe os elementos de uma matriz 4x4 e retorna a posição do maior elemento.')

matriz = [[int(input(f'Elemento {i}{j}: ')) for j in range(4)] for i in range(4)]

maior = max(max(linha) for linha in matriz)

posicao = [str(matriz.index(linha)) + str(linha.index(n)) for linha in matriz for n in linha if n == maior]

print(f"Linha/Coluna do maior elemento: {'/'.join(posicao[0])}")
"""