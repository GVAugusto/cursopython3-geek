"""
13. Gere uma matriz 4x4 com valores no intervalo [1, 20]. Escreva um programa
que transforme a matriz gerada numa matriz triangular inferior, ou seja, atribuindo
zero a todos os elementos acima da diagonal principal. Imprima a matriz original
e a matriz transformada.
"""

import random

matriz, lista = [], []
soma = 0

for i in range(4):
    for j in range(4):
        lista.append(random.randint(1, 20))
    matriz.append(list(lista))
    lista.clear()

print('Matriz:')

for linhas in matriz:
    print(linhas)

for i in range(4):
    for j in range(4):
        if i < j:
            matriz[i][j] = 0

print('\nMatriz Triangular Inferior:')

for linhas in matriz:
    print(linhas)
