"""
06. Leia duas matrizes 4x4 e escreva uma terceira com os maiores valores de cada
posição das matrizes lidas.
"""

import random

matriz1, matriz2, lista1, lista2 = [], [], [], []
matriz3, lista3 = [], []

for i in range(4):
    for j in range(4):
        lista1.append(random.randint(0, 100))
        lista2.append(random.randint(0, 100))
    matriz1.append(list(lista1))
    matriz2.append(list(lista2))
    lista1.clear()
    lista2.clear()

print('\nMatriz 1:')

for linha1 in matriz1:
    print(linha1)

print('\nMatriz 2:')

for linha2 in matriz2:
    print(linha2)

for i in range(4):
    for j in range(4):
        if matriz1[i][j] >= matriz2[i][j]:
            lista3.append(matriz1[i][j])
        else:
            lista3.append(matriz2[i][j])
    matriz3.append(list(lista3))
    lista3.clear()

print('\nMatriz 3:')

for linha3 in matriz3:
    print(linha3)


"""
Solução para estudo:

'Ex6'
print('Este programa gera duas matrizes 4x4 e retorna uma terceira com o maior valor de cada posição.')
 
from random import randint
 
M1 = [[randint(-10, 10) for _ in range(4)] for _ in range(4)]          # gerando elementos aleatórios
M2 = [[randint(-10, 10) for _ in range(4)] for _ in range(4)]
 
print('M1:')                                                           # exibindo M1
for linha in M1:
    print(linha)
 
print('\nM2:')                                                         # exibindo M2
for linha in M2:
    print(linha)
 
M3 = [[max(M1[i][j], M2[i][j]) for j in range(4)] for i in range(4)]   # pegando o maior [i][j] entre M1 e M2
 
print('\nM3:')                                                         # exibindo M3
for linha in M3:
    print(linha)
"""