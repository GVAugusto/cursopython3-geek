"""
20. Faça um programa que leia uma matriz 3x6 com valores reais.

a. Imprima a soma de todos os elementos das colunas ímpares
b. Imprima a média aritmética dos elementos da segunda e quarta colunas
c. Substitua os valores da sexta coluna pela soma dos valores das colunas 1 e 2
d. Imprima a matriz modificada
"""

import random

matriz, lista = [], []
valmin, valmax = 0.0, 100.0
soma_impar, soma_media_arit, total_num = 0, 0, 0

for i in range(3):
    for j in range(6):
        lista.append(round(valmin + (valmax - valmin) * random.random(), 3))
    matriz.append(list(lista))
    lista.clear()

for linha in matriz:
    print(linha)

for i in range(len(matriz)):
    for n in range(len(matriz[i])):
        if (n + 1) % 2 == 1:
            soma_impar += matriz[i][n]

        if n == 1 or n == 3:
            soma_media_arit += matriz[i][n]
            total_num += 1

        if n == 5:
            matriz[i].pop()
            matriz[i].insert(n, round(matriz[i][0] + matriz[i][1], 3))

media_arit = round(soma_media_arit / total_num, 3)

print(f'Soma colunas impares: {soma_impar:3}\n'
      f'Média aritmética: {media_arit}\n'
      f'Vetor modificado:')

for linha in matriz:
    print(linha)
