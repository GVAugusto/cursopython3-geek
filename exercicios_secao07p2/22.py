"""
22. Faça um programa que leia duas matrizes A e B de tamanho 3 x 3 e calcule
C = A * B
"""

import random

matriz1, lista1, matriz2, lista2, matriz3, lista3 = [], [], [], [], [], []
valmin, valmax = 0.0, 10.0


for i in range(3):
    for j in range(3):
        lista1.append(round(valmin + (valmax - valmin) * random.random(), 2))
        lista2.append(round(valmin + (valmax - valmin) * random.random(), 2))

    matriz1.append(list(lista1))
    matriz2.append(list(lista2))

    lista1.clear()
    lista2.clear()

print('Matriz 1:')

for linha in matriz1:
    print(linha)

print('\nMatriz 2:')

for linha in matriz2:
    print(linha)

for i in range(len(matriz1)):
    for j in range(len(matriz1[i])):
        valor = 0
        for k in range(len(matriz1[i])):
            "print(f'Multi {k}: {matriz1[i][k]} * {matriz2[k][j]}') # Visualização do calculo"
            valor += (matriz1[i][k] * matriz2[k][j])
        "print(f'Valor {i}{j}: {valor}') # Visualização do valor retornado"
        lista3.append(round(valor, 2))
        "print(f'Lista: {lista3}') # Visualização da lista a ser inclusa na matriz"
    matriz3.append(list(lista3))
    lista3.clear()

print('\nMatriz resultante da soma:')
for linha in matriz3:
    print(linha)
